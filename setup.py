import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="sila2lib_implementations", # Replace with your own username
    version="0.0.1",
    author="Lukas Bromig",
    author_email="lukas.bromig@tum.de",
    description="A package of SiLA drivers for various devices at TUM-BVT",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/lukas.bromig/sila2lib_implementations",
    include_package_data=True,
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)
