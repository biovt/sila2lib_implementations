Creation date: 20.10.2020, 12:50  
Last modification: 05.02.2022, 12:52  
Authors: Lukas Bromig, Nikolas von den Eichen, Felix Moorhoff  
Contributors: Valeryia Sidarava, Jose Jesus de Pina Torres

![sila-python](docs/website/static/img/sila-python-logo.png)

Sila2lib_implementations
=====================================================

[![Pipeline Status](https://gitlab.com/biovt/sila2lib_implementations/badges/master/pipeline.svg)](https://gitlab.com/biovt/sila2lib_implementations/commits/master)
[![coverage report](https://gitlab.com/biovt/sila2lib_implementations/badges/master/coverage.svg)](https://gitlab.com/biovt/sila2lib_implementations/commits/main)


This repository contains [SiLA2](https://sila-standard.com) drivers for a variety of devices that are being used at the [Institute of Biochemical Engineering](https://www.mw.tum.de/en/biovt/home/) at TUM.


The following devices are supported:
- **2mag, BioREACTOR48 (48x Parallel Bioreactor System)**
- **BlueSens, BlueVary (Offgas-analytics)**
- **Eppendorf, DASGIP (4x Parallel Bioeactor System)**
- **LAUDA, LOOP 250**
- **LAUDA, LOOP 500**
- **PreSens sensor bars**
- **Reglo, RegloICC (Peristaltic Pump)**
- **Reglo, RegloDC (Peristaltic Pump)**
- **Bronkhorst, El-Flow Prestige (Flowmeter)**
- **Mettler-Toledo, MT Viper SW (Laboratory Balance)**

SiLA version of drivers may vary. In case of incompatibilities, please reach out to:
*lukas.bromig@tum.de*

## Content

| **Device**      | **Description**             | **SiLA Python branch(*)** | **State** | **Last update (main)** |
|-----------------|----------------------|------------------------|-----------|-----------------------------|
| BioREACTOR48    | mL-Bioreactor system | master (v.1.0)         | in use    |          20.08.2021         |
| BlueVary        | off-gas analytic     | codegenerator-0.3      | in use    |          23.07.2021         |
| DASGIP          | L-Bioreactor system  | master (v.1.0)         | in use    |          26.04.2021         |
| ELFLOW Prestige | flowmeter            | codegenerator-0.3      | testing   |          30.08.2021         |
| Lauda Loop250   | thermostat           | master (v.1.0)         | not used  |          25.10.2020         |
| Lauda Loop500   | thermostat           | master (v.1.0)         | not used  |          25.10.2020         |
| MT Viper SW     | laboratory balance   | codegenerator-0.3      | in use    |          14.04.2021         |
| Presens         | pH/DO-sensor bars    | codegenerator-0.3      | in use    |          20.08.2021         |
| RegloDC         | peristaltic pump     | codegenerator-0.3      | in use    |          14.04.2021         |
| RegloICC        | peristaltic pump     | master (v.1.0)         | in use    |          12.03.2021         |

(*) SiLA Python branches:  
As of 11.11.2021, SiLA 2 Python versions "master (v.1.0)" and "codegenerator-0.3" are deprecated. Backwards
compatibility is intended. If you encounter any issues, please reach out! All forthcoming development and driver updates
will use the latest SiLA 2 Python reference implementation.

## Project structure - generically
```
└── sila2lib_implementations
    ├── sila2lib_implementations
    │   ├── Device
    │   │   └── Device_project
    │   │   │   └── DeviceServicer.sila.xml
    │   │   │   └── service_description.json
    │   │   │   └── SiLAFramework.proto
    │   │   └── DeviceService
    │   │   │   └── DeviceServicer
    │   │   │   │   └── DeviceServicer_servicer.py
    │   │   │   │   └── DeviceServicer_real.py
    │   │   │   │   └── DeviceServicer_simulation.py
    │   │   │   │   └── DeviceServicer_default_arguments.py
    │   │   │   │   └── __init__.py
    │   │   │   └── DeviceService_client.py
    │   │   │   └── DeviceService_server.py
    │   │   │   └── __init__.py
    │   │   └── __init__.py
    │   └── __init__.py
    ├── tests
    │   ├── Device
    │   │   └── test_device.py
    ├── README.md
    ├── LICENSE
    ├── pyproject.toml
    └── setup.py
```

A more detailed description of all folders and files can be found on the [website](https://biovt.gitlab.io/sila2lib_implementations/docs/Developer-section/general-structure/). 



## Installation
The repository is setup in a way that it can be installed conveniently as python package.
Please [read the docs](https://biovt.gitlab.io/sila2lib_implementations/docs/Developer-section/troubleshooting-installation/)
if you encounter any errors. A detailed troubleshooting on any errors is provided there.

### Python Package Index (PyPI) (recommended)
Stable releases are available at the [Python Package Index (PyPI)](https://pypi.org/project/sila2lib-implementations/).
Use the package installer for python (pip) for installation:
``` {.sourceCode .console}
pip install sila2lib_implementations
```

### Gitlab (for developers):
Navigate into a directory where you would like to build the sila2lib-implementations. Clone the repository (or desired branches) and change into it. Install the package management tool poetry and configure it in a way to create the virtual environment in your project directory. This might be crutial to circumvent some import errors. With `poetry shell` you can activate the virtual environment where poetry installed your software in. Use `deactivate` to exit the activated venv again.  
Alternatively, pip or different methods listed [here](https://packaging.python.org/en/latest/tutorials/installing-packages/) can be used for installation. A `setup.py` and `requirements.txt` file is provided for these cases. However, they are not recommended.
Finally, have a look at [Quickstart - first steps  ](#quickstart---first-steps) to verify if installation was entirely successful and to get comfortable with sila2lib-implementations :)

``` { .sourceCode .console }
git clone https://gitlab.com/biovt/sila2lib_implementations
cd sila2lib_implementations

sudo apt update && sudo apt upgrade
pip install poetry
poetry config virtualenvs.in-project true
poetry install
poetry shell 

# with pip
pip install --editable .[dev]  # [dev] installs the development packages, too
```

## Quickstart - first steps
Check if the installation was successful: Try to start your first sila server and run some unit test. Test are executed by
default in simulation mode, so you do not have to worry about device damages etc.
It is assumed that you are located in the `sila2lib_implementations` root directory. Refer to section [Project structure - generically](#project-structure---generically)
to get comfortable with the projects structure. Additionally, you need to be in your activated virtual environment in case you
installed all the packages there (`poetry shell` or `source bin/activate`).
``` {.sourceCode .console}
python -m sila2lib_implementations/LAUDA/LAUDA_ThermostatService/LAUDA_ThermostatService_server.py  # start your first server
pytest tests/LAUDA/test_lauda.py  # run a simulation for the device 'Lauda'
```
For further usage, like editing existing devices or implementing new ones, please continue reading the [docs](https://biovt.gitlab.io/sila2lib_implementations/docs/Developer-section/update-drivers/)

## Developer note
Any contribution is highly appreciated, not only on a coding base. If you are facing issues of any kind please do not
hesitate to [open an issue](https://gitlab.com/biovt/sila2lib_implementations/issues).

## Development environment  
Before committing please make sure to run the checks to push nice and working code. Or integrate the pre-commit tool to aid
you in reminding of that matter. Everything is explained [here](https://biovt.gitlab.io/sila2lib_implementations/docs/Developer-section/contribution/).

## Implementation of new devices
Please refer to the [Sila_python](https://gitlab.gwdg.de/niklas.mertsch/sila2-redo/-/tree/master) repository for implementation
of new devices. However, we are also providing a step to step implementation of the RegloICC periplastic pump [here](https://biovt.gitlab.io/sila2lib_implementations/docs/Developer-section/write-a-sila-server/).
In case of extension or changes of existing devices keep in mind to also include the changes in the respective `.xml` files. 

## Documentation
The documentation of sila_python can be found here: [https://sila2.gitlab.io/sila_python](https://sila2.gitlab.io/sila_python)  
The official SiLA-Standard specifications can be found at [https://sila-standard.org](https://sila-standard.org).  
If you still can't find an answer, please [open an issue](https://gitlab.com/biovt/sila2lib_implementations/issues).

License
=======
This code is licensed under the [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html).
