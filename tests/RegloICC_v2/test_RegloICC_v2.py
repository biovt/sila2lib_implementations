import logging
import pytest

from sila2.framework.errors.validation_error import ValidationError
import RegloICC
# from RegloICC import RegloLib


ADDRESS = 1

logger = logging.getLogger(__name__)
client = RegloICC.Client(address='127.0.0.1', port=50052, insecure=True)
# reglo_lib = RegloLib(client)


is_channel_addressing = client.DeviceService.GetChannelAddressing(Address=ADDRESS)
print(is_channel_addressing)
print(type(is_channel_addressing.ChannelAddressing))
response = client.DeviceService.GetChannelAddressing(Address=ADDRESS).ChannelAddressing
if response is False:
    client.DeviceService.SetChannelAddressing(Address=ADDRESS, ChannelAddressing=True)


# pytest -v -o log_cli=true /RegloICC_v2/test_RegloICC_v2.py


class TestFeatureDeviceServiceReal:

    @pytest.fixture(scope='class', autouse=True)
    def setup_and_teardown(self):
        client.ParameterController.SetFlowRateMode(Channel=1)
        client.ParameterController.SetFlowRate(Channel=1, SetFlowRate=10)
        yield

    def test_GetChannelAddressing(self):
        """ Information whether or not channel addressing is enabled String of form XXXXXXX; 0=disabled,1=enabled. """
        response = client.DeviceService.GetChannelAddressing(Address=ADDRESS)
        assert response
        assert isinstance(response.ChannelAddressing, bool)
        assert response.ChannelAddressing in [True, False]

    @pytest.mark.parametrize("channel_addressing", [True, False])
    def test_SetChannelAddressing(self, channel_addressing):
        """ Channel Addressing of the pump succeeded to Set. """
        response = client.DeviceService.SetChannelAddressing(Address=ADDRESS, ChannelAddressing=channel_addressing)
        assert response
        assert isinstance(response.ChannelAddressingSet, bool)
        assert response.ChannelAddressingSet is True

    @pytest.mark.parametrize("channel_addressing", [True, False])
    def test_switch_channel_addressing(self, channel_addressing):
        """ Check if switching between True/False is working """
        client.DeviceService.SetChannelAddressing(Address=ADDRESS, ChannelAddressing=channel_addressing)
        response = client.DeviceService.GetChannelAddressing(Address=ADDRESS)
        assert response.ChannelAddressing is channel_addressing

    def test_GetPumpStatus(self):
        """ Current pump status. +=running, -=stopped/standby. """
        response = client.DeviceService.GetPumpStatus(Address=ADDRESS)
        assert response
        assert isinstance(response.CurrentPumpStatus, str)
        assert response.CurrentPumpStatus in ['+', '-']

    @pytest.mark.parametrize("mode, expected_response", [('running', '+'), ('stopped', '-')])
    def test_switch_pump_status(self, mode, expected_response):
        """ Current pump status. +=running, -=stopped/standby. """
        client.DeviceService.SetChannelAddressing(Address=ADDRESS, ChannelAddressing=True)
        response = None
        if mode == 'running':
            client.DriveController.StartPump(Channel=1)
            response = client.DeviceService.GetPumpStatus(Address=ADDRESS)
            client.DriveController.StopPump(Channel=1)
        elif mode == 'stopped':
            client.DriveController.StopPump(Channel=1)
            response = client.DeviceService.GetPumpStatus(Address=ADDRESS)
        assert response
        assert isinstance(response.CurrentPumpStatus, str)
        assert response.CurrentPumpStatus in ['+', '-']


class TestFeatureDriveControllerReal:

    @pytest.fixture(scope='class', autouse=True)
    def setup_and_teardown(self):
        client.DeviceService.SetChannelAddressing(Address=ADDRESS, ChannelAddressing=True)
        yield

    def test_StartPump(self):
        """ Start of pump succeeded. """
        response = client.DriveController.StartPump(Channel=1)
        assert response
        assert isinstance(response.StartStatus, str)
        assert response.StartStatus == '*'

    def test_StopPump(self):
        """ Shut-down of pump succeeded. """
        response = client.DriveController.StopPump(Channel=1)
        assert response
        assert isinstance(response.StopStatus, str)
        assert response.StopStatus == '*'

    def test_PausePump(self):
        """ Shut-down of pump succeeded. """
        client.DriveController.StartPump(Channel=1)
        response = client.DriveController.PausePump(Channel=1)
        client.DriveController.StopPump(Channel=1)
        assert response
        assert isinstance(response.PauseStatus, str)
        assert response.PauseStatus == '*'

    def test_GetPumpDirection(self):
        """ Pump direction query. J = clockwise/ K = counter-clockwise.  """
        response = client.DriveController.GetPumpDirection(Channel=1)
        assert response
        assert isinstance(response.PumpDirection, str)
        assert response.PumpDirection in ['J', 'K']

    def test_SetDirectionClockwise(self):
        """ Set Direction succeeded to Set. """
        response = client.DriveController.SetDirectionClockwise(Channel=1)
        assert response
        assert isinstance(response.SetDirectionSet, str)
        assert response.SetDirectionSet == '*'

    def test_SetDirectionCounterClockwise(self):
        """ Set Direction succeeded to Set. """
        response = client.DriveController.SetDirectionCounterClockwise(Channel=1)
        assert response
        assert isinstance(response.SetDirectionSet, str)
        assert response.SetDirectionSet == '*'

    @pytest.mark.parametrize("direction, expected_response", [('cw', 'J'), ('ccw', 'K')])
    def test_switch_direction(self, direction, expected_response):
        response = None
        if direction == 'cw':
            client.DriveController.SetDirectionClockwise(Channel=1)
            response = client.DriveController.GetPumpDirection(Channel=1)
        elif direction =='ccw':
            client.DriveController.SetDirectionCounterClockwise(Channel=1)
            response = client.DriveController.GetPumpDirection(Channel=1)
        assert response.PumpDirection == expected_response


class TestFeatureParameterControllerReal:

    def test_GetMode(self):
        """ Current channel or pump mode is retrieved. """
        response = client.ParameterController.GetMode(Channel=1)
        assert response
        assert isinstance(response.CurrentPumpMode, str)
        assert response.CurrentPumpMode in ['L', 'M', 'O', 'G', 'Q', 'N', 'P']


    def test_SetFlowRateMode(self):
        """ Pump mode succeeded to Set. """
        response = client.ParameterController.SetFlowRateMode(Channel=1)
        assert response
        assert isinstance(response.PumpModeSet, str)
        assert response.PumpModeSet == '*'

    def test_switch_to_flow_rate_mode(self):
        client.ParameterController.SetFlowRateMode(Channel=1)
        response = client.ParameterController.GetMode(Channel=1)
        assert response.CurrentPumpMode == 'M'

    def test_SetVolumeRateMode(self):
        """ Pump mode succeeded to Set. """
        response = client.ParameterController.SetVolumeRateMode(Channel=1)
        assert response
        assert isinstance(response.PumpModeSet, str)
        assert response.PumpModeSet == '*'

    def test_switch_to_volume_rate_mode(self):
        client.ParameterController.SetVolumeRateMode(Channel=1)
        response = client.ParameterController.GetMode(Channel=1)
        assert response.CurrentPumpMode == 'O'

    def test_SetTimeMode(self):
        """ Pump mode succeeded to Set. """
        response = client.ParameterController.SetTimeMode(Channel=1)
        assert response
        assert isinstance(response.PumpModeSet, str)
        assert response.PumpModeSet == '*'

    def test_switch_to_set_time_mode(self):
        client.ParameterController.SetTimeMode(Channel=1)
        response = client.ParameterController.GetMode(Channel=1)
        assert response.CurrentPumpMode == 'N'

    @pytest.mark.parametrize("flow_rate", [0, 1])
    def test_SetFlowRateAtModes(self, flow_rate):
        """ Current setting for flow for other modes is calculated by RPM (True) or flow rate (False) """
        response = client.ParameterController.SetFlowRateAtModes(Channel=1, FlowRate=flow_rate)
        assert response
        assert isinstance(response.SetFlowRateSet, str)
        assert response.SetFlowRateSet == '*'

    def test_GetFlowRateAtModes(self):
        """ Current setting for flow for other modes is calculated by RPM (True) or flow rate (False)"""
        response = client.ParameterController.GetFlowRateAtModes(Channel=1)
        assert response
        assert isinstance(response.CurrentFlowRate, bool)
        assert response.CurrentFlowRate in [True, False]

    @pytest.mark.parametrize("mode, expected_response", [('rpm_mode', False), ('flow_rate_mode', True)])
    def test_switch_flow_rate_at_modes(self, mode, expected_response):
        if mode == 'rpm_mode':
            client.ParameterController.SetRPMMode(Channel=1)
        elif mode == 'flow_rate_mode':
            client.ParameterController.SetFlowRateMode(Channel=1)
        response = client.ParameterController.GetFlowRateAtModes(Channel=1)
        client.ParameterController.SetFlowRateMode(Channel=1)
        assert response.CurrentFlowRate is expected_response

    def test_GetMaximumFlowRateWithCalibration(self):
        response = client.ParameterController.GetMaximumFlowRateWithCalibration(Channel=1)
        assert response
        assert isinstance(response.MaximumFlowRateWithCalibration, float)

    @pytest.mark.parametrize("volume, expected_response", [(9999, 9999.0), (5050, 5050.0), (0, 0.0), (100, 100.0)])
    def test_SetVolume(self, volume, expected_response):
        response = client.ParameterController.SetVolume(Channel=1, Volume=volume)
        assert response
        assert isinstance(response.VolumeSet, str)
        assert response.VolumeSet == str(expected_response)

    @pytest.mark.parametrize("flow_rate", [9999, 5050, 0, 100])
    def test_SetFlowRate(self, flow_rate):
        response = client.ParameterController.SetFlowRate(Channel=1, SetFlowRate=flow_rate)
        assert response
        assert isinstance(response.SetFlowRateSet, float)
        assert response.SetFlowRateSet == flow_rate

    def test_GetCurrentRunTime(self):
        response = client.ParameterController.GetCurrentRunTime(Channel=1)
        assert response
        assert isinstance(response.CurrentRunTime, int)
        assert response.CurrentRunTime < 35964000 and response.CurrentRunTime >= 0

    @pytest.mark.parametrize("run_time", [0, 3596400, 9999])
    def test_SetCurrentRunTime(self, run_time):
        response = client.ParameterController.SetCurrentRunTime(Channel=1, RunTime=run_time)
        assert response
        assert isinstance(response.SetCurrentRunTimeSet, str)
        assert response.SetCurrentRunTimeSet == '*'

    @pytest.mark.parametrize("run_time, expected_response", [(9999, 99995), (3596399, 35963995), (0, 0.5)])
    def test_change_current_run_time(self, run_time, expected_response):
        client.ParameterController.SetCurrentRunTime(Channel=1, RunTime=run_time)
        response = client.ParameterController.GetCurrentRunTime(Channel=1)
        assert response
        assert isinstance(response.CurrentRunTime, int)
        assert response.CurrentRunTime < expected_response and response.CurrentRunTime >= run_time

'''
class TestFeatureDeviceServiceSimulation:

    def test_SetPumpAddress(self):
        response = client.DeviceService.SetPumpAddress(Address=ADDRESS)
        assert response.PumpAddressSet
        assert isinstance(response.PumpAddressSet, str)

    def test_SetPumpAddress_constraint_violation(self):
        pass

    def test_SetPumpAddress_invalid_input(self):
        pass

    @pytest.mark.parametrize("language", [0, 1, 2, 3])
    def test_SetLanguage(self, language):
        """ 0 = English / 1 = French / 2 = Spanish / 3 = German """
        response = client.DeviceService.SetLanguage(Address=ADDRESS, Language=language)
        assert response.LanguageSet
        assert isinstance(response.LanguageSet, str)
        assert response.LanguageSet == 'Simulation: *'

    @pytest.mark.parametrize(
        "language, error, message", [
            (-1, ValidationError, "MinimalInclusive(0.0): -1"),
            (5, ValidationError, "MaximalInclusive(3.0): 5")])
    def test_SetLanguage_constraint_violation(self, language, error, message):
        """ 0 = English / 1 = French / 2 = Spanish / 3 = German """
        try:
            response = client.DeviceService.SetLanguage(Address=ADDRESS, Language=language)
            assert False
        except ValidationError as err:
            assert type(err) == error
            assert message in err.message
        except Exception as err:
            assert False

    @pytest.mark.parametrize(
        "language, error, message", [
            ('1', TypeError, "'1' has type <class 'str'>, but expected one of: (<class 'int'>,)"),
            (1.0, TypeError, "1.0 has type <class 'float'>, but expected one of: (<class 'int'>,)" )])
    def test_SetLanguage_invalid_input(self, language, error, message):
        """ 0 = English / 1 = French / 2 = Spanish / 3 = German """
        try:
            response = client.DeviceService.SetLanguage(Address=ADDRESS, Language=language)
            assert False
        except TypeError as err:
            assert type(err) == error
            assert message in err.args[0]
        except Exception as err:
            assert False

    def test_GetLanguage(self):
        """ 0 = English / 1 = French / 2 = Spanish / 3 = German """
        response = client.DeviceService.GetLanguage(Address=ADDRESS)
        assert response.CurrentLanguage
        assert isinstance(response.CurrentLanguage, int)
        assert response.CurrentLanguage == 1

    def test_GetPumpStatus(self):
        """ +=running, -=stopped/standby. """
        response = client.DeviceService.GetPumpStatus(Address=ADDRESS)
        assert response.CurrentPumpStatus
        assert isinstance(response.CurrentPumpStatus, str)
        assert response.CurrentPumpStatus == '+'

    def test_GetVersionType(self):
        """ Current pump information. Response is string of model description (variable length), software version
        (3 digits) and pump head model type code (3 digits). """
        response = client.DeviceService.GetVersionType(Address=ADDRESS)
        assert response.CurrentVersionType
        assert isinstance(response.CurrentVersionType, str)
        # Todo: Implement reasonable response in simulation mode
        assert response.CurrentVersionType == 'abc-123-456'

    def test_GetVersionSoftware(self):
        """ Get pump software version. Response is string. """
        response = client.DeviceService.GetVersionSoftware(Address=ADDRESS)
        assert response.CurrentVersionSoftware
        assert isinstance(response.CurrentVersionSoftware, str)
        # Todo: Implement reasonable response in simulation mode
        assert response.CurrentVersionSoftware == '9999'

    def test_GetSerialNumber(self):
        """ Pump serial number query.  """
        response = client.DeviceService.GetSerialNumber(Address=ADDRESS)
        assert response.SerialNumber
        assert isinstance(response.SerialNumber, str)
        # Todo: Implement reasonable response in simulation mode
        assert response.SerialNumber == 'MySimulatedSerialNumber'

    @pytest.mark.parametrize("channel_addressing, expected_response", [(True, True), (False, False)])
    def test_SetChannelAddressing(self, channel_addressing, expected_response):
        """ Channel Addressing of the pump succeeded to Set. """
        response = client.DeviceService.SetChannelAddressing(Address=ADDRESS, ChannelAddressing=channel_addressing)
        assert response
        assert isinstance(response.ChannelAddressingSet, bool)
        assert response.ChannelAddressingSet is expected_response

    @pytest.mark.parametrize("channel_addressing, error, message",[
        (1, TypeError, "Expected a bool value"),
        (0, TypeError, "Expected a bool value"),
        ('1', TypeError, "Expected a bool value"),
        ('0', TypeError, "Expected a bool value"),
        ({}, TypeError, "Expected a bool value"),
        ([], TypeError, "Expected a bool value"),
    ])
    def test_SetChannelAddressing_invalid_input(self, channel_addressing, error, message):
        """ Channel Addressing of the pump succeeded to Set. """
        try:
            response = client.DeviceService.SetChannelAddressing(Address=ADDRESS, ChannelAddressing=channel_addressing)
            assert False
        except TypeError as err:
            assert type(err) == error
            assert message in err.args[0]
        except Exception as err:
            assert False

    def test_GetChannelAddressing(self):
        """ Pump serial number query.  """
        response = client.DeviceService.GetChannelAddressing(Address=ADDRESS)
        assert response.ChannelAddressing
        assert isinstance(response.ChannelAddressing, bool)
        # Todo: Implement reasonable response in simulation mode
        assert response.ChannelAddressing is True

    @pytest.mark.parametrize("event_messages, expected_response", [
        (True, True),
        (False, False),
    ])
    def test_SetEventMessages(self, event_messages, expected_response):
        """ Event Messages of the pump succeeded to Set. """
        response = client.DeviceService.SetEventMessages(Address=ADDRESS, EventMessages=event_messages)
        assert response
        assert isinstance(response.EventMessagesSet, bool)
        assert response.EventMessagesSet is expected_response

    @pytest.mark.parametrize("event_messages, error, message", [
        (1, TypeError, "Expected a bool value"),
        (0, TypeError, "Expected a bool value"),
        ('1', TypeError, "Expected a bool value"),
        ('0', TypeError, "Expected a bool value"),
        ({}, TypeError, "Expected a bool value"),
        ([], TypeError, "Expected a bool value"),
    ])
    def test_SetEventMessages_invalid_input(self, event_messages, error, message):
        """ Channel Addressing of the pump succeeded to Set. """
        try:
            response = client.DeviceService.SetEventMessages(Address=ADDRESS, EventMessages=event_messages)
            assert False
        except TypeError as err:
            assert type(err) == error
            assert message in err.args[0]
        except Exception as err:
            assert False

    def test_GetEventMessages(self):
        """ Information whether or not event messages are enabled; 0=disabled,1=enabled. """
        response = client.DeviceService.GetEventMessages(Address=ADDRESS)
        assert response.EventMessages
        assert isinstance(response.EventMessages, bool)
        # Todo: Implement reasonable response in simulation mode
        assert response.EventMessages is True

    def test_GetSerialProtocol(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetPumpName(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetPumpName_constraint_violation(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetPumpName_invalid_input(self):
        # Todo: Implement this test
        pass

    def test_GetChannelNumber(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetChannelNumber(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetChannelNumber_constraint_violation(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetChannelNumber_invalid_input(self):
        # Todo: Implement this test
        pass

    def test_GetRevolutions(self):
        # Todo: Implement this test
        pass

    def test_GetChannelTotalVolume(self):
        # Todo: Implement this test
        pass

    def test_GetTotalTime(self):
        # Todo: Implement this test
        pass

    def test_GetHeadModel(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetHeadModel(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetHeadModel_constraint_violation(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetHeadModel_invalid_input(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetUserInterface(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetUserInterface_constraint_violation(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetUserInterface_invalid_input(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetDisableInterface(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetDisableInterface_constraint_violation(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetDisableInterface_invalid_input(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetDisplayNumbers(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetDisplayNumbers_constraint_violation(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetDisplayNumbers_invalid_input(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetDisplayLetters(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetDisplayLetters_constraint_violation(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetDisplayLetters_invalid_input(self):
        # Todo: Implement this test
        pass

    def test_GetTimeSetting(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetTimeSetting(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetTimeSetting_constraint_violation(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetTimeSetting_invalid_input(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetRunTimeM(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetRunTimeM_constraint_violation(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetRunTimeM_invalid_input(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetRunTimeH(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetRunTimeH_constraint_violation(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetRunTimeH_invalid_input(self):
        # Todo: Implement this test
        pass

    def test_GetRollerStepsLow(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetRollerStepsLow(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetRollerStepsLow_constraint_violation(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetRollerStepsLow_invalid_input(self):
        # Todo: Implement this test
        pass

    def test_GetRollerStepsHigh(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetRollerStepsHigh(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetRollerStepsHigh_constraint_violation(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetRollerStepsHigh_invalid_input(self):
        # Todo: Implement this test
        pass

    def test_GetRSV(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetRSV(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetRSV_constraint_violation(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetRSV_invalid_input(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetRSVReset(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetRSVReset_constraint_violation(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetRSVReset_invalid_input(self):
        # Todo: Implement this test
        pass

    def test_ResetRSVTable(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetNonFactoryRSV(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetNonFactoryRSV_constraint_violation(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetNonFactoryRSV_invalid_input(self):
        # Todo: Implement this test
        pass

    def test_GetPauseTime(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetPauseTime(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetPauseTime_constraint_violation(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetPauseTime_invalid_input(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetPauseTimeM(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetPauseTimeM_constraint_violation(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetPauseTimeM_invalid_input(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetPauseTimeH(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetPauseTimeH_constraint_violation(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetPauseTimeH_invalid_input(self):
        # Todo: Implement this test
        pass

    def test_GetTotalVolume(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SaveSettings(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SaveSettings_constraint_violation(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SaveSettings_invalid_input(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SaveSetRoller(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SaveSetRoller_constraint_violation(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SaveSetRoller_invalid_input(self):
        # Todo: Implement this test
        pass

    def test_GetFootSwitchStatus(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetRollersNumber(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetRollersNumber_constraint_violation(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetRollersNumber_invalid_input(self):
        # Todo: Implement this test
        pass

    def test_GetRollersNumber(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetPumpSerialNumber(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetPumpSerialNumber_constraint_violation(self):
        # Todo: Implement this test
        pass

    # @pytest.mark.parametrize()
    def test_SetPumpSerialNumber_invalid_input(self):
        # Todo: Implement this test
        pass
'''