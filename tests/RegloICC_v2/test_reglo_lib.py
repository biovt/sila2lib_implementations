import pytest
from time import sleep
import RegloICC
import RegloICC.reglo_lib as rlib

ADDRESS = 1

client = RegloICC.Client(address='127.0.0.1', port=50052, insecure=True)
reglo_lib = rlib.RegloLib(client=client)


def test_RegloLib_instantiation():
    object = rlib.RegloLib(client=client)
    assert object.client == client


@pytest.mark.parametrize("flow_rate, channels, volume, direction", [
    ([10, 10], [1, 2], [2, 2], ['K', 'K']),
    # (None, [3, 4], [2, 2], ['K', 'J']),
    # (None, [1, 2, 3, 4], [5, 5, 7, 7], ['K', 'J', 'K', 'J']),
])
def test_pump_set_volume_at_flow_rate(flow_rate, channels, volume, direction):
    max_wait_time = 20
    i = 0
    print(client.DeviceService.GetPumpStatus(Address=ADDRESS).CurrentPumpStatus)
    while True:
        if client.DeviceService.GetPumpStatus(Address=ADDRESS).CurrentPumpStatus == '-':
            break
        elif client.DeviceService.GetPumpStatus(Address=ADDRESS).CurrentPumpStatus == '+' and i < max_wait_time:
            sleep(1)
            i += 1
        elif client.DeviceService.GetPumpStatus(Address=ADDRESS).CurrentPumpStatus == '+' and i >= max_wait_time:
            for channel in range(4):
                client.DriveController.StopPump(Channel=i+1)
        else:
            assert False
    return_value = reglo_lib.pump_set_volume_at_flow_rate(
        pump_address=ADDRESS, flow_rate=flow_rate, channels=channels, volume=volume, direction=direction
    )
    assert return_value == volume




"""
if __name__ == '__main__':
    reglo_lib.pump_set_volume_at_flow_rate(
        pump_address=ADDRESS,
        flow_rate=[10.5, 11],
        channels=[1, 2],
        volume=[10, 10],
        direction=['K', 'J']
    )
    
"""
