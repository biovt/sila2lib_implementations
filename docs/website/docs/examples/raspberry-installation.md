---
sidebar_position: 2
---

# Raspberry Pi Installation
Many operating systems are available for Raspberry Pi, including Raspberry Pi OS, their official supported operating system. We will assume you have this image installed.

## Raspberry Pi OS Buster
**latest successful deploy: 30.1.2022**  
Debian Buster with Raspberry Pi Desktop

    Release date: January 11th 2021
    System: 32-bit
    Kernel version: 4.19
    Debian version: 10 (buster)
    shipps Python 2.7.16 that is activated by default. 
    Python3 is: Python 3.7.3


### With pip (recommended)
Get the repository and change into it. Make sure your system is up-to-date and create yourself a virtual environment within your project. Activate it to install all packages inside. Additionally, you can select which devices you want to install.
```
git clone https://gitlab.com/biovt/sila2lib_implementations
cd sila2lib_implementations

sudo apt update && sudo apt upgrade
python3 -m pip install --upgrade pip
python3 -m venv . && source bin/activate

pip install --editable .[dev]  # [dev] installs the development packages, too

# install desired devices, like BlueVary
pip install --editable sila2lib_implementations/BlueVary_v2/
```
:::tip
You can specify a version or a branch that you want to install on your system:
```
git clone -b 0.0.2 https://gitlab.com/biovt/sila2lib_implementations
git clone -b branch_name https://gitlab.com/biovt/sila2lib_implementations
pip install sila2lib-implementations==0.0.2 
```
:::

### With poetry
Get the repository and change into it. Make sure your system is up-to-date and you got 'Poetry' as package management. Configure Poetry to create its virtual environment in your project folder. This steps might be crutial and fix import errors. Install packages and activate the virtual environment.
```
git clone https://gitlab.com/biovt/sila2lib_implementations
cd sila2lib_implementations

sudo apt update && sudo apt upgrade
python3 -m pip install --upgrade pip
python3 -m pip install --upgrade poetry

# pip install .  # use simply this if below commands fail
python3 -m poetry config virtualenvs.in-project true
python3 -m poetry install
python3 -m poetry shell
```


## Raspberry Pi OS Bullseye
**latest successful deploy: :(**  
Raspberry Pi OS with desktop and recommended software

    Release date: January 28th 2022
    System: 32-bit
    Kernel version: 5.10
    Debian version: 11 (bullseye)
    Shipps python 3.10.

:::warning
Support for Bullseye is comming soon!
:::

## Only for developers: Deployment failures?
*Thats not true i guess -> remove it?*
Try to start each deployment in a new environment to not base on already fixed issues that you forget to document. There is no need to setup the device again. Simply create a new user (with sudo permissions), switch into its account and change into its home directory. Voilá, you will be re-set.
```
# old failed deploy environment
sudo adduser desired_name
sudo adduser desired_name sudo
su desired_name
# new user
python --version
python3 -m pip list
# start again :)
```
