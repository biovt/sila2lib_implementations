---
sidebar_position: 1
---

# First steps


## Installation

The repository is setup in a way that it can be installed conveniently as python package. Stable releases are available at the [Python Package Index (PyPI)](https://pypi.org/project/sila2lib-implementations/).
In case of any failures make sure to read the troubleshooting section and try to install the git repository. If you want to install on a raspberry Pi OS, make sure to read the [regarding article](./raspberry-installation)

```commandline
pip install sila2lib_implementations
```

## Running your first server

Explain why simulation/ why real


### Running your first server in simulation mode

The server start is implemented in a way to assist you with itself. Only issue this command and let you
walk trough its start :) \\todo: 
Lukas: this is somehow not working again? which command you have to issue? (real mode needs a serial to not end in errors
and simulation mode needs no units :/)  
```
python -m BlueVary_Service -a "127.0.0.1"
```  
\\todo: Show terminal recording of the execution


:::tip Arguments

When getting more proficient with the usage you can specify the _arguments_ directly:  
-a: IP address the device is available to  
-p: Port. The open port to communicate with the device  
-s: Starts the server in the simulation mode  
-t: Starts tests in the choosen mode  
-u: describes the unitID. In case for the DASGIP system there are 4 separate reactors. Each unit has its own
ID to identify it/address it.

:::

> Play around with addressing the different units? How would you do it?  
[Also you can pass multiple ports. Ports 50001-50004. The unitIDs are found by itself. Do not worry :)]

### Running your first server in real mode

```
python -m BlueVary_Service -a "127.0.0.1" -p 50002 -u "85 CO2_30753 O2_30700 HUM_30279 :I,BD" -t
```
> Note: The `-s` _argument_ is now missing.

## Unit tests = Device tests

Unit tests are supposed to test the functionality of a device or its implemented functions.
Therefore it is usefull before operating your device to test its functionality. There is a dry run available
to test the implementation and all the functions your device is capable to run during your fermentation.
Right before your experiment you can also start a wet/real run to see the device properly responding to its
control software

### Run a dry test = simulation mode
A dry run can be executed while spawning the simulation server (remember the `-s` _argument_). Additionally,
you can pass the `-t` _argument_ for testing.
To start the new BlueVary server for the second unit type:  
```
python -m BlueVary_Service -a "127.0.0.1" -p 50002 -u "85 CO2_30753 O2_30700 HUM_30279 :I,BD" -s -t
```

> Which unit you were acutally testing?   
[The 2nd. In case you did not know the answer please continue playing around a bit before starting the real test]


### Run a wet test = real mode
Run it with the server start. E.g.  
```
python -m BlueVary_Service -a "127.0.0.1" -p 50002 -u "85 CO2_30753 O2_30700 HUM_30279 :I,BD" -s -t
```

During the run you can check the server logs that are written to the terminal in real time. Cool right?
The cammands might be issues too quick but you can always scroll to check them and to see if everything works
as promised ;). The **- INFO-** statements are only to tell you (or more precise: the developers), which functions
are actually executed at the moment. Keep on looking for **- WARNING-** or  **-ERROR-** and reach out to the 
responsible persons to assist.  

:::info

The tests are designed to bring the device back to its pre-test settings. However, .csv files are
created with the first execution to backup the data before 'SET' new values. The location of these files is
`tests/Device/data-dump/timestamp.csv`. Files are stored using a timestamp that you can identify your current files easily.  
\\ todo:
link an example file here? [versioning]"(../tutorial-extras/manage-docs-versions.md)"

:::

Any problems or more information desired? Please check out the [developer documentation](../developers/installation) to get more insights.  
//todo: link to the correct section


### Operate your device

Make sure to connect all devices properly. Real tests have to be executed with precaution. 
However, a secure mode is implemented by default that prevents possible breakdowns hopefully. 
Do not rely on this please. Possible breakdowns can be overheating e.g..  
For safe testing these steps have to be performed BEFORE test execution:  
* Perform steps as usually to properly start up the device  
* Connect the serial cable to device and PC (USB port favorably)  

:::danger Ensure safe operation

Connect all devices properly before usage.

:::

**Congratulations, you are now an expert of using Sila. Why not joining us as a developer? ;)**
