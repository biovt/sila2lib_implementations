---
sidebar_position: 6
---

# Installation troubleshooting

Distros can be shipped using very old and outdated software. Problems might disappear already by updating your system. Also
check the [Installation requirements](#installation-requirements) once again.
On top, this troubleshooting section works best if the latest software is used anyways. It is assumed that all commands listed
here are executed in a linux terminal. Update your software: `sudo apt update && sudo apt upgrade`  

Poetry and Pip as package manager interfere sometimes in your use cases as `python -m command` execution depends on the
SYSTEM pip packages whereas `poetry run python -m command` depends on the poetry venv. This is confusing and irritating
a bit, especially when all commands need to be run explicitly with a longer command like `poetry run commands`. 
Anyhow, it is convenient to also have the packages pip installed in the poetry venv to bypass some trouble.
In case of failures, try to run commands from both packages and also compare their respective package version numbers with:
`poetry show` or `pip list`


## Troubleshooting for Debian based installations (e.g. on Raspberry Pis)

TLDR;
Sub dependency issues on a Raspberry Pi OS Buster (ships python 3.7.3) or OS Bullseye (python 3.9.2):
check the file `pyproject.toml` for comments and type:
``` {.sourceCode .console}
sudo apt-get install gcc musl-dev python3-dev openssl libssl-dev libffi-dev libxml2-dev libxslt1-dev libpq-dev

```

Install issues:
- pip failure to find `poetry` version  
  - pip might be outdated. Update it via: `pip install --upgrade pip`  
  - python2 pip might be used preferably by your (old, outdated) OS. Use and update pip3 for installation `pip3 install --upgrade pip`
  `pip install poetry`
- `poetry` command not found  
  - logout and login again (`source ~/.bashrc` not sufficient unfortunately)  
- command `bdist_wheel` not found. Probably `wheel` is not installed. Install it via:
  - `pip install --upgrade wheel setuptools`
- If that is failing all, try another way of installing poetry:  
`curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python3 - # --uninstall`

(Sub-)Dependency issues:  
- Build issues:  
  - Make sure to fulfill the dependencies. To ensure this type: `pip install --upgrade pip setuptools wheel`  
- install issues for the package `cryptography`    
  - this error is most probably linked to a grpcio issue listed in the section 'Troubleshooting during usage'
  please try first the steps listed there and re-try install. Try the next possibilities if this was not working:  
  - cryptography requires a (not too old) rust compiler (rustc). Make sure to have the rustc installed and additional dependencies by typing:  
`sudo apt-get install libssl-dev libffi-dev build-essential python3-dev cargo`  
  - If it is still not working pinn `cryptography` to an older version: `poetry add cryptography==2.3`  
  - If it is still not working install cryptography via pip as a walk around: `pip install cryptography`  
- install issues for the package `lxml`  
  - lxml requires 2 additional development packages. Install them by typing: `sudo apt-get install libxml2-dev libxslt1-dev`  
- install issues for the package `psycopg2-binary`  
  - Note: Only applies for the LHS_scheduler: psycopg2-binary requires: ` libpq-dev`  on Debian (Ubuntu, Mint) or
  `libpq-devel` on Centos/Fedora/Cygwin/Babun. Install them for Debian by typing: `sudo apt-get install libpq-dev`



## Troubleshooting during usage (points to installation issues finally)
- `ImportError: /lib/arm-linux-gnueabihf/libc.so.6: version 'GLIBC_2.33' not found`  
  - The installed grpcio version is not compatible with your current kernel (e.g. Buster). [Check this link](https://www.piwheels.org/project/grpcio/)
  to see all releases and the kernels they support. Adapt the versions in the command below. Example is shown for Buster:    
`pip3 install --no-cache-dir  --force-reinstall -Iv grpcio-tools==1.42.0`  
`pip3 install --no-cache-dir  --force-reinstall -Iv grpcio==1.42.0`  
  - This error can also be risen with a correct `grpcio` version if the `cryptography` is too new. Make sure to pinn it to 2.3:
  `poetry add cryptography==2.3`
- Error: `ImportError: /home/pi/.local/.../cygrpc.cpython-39-arm-linux-gnueabihf.so: undefined symbol: __atomic_exchange_8`  
  - Try a downgraded version of grpc like shown above for 'GLIBC_2.33'  
  - If it is still not working, uninstall `grpcio` and `grpcio-tools` and reinstall them with `apt`  
    - `pip uninstall grpcio grpcio-tools`  
    - `sudo apt install python3-grpcio python3-grpc-tools`  
  - If it is still not working, try it again with specified versions ([see this link](https://www.piwheels.org/project/grpcio/)
  to check kernel compatibility)
- Problems that might arise with newer python version