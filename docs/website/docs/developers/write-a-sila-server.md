---
sidebar_position: 5
---

# Write a new sila driver / or re-write an old one

## Setting up the environment

Clone the repository of [Sila_Python](https://gitlab.com/SiLA2/sila_python) and follow the instructions mentioned there for a sucessfull install. Install in a virtual environment to not pollute your system. If not updated recently, these are the commands to run:
```
git clone https://gitlab.com/SiLA2/sila_python
python3 -m venv .
source bin/activate
cd sila_python
pip install -e .[dev]
```
:::tip
Create a new project and do not use your old one. This way you are not polluting your drivers space and can check at the end of the project if the newly created driver is importable/installable.
:::

## Create folder structure

We assume your current working directory is the projects root (`path_to/sila_python`). Create a new directory `RegloICC` in the projects root and change into it. Create a new folder `RegloICC_project` there. Use your IDE if not on Linux since commands below are Linux only:

```
mkdir RegloICC
cd RegloICC
mkdir RegloICC_project
```

## Add feature implementation files

Features of the device are defined in the file format `.xml`. Based on the `.xml` file the code is generated (idea of code generators). Since RegloICC was already implemented at the chair, we can simply copy these files into our created directory `RegloICC_project`. Done :)
:::tip
The `.xml` is the heart of the whole device. Its now a perfect opportunity to quickly check it to fix typos or to add constraints or define errors.
:::

## Optional: Defined execution errors

It is good and desired practice to have defined execution errors in the `.xml` implementation. If they are not there copy past them before generating all or define new once, depending on the device /manual:
copy them in the respective feaure.xml at the bottom and define them at the function levels, that they get used actually. This is a proper .xml definition. Our device was lacking that and therefore we had to integrate it errorhandling is defined in the manual. serial command, possible error, (serial connection errors possible as addon to the existing device errors)

## Generate the new Device

All steps are well explained in the [Sila_Python documentation](https://gitlab.com/SiLA2/sila_python/-/blob/master/docs/server-implementation.md). Follow instructions there to generate necessary modules. Here we will implement the RegloICC as an example how the adaptation to Drivers of the BVT chair shall look like. This is a small example to illustrate the necessary adaptations to create the device with all features in a single run (`*.xml`):
```
sila2-codegen new-package -n [package-name] [-o output-directory] [feature-definition-files]
sila2-codegen new-package -n RegloICC -o RegloICC/RegloICCService RegloICC/RegloICC_project/*.xml
```
:::warning On error
The generation will not work if there are some incompabilities in the feature implementation files (`.xml`). Follow the error output and try to fix the lines that are causing the issues. Be patient. This step is crutial!
:::
On sucess, you shall be able to start the server already:
```
python -m RegloICC -s -v
python -m RegloICC
```
The server is first started in simulation mode (`-s` flag). The `-v` flag stands for 'verbose' so it provides you with a more detailed output.

:::warning On error
If the server start failed:
* the new device was probably not installed. Install it with (the path needs to point where the `setup.py` file is located):
```
pip install RegloICC/RegloICCService
```
:::

## Required folder structure

<details>
  <summary>Click to expand!</summary>

To make a small resumé: Your folder structure look like this after the generation:
```tree
RegloICC
├── RegloICC_project
│   ├── CalibrationServicer.sila.xml
│   ├── ConfigurationServicer.sila.xml
│   ├── DeviceServicer.sila.xml
│   ├── DriveControlServicer.sila.xml
│   ├── ParameterControlServicer.sila.xml
│   └── service_decription.json
└── RegloICCService
    ├── MANIFEST.in
    ├── RegloICC
    │   ├── feature_implementations
    │   │   ├── calibrationservicer_impl.py
    │   │   ├── configurationservicer_impl.py
    │   │   ├── deviceservicer_impl.py
    │   │   ├── drivecontrolservicer_impl.py
    │   │   ├── __init__.py
    │   │   ├── parametercontrolservicer_impl.py
    │   │   └── serial_connector.py
    │   ├── generated
    │   │   ├── calibrationservicer
    │   │   │   ├── calibrationservicer_base.py
    │   │   │   ├── calibrationservicer_client.pyi
    │   │   │   ├── calibrationservicer_errors.py
    │   │   │   ├── calibrationservicer_feature.py
    │   │   │   ├── CalibrationServicer.sila.xml
    │   │   │   ├── calibrationservicer_types.py
    │   │   │   └── __init__.py
    │   │   ├── client.py
    │   │   ├── configurationservicer
    │   │   │   ├── configurationservicer_base.py
    │   │   │   ├── configurationservicer_client.pyi
    │   │   │   ├── configurationservicer_errors.py
    │   │   │   ├── configurationservicer_feature.py
    │   │   │   ├── ConfigurationServicer.sila.xml
    │   │   │   ├── configurationservicer_types.py
    │   │   │   └── __init__.py
    │   │   ├── deviceservicer
    │   │   │   ├── deviceservicer_base.py
    │   │   │   ├── deviceservicer_client.pyi
    │   │   │   ├── deviceservicer_errors.py
    │   │   │   ├── deviceservicer_feature.py
    │   │   │   ├── DeviceServicer.sila.xml
    │   │   │   ├── deviceservicer_types.py
    │   │   │   └── __init__.py
    │   │   ├── drivecontrolservicer
    │   │   │   ├── drivecontrolservicer_base.py
    │   │   │   ├── drivecontrolservicer_client.pyi
    │   │   │   ├── drivecontrolservicer_errors.py
    │   │   │   ├── drivecontrolservicer_feature.py
    │   │   │   ├── DriveControlServicer.sila.xml
    │   │   │   ├── drivecontrolservicer_types.py
    │   │   │   └── __init__.py
    │   │   ├── __init__.py
    │   │   └── parametercontrolservicer
    │   │       ├── __init__.py
    │   │       ├── parametercontrolservicer_base.py
    │   │       ├── parametercontrolservicer_client.pyi
    │   │       ├── parametercontrolservicer_errors.py
    │   │       ├── parametercontrolservicer_feature.py
    │   │       ├── ParameterControlServicer.sila.xml
    │   │       └── parametercontrolservicer_types.py
    │   ├── __init__.py
    │   ├── __main__.py
    │   └── server.py
    └── setup.py
```
</details>

## Update the server.py implementation

By now all the setup was done automatically for an example server. We will now insert some code to adapt it to our needs. The server.py file is located here: `RegloICC/RegloICCService/RegloICC/server.py`

### Server names etc.

As the 'TODO' is telling us, we need to make the example server ours! In the `RegloICC/RegloICC_project/service_description.json` there are the old details we could use:

```python
class Server(SilaServer):
    def __init__(self):
        # TODO: fill in your server information
        super().__init__(
            server_name="RegloICCService",
            server_type="Pump",
            server_version="0.0",
            server_description="This is a RegloICC Service",
            server_vendor_url="https://www.epe.ed.tum.de/biovt/startseite/",
        )
```

:::tip
Run the server after every section you completed. By this you make sure to identify erros as early as possible. It may be necessary to re-install the device to make changes appear:
```
pip install RegloICC/RegloICCService
python -m RegloICC
```
:::

## setup.py: Server properties, serial connector and device servicer

Spoiler: This section will be difficult :/  
TLDR:  
Make your your 'setup.py' looks like this afterwards:
<details>
  <summary>Click to expand</summary>

```python
from sila2.server import SilaServer

from .feature_implementations.calibrationservicer_impl import CalibrationServicerImpl
from .feature_implementations.configurationservicer_impl import ConfigurationServicerImpl
from .feature_implementations.deviceservicer_impl import DeviceServicerImpl
from .feature_implementations.drivecontrolservicer_impl import DriveControlServicerImpl
from .feature_implementations.parametercontrolservicer_impl import ParameterControlServicerImpl
from .generated.calibrationservicer import CalibrationServicerFeature
from .generated.configurationservicer import ConfigurationServicerFeature
from .generated.deviceservicer import DeviceServicerFeature
from .generated.drivecontrolservicer import DriveControlServicerFeature
from .generated.parametercontrolservicer import ParameterControlServicerFeature

# self made imports
import logging
import sys
from .feature_implementations.serial_connector import SharedProperties  # serial connector
from .generated.deviceservicer import SerialConnectionError

class Server(SilaServer):
    def __init__(self, device_id: str, device_command: str = '1xS\r\n', simulation_mode: bool = True):
        # TODO: fill in your server information
        super().__init__(
            server_name="RegloICCService",
            server_type="Pump",
            server_version="1.0",
            server_description="This is a RegloICC Service",
            server_vendor_url="https://www.epe.ed.tum.de/biovt/startseite/",
        )
        self.properties = SharedProperties(
            device_id=device_id,
            device_command=device_command,
            simulation_mode=simulation_mode
        )
        logging.info(f'Server started in {"simulation" if self.properties.simulation_mode == True else "real"} mode.')
        try:
            self.properties.connect_serial()
        except (SerialConnectionError, Exception) as e:
            logging.error(e)
            self.stop()
            sys.exit(1)
        self.calibrationservicer = CalibrationServicerImpl(self.properties)
        self.configurationservicer = ConfigurationServicerImpl(self.properties)
        self.deviceservicer = DeviceServicerImpl(self.properties, self.child_task_executor)
        self.drivecontrolservicer = DriveControlServicerImpl(self.properties)
        self.parametercontrolservicer = ParameterControlServicerImpl(self.properties)

        self.set_feature_implementation(CalibrationServicerFeature, self.calibrationservicer)
        self.set_feature_implementation(ConfigurationServicerFeature, self.configurationservicer)
        self.set_feature_implementation(DeviceServicerFeature, self.deviceservicer)
        self.set_feature_implementation(DriveControlServicerFeature, self.drivecontrolservicer)
        self.set_feature_implementation(ParameterControlServicerFeature, self.parametercontrolservicer)
```
</details>

### serial connector and its shared properties
The serial connector shall be the same for all devices. So use the one of BlueVary e.g. `BlueVary_v2/BlueVary_Service/feature_implementations/serial_connector.py`. Copy it to your RegloICC: `RegloICC/RegloICCService/RegloICC/feature_implementations/serial_connector.py`. We also need to import some packages in order to use the 'serial_connector'. 
<details>
  <summary>Click to expand</summary>

```python
# self made imports
import logging
import sys
from .feature_implementations.serial_connector import SharedProperties  # serial connector
from .generated.deviceservicer import SerialConnectionError
```
</details>

**shared properties**
With this implementation we are required to define some 'shared properties' in the 'setup.py' as well. The class init we need to adapt accordingly!
```python
class Server(SilaServer):
    def __init__(self, device_id: str, device_command: str = '1xS\r\n', simulation_mode: bool = True):
        # TODO: fill in your server information
```

<details>
  <summary>Click to expand</summary>

```python
        self.properties = SharedProperties(
            device_id=device_id,
            device_command=device_command,
            simulation_mode=simulation_mode
        )
        logging.info(f'Server started in {"simulation" if self.properties.simulation_mode == True else "real"} mode.')
        try:
            self.properties.connect_serial()
        except (SerialConnectionError, Exception) as e:
            logging.error(e)
            self.stop()
            sys.exit(1)
```
</details>


:::info
If you want to implement different devices you need to adapt the 'device_command'. The device command is a string that is sent to the device. The device should send us as response its 'serial number'. We will use it to address the device in the future. Every device has its specific command/sting for this reuest. You need to look it up in the old files or the manual of the device.  
['%sxS\r\n' is the expression whereas '%' we have to fill with an exact address we want. In this case address 1: '1xS\r\n'] 
:::

### Device servicer

At the end of our 'setup.py' we define the servicer the device is capable of. TODO: Add some more explanation here.
<details>
  <summary>Click to expand</summary>

```python
        self.calibrationservicer = CalibrationServicerImpl(self.properties)
        self.configurationservicer = ConfigurationServicerImpl(self.properties)
        self.deviceservicer = DeviceServicerImpl(self.properties, self.child_task_executor)
        self.drivecontrolservicer = DriveControlServicerImpl(self.properties)
        self.parametercontrolservicer = ParameterControlServicerImpl(self.properties)

        self.set_feature_implementation(CalibrationServicerFeature, self.calibrationservicer)
        self.set_feature_implementation(ConfigurationServicerFeature, self.configurationservicer)
        self.set_feature_implementation(DeviceServicerFeature, self.deviceservicer)
        self.set_feature_implementation(DriveControlServicerFeature, self.drivecontrolservicer)
        self.set_feature_implementation(ParameterControlServicerFeature, self.parametercontrolservicer)
```
</details>


:::warning
Since you imported some new packages, you probably miss the installation for them. You will get a respective error output that tells you which package you need to install manually. Install them with 'pip install' e.g.:
```
pip install wheel
pip install pyserial
```
:::

### Necessary adaptions in __main__.py

In our recently adapted `def __init__()` in the `setup.py`, we defined 'device_id: str'. However, we will run into problems since we did not define default values for the 'device_id'. This error you will encounter then:  
`TypeError: __init__() missing 1 required positional argument: 'device_id'`  
We want to set 'device_id' via command line. So, we need to continue with the file `__main__.py` in `RegloICC/RegloICCService/RegloICC/__main__.py`:  
**i didnt get this section entirely, whats missing here?**
In `__main__.py`, the server object requires specified 'device_id' in order to identify the correct port for device connection, the user has to supply these as cmd_args else. Otherwise there is this error raised:
`TypeError: __init__() missing 1 required positional argument: 'device_id'`
therefore we need to add the respective add_argument options in the `__main__.py` file
and on top we need to parse the attributes to the server object:
**i didnt get this section entirely, whats missing here?**

We do not simply want to start a server. We want to define a mode in which it starts (simulation_mode) and also we want to define the unit that we want to address. The unit refers e.g. to 1 channel out of 4. So you do not want to always use 4 pumps with the same flow values etc., right. So we need to adapt our server settings:  
```python
def start_server(args):
    server = Server(device_id=unit_id, simulation_mode=simulation_mode)
```
```python
    parser.add_argument("-s", "--simulation-mode", action="store_true", help="Specify if you want to start in simulation mode.")
    parser.add_argument("-u", "--unit-id", type=str, default="",
                        help="Specify the unit that shall be connected to this server.")
```
If you are not sure where to add the lines, check the final file in our repository as comparison.

:::warning
on serial device errors:
`ERROR:root:No serial ports found on this system. Make sure your device is connected!`
As it says there is no device connected. Try out to run the simulation mode with (-s flag):
`python -m RegloICC -s`
:::


### Sharing the included properties with all the servicers

Sharing is caring. The servicer also need to know whats up. We need to pass the 'properties' to ALL the servicers. This we do in the `def __init()`. Do not forget the imports. One example for the calibration_servicer:
```python
import logging
from .serial_connector import SharedProperties
```
```python
class CalibrationServicerImpl(CalibrationServicerBase):
    def __init__(self, properties: SharedProperties):
        self.properties = properties
```
This needs to be done to all servicers. If you are not sure what to do, take an example in our repository how a recently written servicer has to look like in the end.  
Fancy stuff: observable commands are difficult to handle. Let your supervisor write them for you (child_task_executor for observable comands needed). In RegloICC case obnly the device_servicer has observable properties.



## Feature implementation - add functionality

The scaffold is done by now. However, we are still missing all the functionality. The functions were generated with the correct names already - but empty. We have to implement them - or use the old implementation in our case. In case you have no old implementation, you need to look up the serial commands in the device manuals. You can use a function from here as template and adapt the serial commands and return names etc.  
In our case we will demonstrate a function for the `calibration_servicer` again. As templates, use the old implementations of the servicer:
`sila2lib_implementations/RegloICC/RegloICCService/CalibrationServicer/CalibrationServicer_real.py` and `sila2lib_implementations/RegloICC/RegloICCService/CalibrationServicer/CalibrationServicer_servicer.py` 

once the structure is done, you can finally copy, paste all the scaffold :) :) :)
only need to change error types, byte encodings, commands, resonses: this is in sum not trivial unfortunately. Use the our RegloICC implementation in the repository or Bluevary as template.
An example is shown here, too:

<details>
  <summary>Click to expand</summary>

```python
class CalibrationServicerImpl(CalibrationServicerBase):
    def __init__(self, properties: SharedProperties):
        self.properties = properties

    def StartCalibration(
        self, Calibrate: str, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> StartCalibration_Responses:
        logging.debug(
            "StartCalibration called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode('%sxY\r' % Calibrate)
        read = ''
        try:
            if self.properties.simulation_mode:
                # Todo: Implementation for sim_mode
                read = 'Simulation: Started Calibration'
                return StartCalibration_Responses(
                    StartCalibrationStatus=read
                )
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = self.properties.serial_connector.ser.readline().rstrip()
                    read = bytes.decode(read)
                    read = str(read)
                except Exception as e:
                    logging.error("Error writing Start command")
                    raise BaseException
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('Calibration started on channel %s: %s' % (Calibrate, read))
            return StartCalibration_Responses(
                StartCalibrationStatus=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.error(e)

    def CancelCalibration(
        self, Calibrate: str, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> CancelCalibration_Responses:
        logging.debug(
            "CancelCalibration called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode('%sxZ\r' % Calibrate)
        read = ''
        try:
            if self.properties.simulation_mode:
                # Todo: Implementation for sim_mode
                read = 'Simulation: Started Calibration'
                return CancelCalibration_Responses(
                    CancelCalibrationStatus=read
                )
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = self.properties.serial_connector.ser.readline().rstrip()
                    read = bytes.decode(read)
                    read = str(read)
                except Exception as e:
                    logging.error("Error writing Cancel command")
                    raise BaseException
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('Calibration canceled on channel %s: %s' % (Calibrate, read))
            return CancelCalibration_Responses(
                CancelCalibrationStatus=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.error(e)
```
</details>            


## Testing

Add a simple test to see if it is working.
```python
import RegloICC
client = RegloICC.Client('127.0.0.1', 50052)
response = client.CalibrationServicer.StartCalibration(Calibrate='1')
print(response)
```
It is assumend to name the file like this: `RegloICC/RegloICCService/RegloICC/RegloICC_Test.py`. Start a server in one terminal and in the other you start a test. Check their outputs for failures and to see the logging statements:
```
python -m RegloICC -s -v
python RegloICC/RegloICCService/RegloICC/RegloICC_Test.py
```