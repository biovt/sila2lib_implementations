---
sidebar_position: 4
---

# Contribution

## Coding style guidelines

Before committing please make sure to run the checks to push nice and working code. Or integrate the pre-commit tool to aid you in reminding of that matter ;)  
Tests can be executed on demand (and defined individually) by executing: `run-tests`. It is not supposed to automate tests and enable on pushing only on success.

### Pre-commit hooks

There are hooks configured that can be used with the pre-commit tool. Pre-commit is executed automatically with
EACH commit. This results in reliable, nice code but might be annoying sometimes (check the `.pre-commit-config.yaml` to turn it off temporarily, if desired).  
The `.pre-commit-config.yaml` file contains the configration of which hooks to execute. The tools that are executing the hooks are configured itself in the `pyproject.toml` file. To activate pre-commit, install the hooks via:
```
pre-commit install
pre-commit autoupdate
```

:::warning
Make sure to install within your activated virtual environment.
:::

### Manual code checks  

You can run the script `run-formatting` to apply some auto formatting (that you can define by yourself in the file, too btw.). To verify that your code is executable without issues run some tests to see where conflicts or bugs might arise. Run the `run-tests` to execute all tests or only execute specific ones you are working on.

### Utilized tools

[black](https://black.readthedocs.io/): Code formatter  
[isort](https://pycqa.github.io/isort/): Import sorting  
[flake8](https://flake8.pycqa.org/): Checks various code problems  
[pytest](https://docs.pytest.org/): Testing suite  
[pytest-cov](https://github.com/pytest-dev/pytest-cov): Measures pytest's code coverage  
[tox](https://tox.wiki/en/latest/): Optional: can be used with pyenv e.g. to test the release for different python version  
old used tools: flake8, flakehell, commitizen

:::tip

Sometimes it is neccessary to commit and push quickly. You can bypass the enforced checks by pre-commit temporarily (commit base) or permanently (config):
```
git commit --no-verify -m "commit message" file_to_commit optional_other_file
git config core.hooksPath /dev/null  # disable 
git config --global --unset core.hooksPath  # enable
```

:::


## Unit tests

The default execution of the pytest command is regulated in the `tests/pytest.ini` file, where some markers are defined for default executions or safe executions i.a..  
**How do I run simulation tests?**  
Tests are executed by default in simulation mode. See the [Quickstart - first steps](#quickstart---first-steps) section to get comfortable.  
**How do I run "real" tests?**  
Real tests are divided into tests that can be executed safely or not. Tests that can be executed safely are those that
do not require a special device set up like cooling- or moving units. By this it is tried to assure no device breakdown in case
crucial steps have been forgotten to use a device safely.

## CI/CD

**Which tests are run in the GitLAb CI/CD pipeline and when?**  
Whenever we want...  
The pipeline routines are defined in the `.gitlab-ci.yml` file and can be changed there.

### gitlab-ci-local

[gitlab-ci-local](https://github.com/firecow/gitlab-ci-local) is a tool that allows you to run a CI pipeline locally. You can choose between a shell executor (only when on linux OS) or a docker container. A docker container is recomended since the gitlab runners also deploy in docker containers. You will be independent of local installations. In the gitlab settings you can also setup local runners, to save pipeline minutes. However, you are still required to push your code always. This can be annoying for the git history if you need 10 commands to fix a small issue because you were required to commit and push always. Use this tool if this is bothering you, too.  
The tool can be started from the terminal. Make sure to be in the projects root for the execution:
```
gitlab-ci-local
```
The documentation is very nice on the repo and it is well maintained. Browse arround when facing issues. Btw.: environment variables can be defined in your HOME directory: `~/.gitlab-ci-local/variables.yml`. This is an example of how to define variables:
```
global:
  BIOREACTOR48_IP: '127.0.0.1'
  BIOREACTOR48_PORT: '50001'
  BIOREACTOR48_SIMULATION_MODE: 'TRUE'

  PRESENS_IP: '127.0.0.1'
  PRESENS_PORT: '50004'
  PRESENS_SIMULATION_MODE: 'true'

```

:::tip
In the `tests/config.py` you may need to adapt the path 'PROJECT_ROOT' when running gitlab-ci-local.
:::

### Advice

The execution will need some time. Save ressources and time by not running always all jobs, stages or tests. Simply comment out the sections while bugfixing and job will run very fast.

### Git version tags

In order to be able to install or reset to a specific version, git version tags are set. They can be set manually or be triggered in the pipeline. It is reccomended to use them only in the master branch. If you push/work in the master directly it is also reccomended to not set the tags for every push. Until a certain point we can collect changes and bump them together to a new version.

#### Automatic git version tags CI setup

You need to generate a ssh key (`ssh-keygen`) and copy the PRIVATE key in a variable (gitlab.com:Settings->CI->Variables). Name the variable as it is needed in the script (in our case `SSH_PRIVATE_KEY`). Often forgotten: You also need to set a `Deploy key`. This can be done in the gitlab.com:Settings->repository->Deploy Key: create a new Deploy key and name it `semver`. The PUBLIC key you paste into the other field. The implementation is done like [here](https://threedots.tech/post/automatic-semantic-versioning-in-gitlab-ci/). Check it out for more information or errors.

:::tip
If this fails you can try if you can connect to gitlab directly from your terminal. To see if at least the PRIVATE key is recognized and the one on your local machine as well:
```
ssh -T git@gitlab.com
```
:::

#### Manual git version tags

In your local environment you can also set tags manually and push them to the repo if desired (or you are allowed to). The steps are to: 1) list existing local tags, 2) list all tags in the repo, 3) create a new tag (here i create 0.0.1), 4) push it. The check for existing keys is important since, the are unique. So you can not use the same one again. Excelent advice is [here](https://devconnected.com/how-to-list-git-tags/)
```
git tag -n
git fetch --all --tags 
git tag -a 0.0.1 -m "before CI"
# Made a mistake? delete (-d) a key like this: git tag -d 0.0.1 
git push --tags
```


## Implementation of new devices

Please refer to the [Sila_python](https://gitlab.gwdg.de/niklas.mertsch/sila2-redo/-/tree/master) project for implementation of new devices. In case of extension or changes of existing devices keep in mind to also include the changes in the respective .xml files.


## Python version compability

I believe you your code is running excellently on your local machine. Forunately, we can ship our software to customers and there the issues will start. Concludingly, we have to try our best in this regard.

### tox

what does the tox say?  
Different plattforms and operating systems have very likely different python versions installed. To aviod errors on execution on their systems we use tox to 'build' and test our code on different python interpreters first.
tox is the most easy command to run. Guess how?
```
tox
```
Tox is using your systems python versions. But do not worry, you will not need to mess arround with 4 different python versions at the same time. You can use `pyenv` for that.

### Pyenv

Pyenv manages your local python installations in a convenient fashion. With `pyenv versions` you can list your installed versions. The '*' symbol highlights which interpreter you are using currently. For your virtual environment you can set activate or change multiple interpreters (`pyenv local desired_version`).

```
pyenv versions
pyenv local 3.8.10 3.9.2 3.7.5 3.10.0
```
