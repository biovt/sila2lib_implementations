---
sidebar_position: 1
---

# Project structure

## Directory tree

```
└── sila2lib_implementations
    ├── sila2lib_implementations
    │   ├── Device
    │   │   └── Device_project
    │   │   │   └── DeviceServicer.sila.xml
    │   │   │   └── service_description.json
    │   │   │   └── SiLAFramework.proto
    │   │   └── DeviceService
    │   │   │   └── DeviceServicer
    │   │   │   │   └── DeviceServicer_servicer.py
    │   │   │   │   └── DeviceServicer_real.py
    │   │   │   │   └── DeviceServicer_simulation.py
    │   │   │   │   └── DeviceServicer_default_arguments.py
    │   │   │   │   └── __init__.py
    │   │   │   └── DeviceService_client.py
    │   │   │   └── DeviceService_server.py
    │   │   │   └── __init__.py
    │   │   └── __init__.py
    │   └── __init__.py
    ├── tests
    │   ├── Device
    │   │   └── test_device.py
    ├── README.md
    ├── LICENSE
    ├── pyproject.toml
    └── setup.py
```

## Folder explanation

`sila2lib_implementations`: Projects root  
`sila2lib_implementations`: All Device implementations  
`Device`: A device that is implemented  
`Device_project`: Source code directory of the device implementation  
`DeviceServicer.sila.xml`: Feature implementation for the Device    
`service_description.json`:  
`SiLAFramework.proto`:  
`DeviceService`: Code generated directory for the device  
`DeviceServicer`: Contains the Device implementations for different modi (like simulation or real mode)
as well as a servicer that starts either the simulation- or real mode.  
`DeviceServicer_default_arguments` contains the default responses for the simulation mode.  
`DeviceService_client.py`: Sila server implementation specific for the device  
`DeviceService_client.py`: Sila client implementation specific for the device  
`tests`: Unit tests for the devices listed above


## Additional folder description

The folder `sila2lib_implementations` contains the listed devices. Each device itself consists of a `Device_project` folder
where the devices were defined originally (in the file format `.xml`). These .xml-files are written according to the
SiLA2 schema available at [sila_base](https://gitlab.com/SiLA2/sila_base) and are called FDL-files (**F**eature **D**efinition **L**anguage).
Based on these files, the sila2lib-codegenerator (Deprecated, In archived branch of [sila_python](https://gitlab.com/SiLA2/sila_python))
generated all services in the separate directory `DeviceService`. Within the `DeviceService` directory, server and clients have been
generated. Depending on the device's function there can be additional features, named Service, Provider, or Controller.
To learn more about the nomenclature of SiLA2 read the [documentation](https://sila2.gitlab.io/sila_base/) which
contains links to the specification parts A and B. These features account for the main functionality
of the device (e.g. providing of sensor data, control of actors). Each feature contains an implementation for a simulation- or real mode, as well as some
default return arguments to enable a meaningful simulation run. The gRPC folders within the `DeviceService` folder can be ignored, usually.
Unit tests for (almost) all devices can be found in the `test` folder (for details see 'Developer note').
