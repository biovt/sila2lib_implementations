---
sidebar_position: 8
---

# Permanent setup
## Static IP addresses
Contact your system or network administrator before assigning static IP-addresses. Setting static IP-addresses to 
addresses that are already in use may lead to undesired consequences. The following section shows how to set up a static
IP-address for a Raspberry Pi 4 running on Raspian 10 (Buster).

Get the default router (ROUTERIP) using:  

``ip r | grep default``

Get the current DNS nameserver (DNSIP) using:  

``sudo nano /etc/resolv.conf``

Modify the dhcpcd.conf configuration file using:  

``sudo nano /etc/dhcpcd.conf``

Add the following lines to set the file and enter the desired static IP (STATICIP) for the ethernet connection eth0 
(NETWORK):  
 
``
interface <NETWORK>
static ip_address=<STATICIP>/24
static routers=<ROUTERIP>
static domain_name_servers=<DNSIP>
``

Restart the Raspberry PI using:  

``sudo reboot -n``

After the restart check that the configuration worked. The following command should return your new static IP address:  

``hostname -I``

## Registering the SiLA 2 Server as Service with systemd
### Create a shell script that starts the server with the desired options
The shell script will be called by the service file. In this file the command to start the server is defined. Additional 
flags to specify the IP address and the port the server should be running on can be added using the available flags of 
that SiLA Server. An example shell script can be found in the applications folder under:
  
``sila2lib_implementations/applications/run_sila_server.sh``

Test the shell script by invoking:  

``
cd <path>/<to>/<shell>/<script>
sh script.sh
``

### Create a .service file and configure it accordingly. 
To deploy a SiLA 2 Server in a production environment, we can set up a SiLA Server as a system service and assign a 
static IP to it. On Linux systems we can do thi using systemd. Systemd as an init program that runs scripts and system 
reboot, manages them and restarts them if necessary. To register a SiLA Server as a service with systemd, a .service 
file must be created. To do so you can use vi or vim for example:  

``vi /home/pi/Desktop/DASGIP_server.service``

An example file is provided and can be found in:  

 ``sila2lib_implementations/applications/sila_server.service``
 
The .service file can be created with any text editor. Upon creation, the file is moved to the systemd directory using:    

``sudo cp /home/pi/Desktop/DASGIP_server.service /lib/systemd/system/``

If a file with this name has previously been registered with systemd, the systemd daemon needs to reload the director using:    

``systemctl daemon-reload``

To start or restart a service manually, the following command is used:  

``
sudo systemctl start DASGIP_server.service
sudo systemctl restart DASGIP_server.service
``

The status of the service can be viewed with:  

``sudo systemctl status DASGIP_server.service``

A service can be stopped using this command:  

``sudo systemctl stop DASGIP_server.service``

The service automatically writes the server logs to journalctl. The logs can be accessed using:  

``journalctl -e -u DASGIP_server.service``

Once the service has been configured properly it must be enabled. Enabling the service will start the script 
automatically on system reboot. use the following command to enable the service:
  
``sudo systemctl enable DASGIP_server.service``

Restart the Raspberry Pi 4 using ``sudo reboot -n`` and check if the service started successfully using the status 
command. Check if you can access the server using a SiLA Client and you're all set! 
