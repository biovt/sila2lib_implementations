---
sidebar_position: 3
---

# Package management

The packagemanagement as well as all its features like building and publishing are supposed to be accomplished with [Poetry](https://python-poetry.org/). Alternatively, pip or different methods listed [here](https://packaging.python.org/en/latest/tutorials/installing-packages/) can be used for installation. A `setup.py` and `requirements.txt` file is provided for these cases. These dependencies also get updated via the CI pipeline while publishing a new version. However, we would like to stay with Poetry.

Mention the `Poetry run command` issue vs. `python -m command`
poetry install --no-dev


## Pyproject.toml - Package harbourage

Next to the listing of dependencies and development dependencies, the `.toml` file also harbours build information and settings for the delevopment tools. These settings are parsed by the hooks if you use them. Hooks can be managed with `pre-commit` e.g. [link the section here].


> check commands there for old dependencies needed by outdated software as it accounts for Rasberry Pi OS Buster e.g. [compabilities](https://www.piwheels.org/project/grpcio/)
downgraded versions for Raspberry pi OS 'Buster': If necessary un-/comment:  
grpcio-tools = "==1.42.0"  
grpcio = "==1.42.0"  
cryptography = "==2.3"


## Useful Poetry commands
```
poetry shell
poetry update
poetry lock
poetry install
poetry install --no-dev
poetry run
poetry env list
poetry env info
```
:::tip Export dependency
```
poetry export --without-hashes -f requirements.txt -o requirements.txt
poetry export --dev --without-hashes --format=requirements.txt -o requirements-dev.txt
```
:::
\\do there exist pyenv export, too?


## Tips on Poetry usage

* Poetry does install the current project in editable mode on poetry install (like pip install -e .)
* After deleting a dependency manually in the .toml, run the following command to remove all subdependencies in the venv. Doublecheck if you did not remove dependencies that are actually required (happend to me once):
```
poetry install --remove-untracked
```
* In case of errors there is always the possibility to `pip install packages` in the existing poetry venv.
