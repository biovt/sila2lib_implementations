---
sidebar_position: 1
---

# Intro 
WIP

## SiLA 2
WIP

### The SiLA Consortium

The SiLA Consortium is non-profit. It gathers life science experts and mobilizes the industry while developing free & open system communication and data standards to connect scientists with the data that matters.

SiLA and its members provide a framework for the exchange, integration, sharing, and retrieval of electronic laboratory information. These standards define how information is transported from one system to another. Setting the language, structure and data types required for seamless integration, SiLA supports the management, delivery, and evaluation of laboratory services. SiLA is recognized as most commonly used in the world.

Please check out the consortiums main [website](https://sila-standard.com/).  

![Sila logo](/img/sila-python-logo.png)