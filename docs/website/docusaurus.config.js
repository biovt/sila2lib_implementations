// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Sila2lib Implementations',
  tagline: 'A package of SiLA drivers for various devices at TUM-BVT',
  url: 'https://pages.gitlab.io',
  baseUrl: '/sila2lib_implementations/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'biovt', // Usually your GitHub org/user name.
  projectName: 'sila2lib_implementations', // Usually your repo name.

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl: 'https://gitlab.com/biovt/sila2lib_implementations',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl: 'https://gitlab.com/biovt/sila2lib_implementations',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        // title: 'Sila',
        logo: {
          alt: 'My Site Logo',
          src: 'img/sila-python-logo.png',
        },
        items: [
          {
            type: 'doc',
            docId: 'developers/installation',
            position: 'left',
            label: 'Developer',
          },
          {
            type: 'doc',
            docId: 'examples/first-steps',
            position: 'left',
            label: 'Examples',
          },
          {
            type: 'doc',
            docId: 'intro',
            position: 'left',
            label: 'Documentation',
          },
          // {to: '/blog', label: 'Blog', position: 'left'},
          {
            href: 'https://gitlab.com/biovt/sila2lib_implementations',
            label: 'GitLab',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'Tutorial',
                to: '/docs/intro',
              },
              {
                label: 'Sila2-manager',
                to: 'https://sila2-manager.readthedocs.io/en/latest/',
              },
            ],
          },
          {
            title: 'Community',
            items: [
              {
                label: 'Sila consortium',
                href: 'https://sila-standard.com/about-us/',
              },
              {
                label: 'Sila Python',
                href: 'https://gitlab.com/SiLA2/sila_python',
              },
              {
                label: 'All Sila repositories',
                href: 'https://gitlab.com/SiLA2',
              },
            ],
          },
          {
            title: 'More',
            items: [
              {
                label: 'Blog',
                to: '/blog',
              },
              {
                label: 'GitLab',
                href: 'https://gitlab.com/biovt/sila2lib_implementations',
              },
              {
                label: 'PyPi',
                href: 'https://pypi.org/project/sila2lib/',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Institute of Bioprocess Engineering, Technical University of Munich.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
