---
slug: aims-2022
title: Project aims for 2022
authors: [lukas, felix]
tags: [Sila, aims, '2022']
---

# Project aims for 2022

* Support for Raspberry Pi OS Bullseye
* Update project to use Python 3.10
* Add documentation
* Convert old Sila implementation into new one 