<?xml version="1.0" encoding="utf-8" ?>
<Feature SiLA2Version="1.0" FeatureVersion="1.0" Originator="org.silastandard" Category="examples"
         xmlns="http://www.sila-standard.org"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://www.sila-standard.org https://gitlab.com/SiLA2/sila_features/raw/master/schema/FeatureDefinition.xsd">
    <Identifier>OffgasServicer</Identifier>
    <DisplayName>Offgas Servicer</DisplayName>
    <Description>
        Control a DASGIP offgas module. Enables read and write operations for various parameters. 
        By Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 20.05.2019
    </Description>
    <Command>
        <Identifier>GetCTRPV</Identifier>
        <DisplayName>Get CTR PV</DisplayName>
        <Description>Get offgas carbondioxide transfer rate process value</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentCTRPV</Identifier>
            <DisplayName>Current CTR PV</DisplayName>
            <Description>Current offgas carbondioxide transfer rate process value</Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>GetFPV</Identifier>
        <DisplayName>Get F PV</DisplayName>
        <Description>Get offgas total flow process value</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentFPV</Identifier>
            <DisplayName>Current F PV</DisplayName>
            <Description>Current offgas total flow process value</Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>GetOTRPV</Identifier>
        <DisplayName>Get OTR PV</DisplayName>
        <Description>Get offgas oxygen transfer rate process value</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentOTRPV</Identifier>
            <DisplayName>Current OTR PV</DisplayName>
            <Description>Current offgas oxygen transfer rate process value</Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>GetRQPV</Identifier>
        <DisplayName>Get RQ PV</DisplayName>
        <Description>Get offgas RQ process value</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentRQPV</Identifier>
            <DisplayName>Current RQ PV</DisplayName>
            <Description>Current offgas RQ process value</Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>GetXCO2PV</Identifier>
        <DisplayName>Get XCO2 PV</DisplayName>
        <Description>Get offgas CO2 concentration process value</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentXCO2PV</Identifier>
            <DisplayName>Current XCO2 PV</DisplayName>
            <Description>Current offgas CO2 concentration process value</Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>GetVCTPV</Identifier>
        <DisplayName>Get VCT PV</DisplayName>
        <Description>Get offgas cumulated CO2 transfer rate</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentVCTPV</Identifier>
            <DisplayName>Current VCT PV</DisplayName>
            <Description>Current offgas cumulated CO2 transfer rate</Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>GetVOTPV</Identifier>
        <DisplayName>Get VOT PV</DisplayName>
        <Description>Get offgas cumulated O2 transfer rate</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentVOTPV</Identifier>
            <DisplayName>Current VOT PV</DisplayName>
            <Description>Current offgas cumulated O2 transfer rate</Description>
            <DataType>
                <Basic>Real</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>GetState</Identifier>
        <DisplayName>Get State</DisplayName>
        <Description>Get controller state. Controller state (Off, On, Error).</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentState</Identifier>
            <DisplayName>Current Controller State</DisplayName>
            <Description>Current controller state value of the offgas module. Controller state (Off, On, Error).</Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>GetType</Identifier>
        <DisplayName>Get Function Type</DisplayName>
        <Description>Get function type.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentType</Identifier>
            <DisplayName>Current Function Type</DisplayName>
            <Description>Current function type value of the offgas module.</Description>
            <DataType>
                <Basic>String</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>GetAvailable</Identifier>
        <DisplayName>Get Function Availability</DisplayName>
        <Description>Get function availability.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentAvailable</Identifier>
            <DisplayName>Current Function Availability</DisplayName>
            <Description>Current function availability value of the offgas module.</Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>GetName</Identifier>
        <DisplayName>Get Function Name</DisplayName>
        <Description>Get function name.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentName</Identifier>
            <DisplayName>Current Function Name</DisplayName>
            <Description>Current function name of the offgas module.</Description>
            <DataType>
                <Basic>String</Basic>
            </DataType>
        </Response>
    </Command>
    <Command>
        <Identifier>GetVersion</Identifier>
        <DisplayName>Get Function Version</DisplayName>
        <Description>Get function model version number.</Description>
        <Observable>No</Observable>
        <Parameter>
            <Identifier>UnitID</Identifier>
            <DisplayName>UnitID</DisplayName>
            <Description>
                The UnitID of the addressed reactor
            </Description>
            <DataType>
                <Basic>Integer</Basic>
            </DataType>
        </Parameter>
        <Response>
            <Identifier>CurrentVersion</Identifier>
            <DisplayName>Current Function Version</DisplayName>
            <Description>Current function model version number of the offgas module.</Description>
            <DataType>
                <Basic>String</Basic>
            </DataType>
        </Response>
    </Command>
</Feature>