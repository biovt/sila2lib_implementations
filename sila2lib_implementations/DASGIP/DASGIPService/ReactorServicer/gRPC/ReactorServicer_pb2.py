# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ReactorServicer.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


import sila2lib.framework.SiLAFramework_pb2 as SiLAFramework__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='ReactorServicer.proto',
  package='sila2.org.silastandard.examples.reactorservicer.v1',
  syntax='proto3',
  serialized_options=None,
  serialized_pb=_b('\n\x15ReactorServicer.proto\x12\x32sila2.org.silastandard.examples.reactorservicer.v1\x1a\x13SiLAFramework.proto\"I\n\x16GetVInitial_Parameters\x12/\n\x06UnitID\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"N\n\x15GetVInitial_Responses\x12\x35\n\x0f\x43urrentVInitial\x18\x01 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real\"y\n\x16SetVInitial_Parameters\x12/\n\x06UnitID\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\x12.\n\x08VInitial\x18\x02 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real\"J\n\x15SetVInitial_Responses\x12\x31\n\x0bVInitialSet\x18\x01 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real\"H\n\x15GetVLiquid_Parameters\x12/\n\x06UnitID\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"L\n\x14GetVLiquid_Responses\x12\x34\n\x0e\x43urrentVLiquid\x18\x01 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real\"w\n\x15SetVLiquid_Parameters\x12/\n\x06UnitID\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\x12-\n\x07VLiquid\x18\x02 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real\"H\n\x14SetVLiquid_Responses\x12\x30\n\nVLiquidSet\x18\x01 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real\"E\n\x12GetVMax_Parameters\x12/\n\x06UnitID\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"F\n\x11GetVMax_Responses\x12\x31\n\x0b\x43urrentVMax\x18\x01 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real\"q\n\x12SetVMax_Parameters\x12/\n\x06UnitID\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\x12*\n\x04VMax\x18\x02 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real\"B\n\x11SetVMax_Responses\x12-\n\x07VMaxSet\x18\x01 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real\"E\n\x12GetVMin_Parameters\x12/\n\x06UnitID\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"F\n\x11GetVMin_Responses\x12\x31\n\x0b\x43urrentVMin\x18\x01 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real\"q\n\x12SetVMin_Parameters\x12/\n\x06UnitID\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\x12*\n\x04VMin\x18\x02 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real\"B\n\x11SetVMin_Responses\x12-\n\x07VMinSet\x18\x01 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real\"G\n\x14GetVTotal_Parameters\x12/\n\x06UnitID\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"J\n\x13GetVTotal_Responses\x12\x33\n\rCurrentVTotal\x18\x01 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real\"u\n\x14SetVTotal_Parameters\x12/\n\x06UnitID\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\x12,\n\x06VTotal\x18\x02 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real\"F\n\x13SetVTotal_Responses\x12/\n\tVTotalSet\x18\x01 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real2\xe9\x0c\n\x0fReactorServicer\x12\xa6\x01\n\x0bGetVInitial\x12J.sila2.org.silastandard.examples.reactorservicer.v1.GetVInitial_Parameters\x1aI.sila2.org.silastandard.examples.reactorservicer.v1.GetVInitial_Responses\"\x00\x12\xa6\x01\n\x0bSetVInitial\x12J.sila2.org.silastandard.examples.reactorservicer.v1.SetVInitial_Parameters\x1aI.sila2.org.silastandard.examples.reactorservicer.v1.SetVInitial_Responses\"\x00\x12\xa3\x01\n\nGetVLiquid\x12I.sila2.org.silastandard.examples.reactorservicer.v1.GetVLiquid_Parameters\x1aH.sila2.org.silastandard.examples.reactorservicer.v1.GetVLiquid_Responses\"\x00\x12\xa3\x01\n\nSetVLiquid\x12I.sila2.org.silastandard.examples.reactorservicer.v1.SetVLiquid_Parameters\x1aH.sila2.org.silastandard.examples.reactorservicer.v1.SetVLiquid_Responses\"\x00\x12\x9a\x01\n\x07GetVMax\x12\x46.sila2.org.silastandard.examples.reactorservicer.v1.GetVMax_Parameters\x1a\x45.sila2.org.silastandard.examples.reactorservicer.v1.GetVMax_Responses\"\x00\x12\x9a\x01\n\x07SetVMax\x12\x46.sila2.org.silastandard.examples.reactorservicer.v1.SetVMax_Parameters\x1a\x45.sila2.org.silastandard.examples.reactorservicer.v1.SetVMax_Responses\"\x00\x12\x9a\x01\n\x07GetVMin\x12\x46.sila2.org.silastandard.examples.reactorservicer.v1.GetVMin_Parameters\x1a\x45.sila2.org.silastandard.examples.reactorservicer.v1.GetVMin_Responses\"\x00\x12\x9a\x01\n\x07SetVMin\x12\x46.sila2.org.silastandard.examples.reactorservicer.v1.SetVMin_Parameters\x1a\x45.sila2.org.silastandard.examples.reactorservicer.v1.SetVMin_Responses\"\x00\x12\xa0\x01\n\tGetVTotal\x12H.sila2.org.silastandard.examples.reactorservicer.v1.GetVTotal_Parameters\x1aG.sila2.org.silastandard.examples.reactorservicer.v1.GetVTotal_Responses\"\x00\x12\xa0\x01\n\tSetVTotal\x12H.sila2.org.silastandard.examples.reactorservicer.v1.SetVTotal_Parameters\x1aG.sila2.org.silastandard.examples.reactorservicer.v1.SetVTotal_Responses\"\x00\x62\x06proto3')
  ,
  dependencies=[SiLAFramework__pb2.DESCRIPTOR,])




_GETVINITIAL_PARAMETERS = _descriptor.Descriptor(
  name='GetVInitial_Parameters',
  full_name='sila2.org.silastandard.examples.reactorservicer.v1.GetVInitial_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='UnitID', full_name='sila2.org.silastandard.examples.reactorservicer.v1.GetVInitial_Parameters.UnitID', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=98,
  serialized_end=171,
)


_GETVINITIAL_RESPONSES = _descriptor.Descriptor(
  name='GetVInitial_Responses',
  full_name='sila2.org.silastandard.examples.reactorservicer.v1.GetVInitial_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='CurrentVInitial', full_name='sila2.org.silastandard.examples.reactorservicer.v1.GetVInitial_Responses.CurrentVInitial', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=173,
  serialized_end=251,
)


_SETVINITIAL_PARAMETERS = _descriptor.Descriptor(
  name='SetVInitial_Parameters',
  full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVInitial_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='UnitID', full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVInitial_Parameters.UnitID', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='VInitial', full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVInitial_Parameters.VInitial', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=253,
  serialized_end=374,
)


_SETVINITIAL_RESPONSES = _descriptor.Descriptor(
  name='SetVInitial_Responses',
  full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVInitial_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='VInitialSet', full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVInitial_Responses.VInitialSet', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=376,
  serialized_end=450,
)


_GETVLIQUID_PARAMETERS = _descriptor.Descriptor(
  name='GetVLiquid_Parameters',
  full_name='sila2.org.silastandard.examples.reactorservicer.v1.GetVLiquid_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='UnitID', full_name='sila2.org.silastandard.examples.reactorservicer.v1.GetVLiquid_Parameters.UnitID', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=452,
  serialized_end=524,
)


_GETVLIQUID_RESPONSES = _descriptor.Descriptor(
  name='GetVLiquid_Responses',
  full_name='sila2.org.silastandard.examples.reactorservicer.v1.GetVLiquid_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='CurrentVLiquid', full_name='sila2.org.silastandard.examples.reactorservicer.v1.GetVLiquid_Responses.CurrentVLiquid', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=526,
  serialized_end=602,
)


_SETVLIQUID_PARAMETERS = _descriptor.Descriptor(
  name='SetVLiquid_Parameters',
  full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVLiquid_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='UnitID', full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVLiquid_Parameters.UnitID', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='VLiquid', full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVLiquid_Parameters.VLiquid', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=604,
  serialized_end=723,
)


_SETVLIQUID_RESPONSES = _descriptor.Descriptor(
  name='SetVLiquid_Responses',
  full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVLiquid_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='VLiquidSet', full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVLiquid_Responses.VLiquidSet', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=725,
  serialized_end=797,
)


_GETVMAX_PARAMETERS = _descriptor.Descriptor(
  name='GetVMax_Parameters',
  full_name='sila2.org.silastandard.examples.reactorservicer.v1.GetVMax_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='UnitID', full_name='sila2.org.silastandard.examples.reactorservicer.v1.GetVMax_Parameters.UnitID', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=799,
  serialized_end=868,
)


_GETVMAX_RESPONSES = _descriptor.Descriptor(
  name='GetVMax_Responses',
  full_name='sila2.org.silastandard.examples.reactorservicer.v1.GetVMax_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='CurrentVMax', full_name='sila2.org.silastandard.examples.reactorservicer.v1.GetVMax_Responses.CurrentVMax', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=870,
  serialized_end=940,
)


_SETVMAX_PARAMETERS = _descriptor.Descriptor(
  name='SetVMax_Parameters',
  full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVMax_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='UnitID', full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVMax_Parameters.UnitID', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='VMax', full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVMax_Parameters.VMax', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=942,
  serialized_end=1055,
)


_SETVMAX_RESPONSES = _descriptor.Descriptor(
  name='SetVMax_Responses',
  full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVMax_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='VMaxSet', full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVMax_Responses.VMaxSet', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1057,
  serialized_end=1123,
)


_GETVMIN_PARAMETERS = _descriptor.Descriptor(
  name='GetVMin_Parameters',
  full_name='sila2.org.silastandard.examples.reactorservicer.v1.GetVMin_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='UnitID', full_name='sila2.org.silastandard.examples.reactorservicer.v1.GetVMin_Parameters.UnitID', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1125,
  serialized_end=1194,
)


_GETVMIN_RESPONSES = _descriptor.Descriptor(
  name='GetVMin_Responses',
  full_name='sila2.org.silastandard.examples.reactorservicer.v1.GetVMin_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='CurrentVMin', full_name='sila2.org.silastandard.examples.reactorservicer.v1.GetVMin_Responses.CurrentVMin', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1196,
  serialized_end=1266,
)


_SETVMIN_PARAMETERS = _descriptor.Descriptor(
  name='SetVMin_Parameters',
  full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVMin_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='UnitID', full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVMin_Parameters.UnitID', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='VMin', full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVMin_Parameters.VMin', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1268,
  serialized_end=1381,
)


_SETVMIN_RESPONSES = _descriptor.Descriptor(
  name='SetVMin_Responses',
  full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVMin_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='VMinSet', full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVMin_Responses.VMinSet', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1383,
  serialized_end=1449,
)


_GETVTOTAL_PARAMETERS = _descriptor.Descriptor(
  name='GetVTotal_Parameters',
  full_name='sila2.org.silastandard.examples.reactorservicer.v1.GetVTotal_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='UnitID', full_name='sila2.org.silastandard.examples.reactorservicer.v1.GetVTotal_Parameters.UnitID', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1451,
  serialized_end=1522,
)


_GETVTOTAL_RESPONSES = _descriptor.Descriptor(
  name='GetVTotal_Responses',
  full_name='sila2.org.silastandard.examples.reactorservicer.v1.GetVTotal_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='CurrentVTotal', full_name='sila2.org.silastandard.examples.reactorservicer.v1.GetVTotal_Responses.CurrentVTotal', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1524,
  serialized_end=1598,
)


_SETVTOTAL_PARAMETERS = _descriptor.Descriptor(
  name='SetVTotal_Parameters',
  full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVTotal_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='UnitID', full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVTotal_Parameters.UnitID', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='VTotal', full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVTotal_Parameters.VTotal', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1600,
  serialized_end=1717,
)


_SETVTOTAL_RESPONSES = _descriptor.Descriptor(
  name='SetVTotal_Responses',
  full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVTotal_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='VTotalSet', full_name='sila2.org.silastandard.examples.reactorservicer.v1.SetVTotal_Responses.VTotalSet', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1719,
  serialized_end=1789,
)

_GETVINITIAL_PARAMETERS.fields_by_name['UnitID'].message_type = SiLAFramework__pb2._INTEGER
_GETVINITIAL_RESPONSES.fields_by_name['CurrentVInitial'].message_type = SiLAFramework__pb2._REAL
_SETVINITIAL_PARAMETERS.fields_by_name['UnitID'].message_type = SiLAFramework__pb2._INTEGER
_SETVINITIAL_PARAMETERS.fields_by_name['VInitial'].message_type = SiLAFramework__pb2._REAL
_SETVINITIAL_RESPONSES.fields_by_name['VInitialSet'].message_type = SiLAFramework__pb2._REAL
_GETVLIQUID_PARAMETERS.fields_by_name['UnitID'].message_type = SiLAFramework__pb2._INTEGER
_GETVLIQUID_RESPONSES.fields_by_name['CurrentVLiquid'].message_type = SiLAFramework__pb2._REAL
_SETVLIQUID_PARAMETERS.fields_by_name['UnitID'].message_type = SiLAFramework__pb2._INTEGER
_SETVLIQUID_PARAMETERS.fields_by_name['VLiquid'].message_type = SiLAFramework__pb2._REAL
_SETVLIQUID_RESPONSES.fields_by_name['VLiquidSet'].message_type = SiLAFramework__pb2._REAL
_GETVMAX_PARAMETERS.fields_by_name['UnitID'].message_type = SiLAFramework__pb2._INTEGER
_GETVMAX_RESPONSES.fields_by_name['CurrentVMax'].message_type = SiLAFramework__pb2._REAL
_SETVMAX_PARAMETERS.fields_by_name['UnitID'].message_type = SiLAFramework__pb2._INTEGER
_SETVMAX_PARAMETERS.fields_by_name['VMax'].message_type = SiLAFramework__pb2._REAL
_SETVMAX_RESPONSES.fields_by_name['VMaxSet'].message_type = SiLAFramework__pb2._REAL
_GETVMIN_PARAMETERS.fields_by_name['UnitID'].message_type = SiLAFramework__pb2._INTEGER
_GETVMIN_RESPONSES.fields_by_name['CurrentVMin'].message_type = SiLAFramework__pb2._REAL
_SETVMIN_PARAMETERS.fields_by_name['UnitID'].message_type = SiLAFramework__pb2._INTEGER
_SETVMIN_PARAMETERS.fields_by_name['VMin'].message_type = SiLAFramework__pb2._REAL
_SETVMIN_RESPONSES.fields_by_name['VMinSet'].message_type = SiLAFramework__pb2._REAL
_GETVTOTAL_PARAMETERS.fields_by_name['UnitID'].message_type = SiLAFramework__pb2._INTEGER
_GETVTOTAL_RESPONSES.fields_by_name['CurrentVTotal'].message_type = SiLAFramework__pb2._REAL
_SETVTOTAL_PARAMETERS.fields_by_name['UnitID'].message_type = SiLAFramework__pb2._INTEGER
_SETVTOTAL_PARAMETERS.fields_by_name['VTotal'].message_type = SiLAFramework__pb2._REAL
_SETVTOTAL_RESPONSES.fields_by_name['VTotalSet'].message_type = SiLAFramework__pb2._REAL
DESCRIPTOR.message_types_by_name['GetVInitial_Parameters'] = _GETVINITIAL_PARAMETERS
DESCRIPTOR.message_types_by_name['GetVInitial_Responses'] = _GETVINITIAL_RESPONSES
DESCRIPTOR.message_types_by_name['SetVInitial_Parameters'] = _SETVINITIAL_PARAMETERS
DESCRIPTOR.message_types_by_name['SetVInitial_Responses'] = _SETVINITIAL_RESPONSES
DESCRIPTOR.message_types_by_name['GetVLiquid_Parameters'] = _GETVLIQUID_PARAMETERS
DESCRIPTOR.message_types_by_name['GetVLiquid_Responses'] = _GETVLIQUID_RESPONSES
DESCRIPTOR.message_types_by_name['SetVLiquid_Parameters'] = _SETVLIQUID_PARAMETERS
DESCRIPTOR.message_types_by_name['SetVLiquid_Responses'] = _SETVLIQUID_RESPONSES
DESCRIPTOR.message_types_by_name['GetVMax_Parameters'] = _GETVMAX_PARAMETERS
DESCRIPTOR.message_types_by_name['GetVMax_Responses'] = _GETVMAX_RESPONSES
DESCRIPTOR.message_types_by_name['SetVMax_Parameters'] = _SETVMAX_PARAMETERS
DESCRIPTOR.message_types_by_name['SetVMax_Responses'] = _SETVMAX_RESPONSES
DESCRIPTOR.message_types_by_name['GetVMin_Parameters'] = _GETVMIN_PARAMETERS
DESCRIPTOR.message_types_by_name['GetVMin_Responses'] = _GETVMIN_RESPONSES
DESCRIPTOR.message_types_by_name['SetVMin_Parameters'] = _SETVMIN_PARAMETERS
DESCRIPTOR.message_types_by_name['SetVMin_Responses'] = _SETVMIN_RESPONSES
DESCRIPTOR.message_types_by_name['GetVTotal_Parameters'] = _GETVTOTAL_PARAMETERS
DESCRIPTOR.message_types_by_name['GetVTotal_Responses'] = _GETVTOTAL_RESPONSES
DESCRIPTOR.message_types_by_name['SetVTotal_Parameters'] = _SETVTOTAL_PARAMETERS
DESCRIPTOR.message_types_by_name['SetVTotal_Responses'] = _SETVTOTAL_RESPONSES
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

GetVInitial_Parameters = _reflection.GeneratedProtocolMessageType('GetVInitial_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GETVINITIAL_PARAMETERS,
  '__module__' : 'ReactorServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.reactorservicer.v1.GetVInitial_Parameters)
  })
_sym_db.RegisterMessage(GetVInitial_Parameters)

GetVInitial_Responses = _reflection.GeneratedProtocolMessageType('GetVInitial_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GETVINITIAL_RESPONSES,
  '__module__' : 'ReactorServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.reactorservicer.v1.GetVInitial_Responses)
  })
_sym_db.RegisterMessage(GetVInitial_Responses)

SetVInitial_Parameters = _reflection.GeneratedProtocolMessageType('SetVInitial_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _SETVINITIAL_PARAMETERS,
  '__module__' : 'ReactorServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.reactorservicer.v1.SetVInitial_Parameters)
  })
_sym_db.RegisterMessage(SetVInitial_Parameters)

SetVInitial_Responses = _reflection.GeneratedProtocolMessageType('SetVInitial_Responses', (_message.Message,), {
  'DESCRIPTOR' : _SETVINITIAL_RESPONSES,
  '__module__' : 'ReactorServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.reactorservicer.v1.SetVInitial_Responses)
  })
_sym_db.RegisterMessage(SetVInitial_Responses)

GetVLiquid_Parameters = _reflection.GeneratedProtocolMessageType('GetVLiquid_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GETVLIQUID_PARAMETERS,
  '__module__' : 'ReactorServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.reactorservicer.v1.GetVLiquid_Parameters)
  })
_sym_db.RegisterMessage(GetVLiquid_Parameters)

GetVLiquid_Responses = _reflection.GeneratedProtocolMessageType('GetVLiquid_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GETVLIQUID_RESPONSES,
  '__module__' : 'ReactorServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.reactorservicer.v1.GetVLiquid_Responses)
  })
_sym_db.RegisterMessage(GetVLiquid_Responses)

SetVLiquid_Parameters = _reflection.GeneratedProtocolMessageType('SetVLiquid_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _SETVLIQUID_PARAMETERS,
  '__module__' : 'ReactorServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.reactorservicer.v1.SetVLiquid_Parameters)
  })
_sym_db.RegisterMessage(SetVLiquid_Parameters)

SetVLiquid_Responses = _reflection.GeneratedProtocolMessageType('SetVLiquid_Responses', (_message.Message,), {
  'DESCRIPTOR' : _SETVLIQUID_RESPONSES,
  '__module__' : 'ReactorServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.reactorservicer.v1.SetVLiquid_Responses)
  })
_sym_db.RegisterMessage(SetVLiquid_Responses)

GetVMax_Parameters = _reflection.GeneratedProtocolMessageType('GetVMax_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GETVMAX_PARAMETERS,
  '__module__' : 'ReactorServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.reactorservicer.v1.GetVMax_Parameters)
  })
_sym_db.RegisterMessage(GetVMax_Parameters)

GetVMax_Responses = _reflection.GeneratedProtocolMessageType('GetVMax_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GETVMAX_RESPONSES,
  '__module__' : 'ReactorServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.reactorservicer.v1.GetVMax_Responses)
  })
_sym_db.RegisterMessage(GetVMax_Responses)

SetVMax_Parameters = _reflection.GeneratedProtocolMessageType('SetVMax_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _SETVMAX_PARAMETERS,
  '__module__' : 'ReactorServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.reactorservicer.v1.SetVMax_Parameters)
  })
_sym_db.RegisterMessage(SetVMax_Parameters)

SetVMax_Responses = _reflection.GeneratedProtocolMessageType('SetVMax_Responses', (_message.Message,), {
  'DESCRIPTOR' : _SETVMAX_RESPONSES,
  '__module__' : 'ReactorServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.reactorservicer.v1.SetVMax_Responses)
  })
_sym_db.RegisterMessage(SetVMax_Responses)

GetVMin_Parameters = _reflection.GeneratedProtocolMessageType('GetVMin_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GETVMIN_PARAMETERS,
  '__module__' : 'ReactorServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.reactorservicer.v1.GetVMin_Parameters)
  })
_sym_db.RegisterMessage(GetVMin_Parameters)

GetVMin_Responses = _reflection.GeneratedProtocolMessageType('GetVMin_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GETVMIN_RESPONSES,
  '__module__' : 'ReactorServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.reactorservicer.v1.GetVMin_Responses)
  })
_sym_db.RegisterMessage(GetVMin_Responses)

SetVMin_Parameters = _reflection.GeneratedProtocolMessageType('SetVMin_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _SETVMIN_PARAMETERS,
  '__module__' : 'ReactorServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.reactorservicer.v1.SetVMin_Parameters)
  })
_sym_db.RegisterMessage(SetVMin_Parameters)

SetVMin_Responses = _reflection.GeneratedProtocolMessageType('SetVMin_Responses', (_message.Message,), {
  'DESCRIPTOR' : _SETVMIN_RESPONSES,
  '__module__' : 'ReactorServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.reactorservicer.v1.SetVMin_Responses)
  })
_sym_db.RegisterMessage(SetVMin_Responses)

GetVTotal_Parameters = _reflection.GeneratedProtocolMessageType('GetVTotal_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GETVTOTAL_PARAMETERS,
  '__module__' : 'ReactorServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.reactorservicer.v1.GetVTotal_Parameters)
  })
_sym_db.RegisterMessage(GetVTotal_Parameters)

GetVTotal_Responses = _reflection.GeneratedProtocolMessageType('GetVTotal_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GETVTOTAL_RESPONSES,
  '__module__' : 'ReactorServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.reactorservicer.v1.GetVTotal_Responses)
  })
_sym_db.RegisterMessage(GetVTotal_Responses)

SetVTotal_Parameters = _reflection.GeneratedProtocolMessageType('SetVTotal_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _SETVTOTAL_PARAMETERS,
  '__module__' : 'ReactorServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.reactorservicer.v1.SetVTotal_Parameters)
  })
_sym_db.RegisterMessage(SetVTotal_Parameters)

SetVTotal_Responses = _reflection.GeneratedProtocolMessageType('SetVTotal_Responses', (_message.Message,), {
  'DESCRIPTOR' : _SETVTOTAL_RESPONSES,
  '__module__' : 'ReactorServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.examples.reactorservicer.v1.SetVTotal_Responses)
  })
_sym_db.RegisterMessage(SetVTotal_Responses)



_REACTORSERVICER = _descriptor.ServiceDescriptor(
  name='ReactorServicer',
  full_name='sila2.org.silastandard.examples.reactorservicer.v1.ReactorServicer',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  serialized_start=1792,
  serialized_end=3433,
  methods=[
  _descriptor.MethodDescriptor(
    name='GetVInitial',
    full_name='sila2.org.silastandard.examples.reactorservicer.v1.ReactorServicer.GetVInitial',
    index=0,
    containing_service=None,
    input_type=_GETVINITIAL_PARAMETERS,
    output_type=_GETVINITIAL_RESPONSES,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='SetVInitial',
    full_name='sila2.org.silastandard.examples.reactorservicer.v1.ReactorServicer.SetVInitial',
    index=1,
    containing_service=None,
    input_type=_SETVINITIAL_PARAMETERS,
    output_type=_SETVINITIAL_RESPONSES,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='GetVLiquid',
    full_name='sila2.org.silastandard.examples.reactorservicer.v1.ReactorServicer.GetVLiquid',
    index=2,
    containing_service=None,
    input_type=_GETVLIQUID_PARAMETERS,
    output_type=_GETVLIQUID_RESPONSES,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='SetVLiquid',
    full_name='sila2.org.silastandard.examples.reactorservicer.v1.ReactorServicer.SetVLiquid',
    index=3,
    containing_service=None,
    input_type=_SETVLIQUID_PARAMETERS,
    output_type=_SETVLIQUID_RESPONSES,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='GetVMax',
    full_name='sila2.org.silastandard.examples.reactorservicer.v1.ReactorServicer.GetVMax',
    index=4,
    containing_service=None,
    input_type=_GETVMAX_PARAMETERS,
    output_type=_GETVMAX_RESPONSES,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='SetVMax',
    full_name='sila2.org.silastandard.examples.reactorservicer.v1.ReactorServicer.SetVMax',
    index=5,
    containing_service=None,
    input_type=_SETVMAX_PARAMETERS,
    output_type=_SETVMAX_RESPONSES,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='GetVMin',
    full_name='sila2.org.silastandard.examples.reactorservicer.v1.ReactorServicer.GetVMin',
    index=6,
    containing_service=None,
    input_type=_GETVMIN_PARAMETERS,
    output_type=_GETVMIN_RESPONSES,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='SetVMin',
    full_name='sila2.org.silastandard.examples.reactorservicer.v1.ReactorServicer.SetVMin',
    index=7,
    containing_service=None,
    input_type=_SETVMIN_PARAMETERS,
    output_type=_SETVMIN_RESPONSES,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='GetVTotal',
    full_name='sila2.org.silastandard.examples.reactorservicer.v1.ReactorServicer.GetVTotal',
    index=8,
    containing_service=None,
    input_type=_GETVTOTAL_PARAMETERS,
    output_type=_GETVTOTAL_RESPONSES,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='SetVTotal',
    full_name='sila2.org.silastandard.examples.reactorservicer.v1.ReactorServicer.SetVTotal',
    index=9,
    containing_service=None,
    input_type=_SETVTOTAL_PARAMETERS,
    output_type=_SETVTOTAL_RESPONSES,
    serialized_options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_REACTORSERVICER)

DESCRIPTOR.services_by_name['ReactorServicer'] = _REACTORSERVICER

# @@protoc_insertion_point(module_scope)
