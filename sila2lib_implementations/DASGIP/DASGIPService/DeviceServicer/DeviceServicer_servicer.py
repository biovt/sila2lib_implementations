"""
________________________________________________________________________

:PROJECT: SiLA2_python

*Device Servicer*

:details: DeviceServicer:
    General device settings of the DASGIP reactor system can be retrieved and changed within this function.
    By Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 20.05.2019
           
:file:    DeviceServicer_servicer.py
:authors: Lukas Bromig

:date: (creation)          2020-04-16T10:19:13.430816
:date: (last modification) 2020-04-16T10:19:13.430816

.. note:: Code generated by sila2codegenerator 0.2.0

________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "0.0.1"

# import general packages
import logging
import grpc

# meta packages
from typing import Union

# import SiLA2 library
import sila2lib.framework.SiLAFramework_pb2 as silaFW_pb2
from sila2lib.error_handling.server_err import SiLAError

# import gRPC modules for this feature
from .gRPC import DeviceServicer_pb2 as DeviceServicer_pb2
from .gRPC import DeviceServicer_pb2_grpc as DeviceServicer_pb2_grpc

# import simulation and real implementation
from .DeviceServicer_simulation import DeviceServicerSimulation
from .DeviceServicer_real import DeviceServicerReal


class DeviceServicer(DeviceServicer_pb2_grpc.DeviceServicerServicer):
    """
    This is a DASGIP Service
    """
    implementation: Union[DeviceServicerSimulation, DeviceServicerReal]
    simulation_mode: bool

    def __init__(self, reactors, simulation_mode: bool = True):
        """
        Class initialiser.

        :param simulation_mode: Sets whether at initialisation the simulation mode is active or the real mode.
        """
        self.reactors = reactors
        self.simulation_mode = simulation_mode
        if simulation_mode:
            self._inject_implementation(DeviceServicerSimulation(self.reactors))
        else:
            self._inject_implementation(DeviceServicerReal(self.reactors))

    def _inject_implementation(self,
                               implementation: Union[DeviceServicerSimulation,
                                                     DeviceServicerReal]
                               ) -> bool:
        """
        Dependency injection of the implementation used.
            Allows to set the class used for simulation/real mode.

        :param implementation: A valid implementation of the DASGIP_ServiceServicer.
        """

        self.implementation = implementation
        return True

    def switch_to_simulation_mode(self):
        """Method that will automatically be called by the server when the simulation mode is requested."""
        self.simulation_mode = True
        self._inject_implementation(DeviceServicerSimulation(self.reactors))

    def switch_to_real_mode(self):
        """Method that will automatically be called by the server when the real mode is requested."""
        self.simulation_mode = False
        self._inject_implementation(DeviceServicerReal(self.reactors))

    def GetLog(self, request, context: grpc.ServicerContext) \
            -> silaFW_pb2.CommandConfirmation:
        """
        Executes the observable command "Get Log"
            Get the current status of the device from the state machine of the SiLA server.
    
        :param request: gRPC request containing the parameters passed:
            request.EmptyParameter (Empty Parameter): An empty parameter data type used if no parameter is required.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: A command confirmation object with the following information:
            commandId: A command id with which this observable command can be referenced in future calls
            lifetimeOfExecution: The (maximum) lifetime of this command call.
        """
    
        logging.debug(
            "GetLog called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
        try:
            return self.implementation.GetLog(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetLog_Info(self, request, context: grpc.ServicerContext) \
            -> silaFW_pb2.ExecutionInfo:
        """
        Returns execution information regarding the command call :meth:`~.GetLog`.
    
        :param request: A request object with the following properties
            CommandExecutionUUID: The UUID of the command executed.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: An ExecutionInfo response stream for the command with the following fields:
            commandStatus: Status of the command (enumeration)
            progressInfo: Information on the progress of the command (0 to 1)
            estimatedRemainingTime: Estimate of the remaining time required to run the command
            updatedLifetimeOfExecution: An update on the execution lifetime
        """
    
        logging.debug(
            "GetLog_Info called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
        try:
            return self.implementation.GetLog_Info(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetLog_Result(self, request, context: grpc.ServicerContext) \
            -> DeviceServicer_pb2.GetLog_Responses:
        """
        Returns the final result of the command call :meth:`~.GetLog`.
    
        :param request: A request object with the following properties
            CommandExecutionUUID: The UUID of the command executed.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentLogLevel (Current Log Level): The current log level of the latest logs , retrieved from the SiLA server log file.
            request.CurrentLogTimestamp (Current Log Timestamp): The current log timestamp of the latest logs , retrieved from the SiLA server log file.
            request.CurrentLogMessage (Current Log Level): The current log level of the latest logs , retrieved from the SiLA server log file.
        """
    
        logging.debug(
            "GetLog_Result called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
        try:
            return self.implementation.GetLog_Result(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    
    def GetRuntimeClock(self, request, context: grpc.ServicerContext) \
            -> DeviceServicer_pb2.GetRuntimeClock_Responses:
        """
        Executes the unobservable command "Get Runtime Clock"
            Get the current local time of the system.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentRuntime (Current Runtime): The current runtime of the DASGIP reactor system.
        """
    
        logging.debug(
            "GetRuntimeClock called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetRuntimeClock(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetStartedUTC(self, request, context: grpc.ServicerContext) \
            -> DeviceServicer_pb2.GetStartedUTC_Responses:
        """
        Executes the unobservable command "Get Started UTC"
            Get time stamp of system start in UTC
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentStartedUTC (Current Started UTC): The current timestamnp of the system start in UTC
        """
    
        logging.debug(
            "GetStartedUTC called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetStartedUTC(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetStarted(self, request, context: grpc.ServicerContext) \
            -> DeviceServicer_pb2.GetStarted_Responses:
        """
        Executes the unobservable command "Get Started"
            Get time stamp of system start
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentStarted (Current Started): The current timestamnp of the system start
        """
    
        logging.debug(
            "GetStarted called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetStarted(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetStoppedUTC(self, request, context: grpc.ServicerContext) \
            -> DeviceServicer_pb2.GetStoppedUTC_Responses:
        """
        Executes the unobservable command "Get Stopped UTC"
            Get time stamp of system stop in UTC
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentStoppedUTC (Current Stopped UTC): The current timestamnp of the system stop in UTC
        """
    
        logging.debug(
            "GetStoppedUTC called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetStoppedUTC(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetStopped(self, request, context: grpc.ServicerContext) \
            -> DeviceServicer_pb2.GetStopped_Responses:
        """
        Executes the unobservable command "Get Stopped"
            Get time stamp of system stop
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentStopped (Current Stopped): The current timestamnp of the system stop
        """
    
        logging.debug(
            "GetStopped called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetStopped(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetUserId(self, request, context: grpc.ServicerContext) \
            -> DeviceServicer_pb2.GetUserId_Responses:
        """
        Executes the unobservable command "Get UserId"
            Get User Id of the System
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentUserId (Current User ID): The current user ID of the system
        """
    
        logging.debug(
            "GetUserId called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetUserId(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetBatchId(self, request, context: grpc.ServicerContext) \
            -> DeviceServicer_pb2.GetBatchId_Responses:
        """
        Executes the unobservable command "Get BatchId"
            Get Batch Id of the System
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentBatchId (Current Batch ID): The current batch ID of the system
        """
    
        logging.debug(
            "GetBatchId called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetBatchId(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetInoculatedUTC(self, request, context: grpc.ServicerContext) \
            -> DeviceServicer_pb2.GetInoculatedUTC_Responses:
        """
        Executes the unobservable command "Get InoculatedUTC"
            Get Inoculation time in UTC
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentInoculationUTC (Current InoculationUTC): The current inoculation time in UTC
        """
    
        logging.debug(
            "GetInoculatedUTC called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetInoculatedUTC(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetInoculated(self, request, context: grpc.ServicerContext) \
            -> DeviceServicer_pb2.GetInoculated_Responses:
        """
        Executes the unobservable command "Get Inoculated"
            Get Inoculation time
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentInoculation (Current Inoculation): The current inoculation time
        """
    
        logging.debug(
            "GetInoculated called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetInoculated(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetAvailable(self, request, context: grpc.ServicerContext) \
            -> DeviceServicer_pb2.GetAvailable_Responses:
        """
        Executes the unobservable command "Get Available"
            Get System Availability
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentAvailable (Current Available): The current system availability
        """
    
        logging.debug(
            "GetAvailable called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetAvailable(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetName(self, request, context: grpc.ServicerContext) \
            -> DeviceServicer_pb2.GetName_Responses:
        """
        Executes the unobservable command "Get Name"
            Get System Name
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentName (Current Name): The current system name
        """
    
        logging.debug(
            "GetName called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetName(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
    def GetVersion(self, request, context: grpc.ServicerContext) \
            -> DeviceServicer_pb2.GetVersion_Responses:
        """
        Executes the unobservable command "Get Version"
            Get System Version
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentVersion (Current Version): The current system version
        """
    
        logging.debug(
            "GetVersion called in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
    
        try:
            return self.implementation.GetVersion(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)

    def Subscribe_CurrentStatus(self, request, context: grpc.ServicerContext) \
            -> DeviceServicer_pb2.Subscribe_CurrentStatus_Responses:
        """
        Requests the observable property Current Status
            Get the current status of the device from the internal state machine of the SiLA server.
    
        :param request: An empty gRPC request object (properties have no parameters)
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: A response stream with the following fields:
            request.CurrentStatus (Current Status): Get the current status of the device from the internal state machine of the SiLA server.
        """
    
        logging.debug(
            "Property CurrentStatus requested in {current_mode} mode".format(
                current_mode=('simulation' if self.simulation_mode else 'real')
            )
        )
        try:
            return self.implementation.Subscribe_CurrentStatus(request, context)
        except SiLAError as err:
            err.raise_rpc_error(context=context)
    
