"""
________________________________________________________________________

:PROJECT: SiLA2_python

*PumpB Servicer*

:details: PumpBServicer:
    Control a DASGIP PumpB module. Enables read and write operations for various parameters, including PumpB sensor,
    controller, and alarm.
    By Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 20.05.2019

:file:    PumpBServicer_simulation.py
:authors: Lukas Bromig

:date: (creation)          2020-04-16T10:19:14.817050
:date: (last modification) 2020-04-16T10:19:14.817050

.. note:: Code generated by sila2codegenerator 0.2.0

________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "0.0.1"

# import general packages
import logging
import time         # used for observables
import uuid         # used for observables
import grpc         # used for type hinting only

# import SiLA2 library
import sila2lib.framework.SiLAFramework_pb2 as silaFW_pb2

# import gRPC modules for this feature
from .gRPC import PumpBServicer_pb2 as PumpBServicer_pb2
# from .gRPC import PumpBServicer_pb2_grpc as PumpBServicer_pb2_grpc

# import default arguments
from .PumpBServicer_default_arguments import default_dict


# noinspection PyPep8Naming,PyUnusedLocal
class PumpBServicerSimulation:
    """
    Implementation of the *PumpB Servicer* in *Simulation* mode
        This is a DASGIP Service
    """

    def __init__(self, reactors):
        """Class initialiser"""
        self.reactors = reactors
        logging.debug('Started server in mode: {mode}'.format(mode='Simulation'))

    def _get_command_state(self, command_uuid: str) -> silaFW_pb2.ExecutionInfo:
        """
        Method to fill an ExecutionInfo message from the SiLA server for observable commands

        :param command_uuid: The uuid of the command for which to return the current state

        :return: An execution info object with the current command state
        """

        #: Enumeration of silaFW_pb2.ExecutionInfo.CommandStatus
        command_status = silaFW_pb2.ExecutionInfo.CommandStatus.waiting
        #: Real silaFW_pb2.Real(0...1)
        command_progress = None
        #: Duration silaFW_pb2.Duration(seconds=<seconds>, nanos=<nanos>)
        command_estimated_remaining = None
        #: Duration silaFW_pb2.Duration(seconds=<seconds>, nanos=<nanos>)
        command_lifetime_of_execution = None

        # TODO: check the state of the command with the given uuid and return the correct information

        # just return a default in this example
        return silaFW_pb2.ExecutionInfo(
            commandStatus=command_status,
            progressInfo=(
                command_progress if command_progress is not None else None
            ),
            estimatedRemainingTime=(
                command_estimated_remaining if command_estimated_remaining is not None else None
            ),
            updatedLifetimeOfExecution=(
                command_lifetime_of_execution if command_lifetime_of_execution is not None else None
            )
        )

    def GetPVInt(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.GetPVInt_Responses:
        """
        Executes the unobservable command "Get PVInt "
            Get integrated present value
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentPVInt (Current PV Int): Current integrated present value. Accumulated process value.
        """
    
        # initialise the return value
        return_value = None

        CurrentPVInt = 13.5
        return_value = PumpBServicer_pb2.GetPVInt_Responses(CurrentPVInt=silaFW_pb2.Real(value=CurrentPVInt))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.GetPVInt_Responses(
                **default_dict['GetPVInt_Responses']
            )
    
        return return_value

    def GetPV(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.GetPV_Responses:
        """
        Executes the unobservable command "Get PV"
            Get present value
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentPV (Current PV): Current present value
        """
    
        # initialise the return value
        return_value = None

        CurrentPV = 11.0
        return_value = PumpBServicer_pb2.GetPV_Responses(CurrentPV=silaFW_pb2.Real(value=CurrentPV))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.GetPV_Responses(
                **default_dict['GetPV_Responses']
            )
    
        return return_value

    def SetSPM(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.SetSPM_Responses:
        """
        Executes the unobservable command "Set Manual Setpoint"
            Set the manual PumpB setpoint.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
            request.SPM (Manual Setpoint):
            The manual setpoint of the PumpB module.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.SPMSet (Manual Setpoint Set): The set manual setpoint.
        """
    
        # initialise the return value
        return_value = None

        SPMSet = 0.0
        return_value = PumpBServicer_pb2.SetSPM_Responses(SPMSet=silaFW_pb2.Real(value=SPMSet))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.SetSPM_Responses(
                **default_dict['SetSPM_Responses']
            )
    
        return return_value

    def SetSPE(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.SetSPE_Responses:
        """
        Executes the unobservable command "Set External Setpoint"
            Set the external PumpB setpoint.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
            request.SPE (External Setpoint):
            The external setpoint of the PumpB module.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.SPESet (External Setpoint Set): The set external setpoint.
        """
    
        # initialise the return value
        return_value = None

        SPESet = 0.0
        return_value = PumpBServicer_pb2.SetSPE_Responses(SPESet=silaFW_pb2.Real(value=SPESet))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.SetSPE_Responses(
                **default_dict['SetSPE_Responses']
            )
    
        return return_value

    def GetSP(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.GetSP_Responses:
        """
        Executes the unobservable command "Get SP"
            Get setpoint value
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentSP (Current SP): Current setpoint value
        """
    
        # initialise the return value
        return_value = None

        CurrentSP = 15.0
        return_value = PumpBServicer_pb2.GetSP_Responses(CurrentSP=silaFW_pb2.Real(value=CurrentSP))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.GetSP_Responses(
                **default_dict['GetSP_Responses']
            )
    
        return return_value

    def GetSPA(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.GetSPA_Responses:
        """
        Executes the unobservable command "Get SPA"
            Get automatic setpoint value
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentSPA (Current SPA): Current automatic setpoint value
        """
    
        # initialise the return value
        return_value = None

        CurrentSPA = 1.9
        return_value = PumpBServicer_pb2.GetSPA_Responses(CurrentSPA=silaFW_pb2.Real(value=CurrentSPA))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.GetSPA_Responses(
                **default_dict['GetSPA_Responses']
            )
    
        return return_value

    def GetSPM(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.GetSPM_Responses:
        """
        Executes the unobservable command "Get SPM"
            Get manual setpoint value
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentSPM (Current SPM): Current manual setpoint value
        """
    
        # initialise the return value
        return_value = None

        CurrentSPM = 40.0
        return_value = PumpBServicer_pb2.GetSPM_Responses(CurrentSPM=silaFW_pb2.Real(value=CurrentSPM))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.GetSPM_Responses(
                **default_dict['GetSPM_Responses']
            )
    
        return return_value

    def GetSPE(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.GetSPE_Responses:
        """
        Executes the unobservable command "Get SPE"
            Get external setpoint value
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentSPE (Current SPE): Current external setpoint value
        """
    
        # initialise the return value
        return_value = None

        CurrentSPE = 26.0
        return_value = PumpBServicer_pb2.GetSPE_Responses(CurrentSPE=silaFW_pb2.Real(value=CurrentSPE))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.GetSPE_Responses(
                **default_dict['GetSPE_Responses']
            )
    
        return return_value

    def GetSPR(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.GetSPR_Responses:
        """
        Executes the unobservable command "Get SPR"
            Get remote setpoint value
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentSPR (Current SPR): Current remote setpoint value
        """
    
        # initialise the return value
        return_value = None

        CurrentSPR = 13.37
        return_value = PumpBServicer_pb2.GetSPR_Responses(CurrentSPR=silaFW_pb2.Real(value=CurrentSPR))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.GetSPR_Responses(
                **default_dict['GetSPR_Responses']
            )
    
        return return_value

    def GetAccess(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.GetAccess_Responses:
        """
        Executes the unobservable command "Get Access Mode"
            Get access mode value. Controller access (Local, Remote).
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentAccess (Current Access Mode): Current access mode value. Controller access (Local, Remote)
        """
    
        # initialise the return value
        return_value = None

        CurrentAccess = 1
        return_value = PumpBServicer_pb2.GetAccess_Responses(CurrentAccess=silaFW_pb2.Integer(value=CurrentAccess))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.GetAccess_Responses(
                **default_dict['GetAccess_Responses']
            )
    
        return return_value

    def SetCmd(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.SetCmd_Responses:
        """
        Executes the unobservable command "Set Controller Command"
            Set the controller command. Controller command (Nothing, Stop, Start).
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
            request.Cmd (Controller Command):
            The controller command of the PumpB module. Controller command (Nothing, Stop, Start).
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CmdSet (Cmd Set): The set controller command.
        """
    
        # initialise the return value
        return_value = None

        CmdSet = 0
        return_value = PumpBServicer_pb2.SetCmd_Responses(CmdSet=silaFW_pb2.Integer(value=CmdSet))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.SetCmd_Responses(
                **default_dict['SetCmd_Responses']
            )
    
        return return_value

    def GetCmd(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.GetCmd_Responses:
        """
        Executes the unobservable command "Get Controller Command"
            Get the controller command. Controller command (Nothing, Stop, Start).
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentCmd (Current Controller Command): Current controller command value of the PumpB module.
            Controller command (Nothing, Stop, Start)
        """
    
        # initialise the return value
        return_value = None

        CurrentCmd = 0
        return_value = PumpBServicer_pb2.GetCmd_Responses(CurrentCmd=silaFW_pb2.Integer(value=CurrentCmd))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.GetCmd_Responses(
                **default_dict['GetCmd_Responses']
            )
    
        return return_value

    def SetMode(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.SetMode_Responses:
        """
        Executes the unobservable command "Set Controller Mode"
            Set the controller mode. Controller mode (Manual, Automatic).
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
            request.Mode (Controller Mode):
            The controller mode of the PumpB module. Controller mode (Manual, Automatic).
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.ModeSet (Controller Mode Set): The set controller mode.
        """
    
        # initialise the return value
        return_value = None

        ModeSet = 1
        return_value = PumpBServicer_pb2.SetMode_Responses(ModeSet=silaFW_pb2.Integer(value=ModeSet))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.SetMode_Responses(
                **default_dict['SetMode_Responses']
            )
    
        return return_value

    def GetMode(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.GetMode_Responses:
        """
        Executes the unobservable command "Get Controller Mode"
            Get the controller mode. Controller mode (Manual, Automatic).
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentMode (Current Controller Mode): Current controller mode value of the PumpB module. Controller
            mode (Manual, Automatic).
        """
    
        # initialise the return value
        return_value = None

        CurrentMode = 1
        return_value = PumpBServicer_pb2.GetMode_Responses(CurrentMode=silaFW_pb2.Integer(value=CurrentMode))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.GetMode_Responses(
                **default_dict['GetMode_Responses']
            )
    
        return return_value

    def SetSetpointSelect(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.SetSetpointSelect_Responses:
        """
        Executes the unobservable command "Set SetpointSelect"
            Set the selected setpoint that should be used. Setpoint selection (Local, Manual, Internal, Script,
            External).
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
            request.SetpointSelect (Set SetpointSelect):
            The selected setpoint of the PumpB module. Setpoint selection (Local, Manual, Internal, Script, External).
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.SetpointSelectSet (Setpoint selection Set): The set setpoint selection.
        """
    
        # initialise the return value
        return_value = None

        SetpointSelectSet = 2
        return_value = PumpBServicer_pb2.SetSetpointSelect_Responses(
            SetpointSelectSet=silaFW_pb2.Integer(value=SetpointSelectSet))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.SetSetpointSelect_Responses(
                **default_dict['SetSetpointSelect_Responses']
            )
    
        return return_value

    def GetSetpointSelect(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.GetSetpointSelect_Responses:
        """
        Executes the unobservable command "Get SetpointSelect"
            Get the setpoint selection. Controller state (Off, On, Error).
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentSetpointSelect (Current Setpoint Selection): Current setpoint selection value of the PumpB
            module. Controller state (Off, On, Error).
        """
    
        # initialise the return value
        return_value = None

        CurrentSetpointSelect = 2
        return_value = PumpBServicer_pb2.GetSetpointSelect_Responses(
            CurrentSetpointSelect=silaFW_pb2.Integer(value=CurrentSetpointSelect))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.GetSetpointSelect_Responses(
                **default_dict['GetSetpointSelect_Responses']
            )
    
        return return_value

    def GetState(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.GetState_Responses:
        """
        Executes the unobservable command "Get State"
            Get controller state. Controller state (Off, On, Error).
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentState (Current Controller State): Current controller state value of the PumpB module.
            Controller state (Off, On, Error).
        """
    
        # initialise the return value
        return_value = None

        CurrentState = 0
        return_value = PumpBServicer_pb2.GetState_Responses(CurrentState=silaFW_pb2.Integer(value=CurrentState))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.GetState_Responses(
                **default_dict['GetState_Responses']
            )
    
        return return_value

    def GetType(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.GetType_Responses:
        """
        Executes the unobservable command "Get Function Type"
            Get function type.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentType (Current Function Type): Current function type value of the PumpB module.
        """
    
        # initialise the return value
        return_value = None

        CurrentType = 'SimPumpBType'
        return_value = PumpBServicer_pb2.GetType_Responses(CurrentType=silaFW_pb2.String(value=CurrentType))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.GetType_Responses(
                **default_dict['GetType_Responses']
            )
    
        return return_value

    def GetAvailable(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.GetAvailable_Responses:
        """
        Executes the unobservable command "Get Function Availability"
            Get function availability.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentAvailable (Current Function Availability): Current function availability value of the PumpB
            module.
        """
    
        # initialise the return value
        return_value = None

        CurrentAvailable = 0
        return_value = PumpBServicer_pb2.GetAvailable_Responses(
            CurrentAvailable=silaFW_pb2.Integer(value=CurrentAvailable))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.GetAvailable_Responses(
                **default_dict['GetAvailable_Responses']
            )
    
        return return_value

    def GetName(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.GetName_Responses:
        """
        Executes the unobservable command "Get Function Name"
            Get function name.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentName (Current Function Name): Current function name of the PumpB module.
        """
    
        # initialise the return value
        return_value = None

        CurrentName = 'SimPumpB'
        return_value = PumpBServicer_pb2.GetName_Responses(CurrentName=silaFW_pb2.String(value=CurrentName))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.GetName_Responses(
                **default_dict['GetName_Responses']
            )
    
        return return_value

    def GetVersion(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.GetVersion_Responses:
        """
        Executes the unobservable command "Get Function Version"
            Get function model version number.
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentVersion (Current Function Version): Current function model version number of the PumpB
            module.
        """
    
        # initialise the return value
        return_value = None

        CurrentVersion = 'SimPumpBVersion_1.0'
        return_value = PumpBServicer_pb2.GetVersion_Responses(CurrentVersion=silaFW_pb2.String(value=CurrentVersion))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.GetVersion_Responses(
                **default_dict['GetVersion_Responses']
            )
    
        return return_value

    def SetActuatorCalibration(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.SetActuatorCalibration_Responses:
        """
        Executes the unobservable command "Set Actuator Calibration"
            Set the actuator calibration value that should be used. Calibration parameter (at 100% speed).
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
            request.ActuatorCalibration (Set ActuatorCalibration):
            The actuator calibration value that should be used. Calibration parameter (at 100% speed).
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.ActuatorCalibrationSet (Actuator Calibration Set): The set actuator calibration value that is used.
            Calibration parameter (at 100% speed).
        """
    
        # initialise the return value
        return_value = None

        ActuatorCalibrationSet = 105.5
        return_value = PumpBServicer_pb2.SetActuatorCalibration_Responses(
            ActuatorCalibrationSet=silaFW_pb2.Real(value=ActuatorCalibrationSet))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.SetActuatorCalibration_Responses(
                **default_dict['SetActuatorCalibration_Responses']
            )
    
        return return_value

    def GetActuatorCalibration(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.GetActuatorCalibration_Responses:
        """
        Executes the unobservable command "Get Actuator Calibration"
            Get the actuator calibration value that is used. Calibration parameter (at 100% speed).
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentActuatorCalibration (Current Actuator Calibration): Current actuator calibration value of the
            PumpB module. Calibration parameter (at 100% speed).
        """
    
        # initialise the return value
        return_value = None

        CurrentActuatorCalibration = 110.0
        return_value = PumpBServicer_pb2.GetActuatorCalibration_Responses(
            CurrentActuatorCalibration=silaFW_pb2.Real(value=CurrentActuatorCalibration))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.GetActuatorCalibration_Responses(
                **default_dict['GetActuatorCalibration_Responses']
            )
    
        return return_value

    def GetActuatorDirPV(self, request, context: grpc.ServicerContext) \
            -> PumpBServicer_pb2.GetActuatorDirPV_Responses:
        """
        Executes the unobservable command "Get Actuator Dir PV"
            Get the actuator direction value. Actual pump direction (0=Clockwise, 1=Counterclockwise).
    
        :param request: gRPC request containing the parameters passed:
            request.UnitID (UnitID):
            The UnitID of the adressed reactor
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CurrentActuatorDirPV (Current Actuator Dir PV): Current actuator direction present value of the
            PumpB module. Actual pump direction (0=Clockwise, 1=Counterclockwise).
        """
    
        # initialise the return value
        return_value = None

        CurrentActuatorDirPV = 0
        return_value = PumpBServicer_pb2.GetActuatorDirPV_Responses(
            CurrentActuatorDirPV=silaFW_pb2.Integer(value=CurrentActuatorDirPV))

        # fallback to default
        if return_value is None:
            return_value = PumpBServicer_pb2.GetActuatorDirPV_Responses(
                **default_dict['GetActuatorDirPV_Responses']
            )
    
        return return_value
