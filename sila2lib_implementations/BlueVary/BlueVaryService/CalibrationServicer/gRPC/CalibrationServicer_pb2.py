# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: CalibrationServicer.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


import sila2lib.framework.SiLAFramework_pb2 as SiLAFramework__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='CalibrationServicer.proto',
  package='sila2.biovt.mw.tum.de.examples.calibrationservicer.v1',
  syntax='proto3',
  serialized_options=None,
  serialized_pb=_b('\n\x19\x43\x61librationServicer.proto\x12\x35sila2.biovt.mw.tum.de.examples.calibrationservicer.v1\x1a\x13SiLAFramework.proto\"\x1d\n\x1bStartCalibration_Parameters\"W\n\x1aStartCalibration_Responses\x12\x39\n\x11\x43\x61librationStatus\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.String2\xd3\x01\n\x13\x43\x61librationServicer\x12\xbb\x01\n\x10StartCalibration\x12R.sila2.biovt.mw.tum.de.examples.calibrationservicer.v1.StartCalibration_Parameters\x1aQ.sila2.biovt.mw.tum.de.examples.calibrationservicer.v1.StartCalibration_Responses\"\x00\x62\x06proto3')
  ,
  dependencies=[SiLAFramework__pb2.DESCRIPTOR,])




_STARTCALIBRATION_PARAMETERS = _descriptor.Descriptor(
  name='StartCalibration_Parameters',
  full_name='sila2.biovt.mw.tum.de.examples.calibrationservicer.v1.StartCalibration_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=105,
  serialized_end=134,
)


_STARTCALIBRATION_RESPONSES = _descriptor.Descriptor(
  name='StartCalibration_Responses',
  full_name='sila2.biovt.mw.tum.de.examples.calibrationservicer.v1.StartCalibration_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='CalibrationStatus', full_name='sila2.biovt.mw.tum.de.examples.calibrationservicer.v1.StartCalibration_Responses.CalibrationStatus', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=136,
  serialized_end=223,
)

_STARTCALIBRATION_RESPONSES.fields_by_name['CalibrationStatus'].message_type = SiLAFramework__pb2._STRING
DESCRIPTOR.message_types_by_name['StartCalibration_Parameters'] = _STARTCALIBRATION_PARAMETERS
DESCRIPTOR.message_types_by_name['StartCalibration_Responses'] = _STARTCALIBRATION_RESPONSES
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

StartCalibration_Parameters = _reflection.GeneratedProtocolMessageType('StartCalibration_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _STARTCALIBRATION_PARAMETERS,
  '__module__' : 'CalibrationServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.calibrationservicer.v1.StartCalibration_Parameters)
  })
_sym_db.RegisterMessage(StartCalibration_Parameters)

StartCalibration_Responses = _reflection.GeneratedProtocolMessageType('StartCalibration_Responses', (_message.Message,), {
  'DESCRIPTOR' : _STARTCALIBRATION_RESPONSES,
  '__module__' : 'CalibrationServicer_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.calibrationservicer.v1.StartCalibration_Responses)
  })
_sym_db.RegisterMessage(StartCalibration_Responses)



_CALIBRATIONSERVICER = _descriptor.ServiceDescriptor(
  name='CalibrationServicer',
  full_name='sila2.biovt.mw.tum.de.examples.calibrationservicer.v1.CalibrationServicer',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  serialized_start=226,
  serialized_end=437,
  methods=[
  _descriptor.MethodDescriptor(
    name='StartCalibration',
    full_name='sila2.biovt.mw.tum.de.examples.calibrationservicer.v1.CalibrationServicer.StartCalibration',
    index=0,
    containing_service=None,
    input_type=_STARTCALIBRATION_PARAMETERS,
    output_type=_STARTCALIBRATION_RESPONSES,
    serialized_options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_CALIBRATIONSERVICER)

DESCRIPTOR.services_by_name['CalibrationServicer'] = _CALIBRATIONSERVICER

# @@protoc_insertion_point(module_scope)
