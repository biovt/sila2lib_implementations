"""
________________________________________________________________________

:PROJECT: SiLA2_python

*Calibration Servicer*

:details: CalibrationServicer:
    Used to initiate device 1-pt calibration of the off-gas sensors.
    By Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 20.05.2019

:file:    CalibrationServicer_simulation.py
:authors: Lukas Bromig

:date: (creation)          2020-04-16T10:18:59.325918
:date: (last modification) 2020-04-16T10:18:59.325918

.. note:: Code generated by sila2codegenerator 0.2.0

________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "1.0"

# import general packages
import logging
import time         # used for observables
import uuid         # used for observables
import grpc         # used for type hinting only

# import SiLA2 library
import sila2lib.framework.SiLAFramework_pb2 as silaFW_pb2

# import gRPC modules for this feature
from .gRPC import CalibrationServicer_pb2 as CalibrationServicer_pb2
# from .gRPC import CalibrationServicer_pb2_grpc as CalibrationServicer_pb2_grpc

# import default arguments
from .CalibrationServicer_default_arguments import default_dict


# noinspection PyPep8Naming,PyUnusedLocal
class CalibrationServicerSimulation:
    """
    Implementation of the *Calibration Servicer* in *Simulation* mode
        This is a BlueVary Off-Gas Analytics Service
    """

    def __init__(self):
        """Class initialiser"""

        logging.debug('Started server in mode: {mode}'.format(mode='Simulation'))

    def _get_command_state(self, command_uuid: str) -> silaFW_pb2.ExecutionInfo:
        """
        Method to fill an ExecutionInfo message from the SiLA server for observable commands

        :param command_uuid: The uuid of the command for which to return the current state

        :return: An execution info object with the current command state
        """

        #: Enumeration of silaFW_pb2.ExecutionInfo.CommandStatus
        command_status = silaFW_pb2.ExecutionInfo.CommandStatus.waiting
        #: Real silaFW_pb2.Real(0...1)
        command_progress = None
        #: Duration silaFW_pb2.Duration(seconds=<seconds>, nanos=<nanos>)
        command_estimated_remaining = None
        #: Duration silaFW_pb2.Duration(seconds=<seconds>, nanos=<nanos>)
        command_lifetime_of_execution = None

        # TODO: check the state of the command with the given uuid and return the correct information

        # just return a default in this example
        return silaFW_pb2.ExecutionInfo(
            commandStatus=command_status,
            progressInfo=(
                command_progress if command_progress is not None else None
            ),
            estimatedRemainingTime=(
                command_estimated_remaining if command_estimated_remaining is not None else None
            ),
            updatedLifetimeOfExecution=(
                command_lifetime_of_execution if command_lifetime_of_execution is not None else None
            )
        )

    def StartCalibration(self, request, context: grpc.ServicerContext) \
            -> CalibrationServicer_pb2.StartCalibration_Responses:
        """
        Executes the unobservable command "Start Calibration"
            Start the 1-pt calibration of the sensors
    
        :param request: gRPC request containing the parameters passed:
            request.EmptyParameter (Empty Parameter): An empty parameter data type used if no parameter is required.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.CalibrationStatus (Calibration Status): Calibration command recieved. Calibration status response.
        """
    
        # initialise the return value
        return_value = None
    
        # TODO:
        #   Add implementation of Simulation for command StartCalibration here and write the resulting response
        #   in return_value
    
        # fallback to default
        if return_value is None:
            return_value = CalibrationServicer_pb2.StartCalibration_Responses(
                **default_dict['StartCalibration_Responses']
            )
    
        return return_value
    

    
