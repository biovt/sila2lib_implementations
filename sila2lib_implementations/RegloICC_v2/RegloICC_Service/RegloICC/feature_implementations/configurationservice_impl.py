from __future__ import annotations

import logging
import serial
from typing import Any, Dict

from sila2.framework import FullyQualifiedIdentifier

from ..generated.configurationservice import (
    ConfigurationServiceBase,
    GetBacksteps_Responses,
    GetTubingDiameter_Responses,
    ResetToDefault_Responses,
    SetBacksteps_Responses,
    SetTubingDiameter_Responses,
)
from .serial_connector import SharedProperties
from ..generated.configurationservice import (
    SerialConnectionError,
    SerialResponseError,
    TubingDiameterInvalidInput,
    SetBackstepsInvalidInput
)


class ConfigurationServiceImpl(ConfigurationServiceBase):
    def __init__(self, properties: SharedProperties):
        self.properties = properties

    def SetTubingDiameter(
        self, Channel: int, TubingDiameter: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetTubingDiameter_Responses:
        logging.debug(
            "SetTubingDiameter called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        allowed_tubing_diameters = [0.13, 0.19, 0.25, 0.38, 0.44, 0.51, 0.57, 0.64, 0.76, 0.89, 0.95, 1.02, 1.09, 1.14,
                                    1.22, 1.3, 1.42, 1.52, 1.65, 1.75, 1.85, 2.06, 2.29, 2.54, 2.79, 3.17]  # 2.79
        if TubingDiameter not in allowed_tubing_diameters:
            raise TubingDiameterInvalidInput('Wrong tube ID, see manual p.16 (of 36)')

        value = '{:0>5}'.format(TubingDiameter)       # formats the input to 5 digits
        value = value[0:2]+value[3:]              # Extracts 4 digits
        command = str.encode(f'{Channel}+{value}\r')
        try:
            if self.properties.simulation_mode:

                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetTubingDiameter command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('Setting Tubing Diameter of channel %s to %s is: %s' % (Channel, value, read))
            return SetTubingDiameter_Responses(
                TubingDiameterSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def ResetToDefault(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> ResetToDefault_Responses:
        logging.debug(
            "ResetToDefault called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}0\r')
        try:
            if self.properties.simulation_mode:

                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing ResetToDefault command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('Resetting pump %s : %s' % (Channel, read))
            return ResetToDefault_Responses(
                ResetStatus=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetTubingDiameter(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetTubingDiameter_Responses:
        logging.debug(
            "GetTubingDiameter called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}+\r')
        try:
            if self.properties.simulation_mode:

                read = 1.52
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if len(read) != 4:
                        raise SerialResponseError
                    read = float(read)
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetTubingDiameter command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Current tubing diameter setting for channel %s is: %s" % (Channel, read))
            return GetTubingDiameter_Responses(
                CurrentTubingDiameter=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetBacksteps(
        self, Channel: int, BackstepsSetting: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetBacksteps_Responses:
        logging.debug(
            "SetBacksteps called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        if BackstepsSetting > 9999 or BackstepsSetting < 0:
            raise SetBackstepsInvalidInput
        value = '{:0>5}'.format(BackstepsSetting)             # formats the input to 5 digits
        value = value[0:2]+value[3:]
        command = str.encode(f'{Channel}%{value}\r')
        try:
            if self.properties.simulation_mode:

                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetBacksteps command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('Setting backsteps for channel %s to %s: %s ' % (Channel, value, read))
            return SetBacksteps_Responses(
                BackstepsSettingSet=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetBacksteps(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> GetBacksteps_Responses:
        logging.debug(
            "GetBacksteps called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}%\r')
        try:
            if self.properties.simulation_mode:
                read = 9999
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if len(read) != 4:
                        raise SerialResponseError
                    read = int(read)
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetBacksteps command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Current backsteps setting on channel %s is: %s" % (Channel, read))
            return GetBacksteps_Responses(
                CurrentBackstepsSetting=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e
