from __future__ import annotations

import logging
import serial
from typing import Any, Dict

from sila2.framework import FullyQualifiedIdentifier

from ..generated.calibrationservice import (
    CalibrationServiceBase,
    CancelCalibration_Responses,
    GetCalibrationTime_Responses,
    GetDirectionCalibration_Responses,
    GetRunTimeCalibration_Responses,
    GetTargetVolume_Responses,
    SetActualVolume_Responses,
    SetCalibrationTime_Responses,
    SetDirectionCalibration_Responses,
    SetTargetVolume_Responses,
    StartCalibration_Responses,
)
from ..generated.calibrationservice import (
    SerialConnectionError,
    SerialResponseError
)
from .serial_connector import SharedProperties


class CalibrationServiceImpl(CalibrationServiceBase):
    def __init__(self, properties: SharedProperties):
        self.properties = properties

    def StartCalibration(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> StartCalibration_Responses:
        logging.debug(
            "StartCalibration called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        # Todo: Implement check if pump running. If running, no calibration start -> exception
        command = str.encode(f'{Channel}xY\r\n')
        try:
            if self.properties.simulation_mode:

                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    print(read)
                    if read != '*':
                        raise SerialResponseError
                    # elif read == '-':
                    # elif read == '#':
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing Start command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('Calibration started on channel %s: %s' % (Channel, read))
            return StartCalibration_Responses(
                StartCalibrationStatus=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def CancelCalibration(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> CancelCalibration_Responses:
        logging.debug(
            "CancelCalibration called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        # Todo: Implement check if pump running. If not running, no calibration to cancel -> exception
        command = str.encode(f'{Channel}xZ\r\n')
        try:
            if self.properties.simulation_mode:

                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing CancelCalibration command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('Calibration started on channel %s: %s' % (Channel, read))
            return CancelCalibration_Responses(
                CancelCalibrationStatus=read
            )
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetTargetVolume(
        self, Channel: int, TargetVolume: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetTargetVolume_Responses:
        logging.debug(
            "SetTargetVolume called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        value = format(float(TargetVolume), '.4E')                  # formats the input to scientific notation
        target_value = value[0:1]+value[2:5]+value[7:8]+value[9:]   # extracts 4 digits and +-E in pump input format
        command = str.encode(f'{Channel}xU{target_value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = float(target_value[:4]) * 0.001 * (
                            10 ** (float(target_value[5:])))  # translates answer from pump format to "human"
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if len(read) != 7:
                        raise SerialResponseError
                    read = float(read[:4]) * 0.001 * (
                                10 ** (float(read[5:])))  # translates answer from pump format to "human"
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetTargetVolume command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('Target Volume set on channel %s to: %s' % (Channel, read))
            return SetTargetVolume_Responses(TargetVolumeSet=read)
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetActualVolume(
        self, Channel: int, ActualVolume: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetActualVolume_Responses:
        logging.debug(
            "SetActualVolume called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        value = format(float(ActualVolume), '.4E')  # formats the input to scientific notation
        target_value = value[0:1] + value[2:5] + value[7:8] + value[
                                                              9:]  # extracts 4 digits and +-E in pump input format
        command = str.encode(f'{Channel}xV{target_value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = float(target_value[:4]) * 0.001 * (
                        10 ** (float(target_value[5:])))  # translates answer from pump format to "human"
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if len(read) != 7:
                        raise SerialResponseError
                    read = float(read[:4]) * 0.001 * (
                            10 ** (float(read[5:])))  # translates answer from pump format to "human"
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetActualVolume command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('Actual Volume set on channel %s to: %s' % (Channel, read))
            return SetActualVolume_Responses(ActualVolumeSet=read)
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetTargetVolume(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetTargetVolume_Responses:
        logging.debug(
            "GetTargetVolume called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}xU\r\n')
        try:
            if self.properties.simulation_mode:
                read = 9999.0
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if len(read) != 7:
                        raise SerialResponseError
                    read = float(read[:4]) * 0.001 * (
                            10 ** (float(read[5:])))  # translates answer from pump format to "human"
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetTargetVolume command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Current target volume setting on channel %s is: %s mL" %(Channel,read))
            return GetTargetVolume_Responses(CurrentTargetVolume=read)
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetDirectionCalibration(
        self, Channel: int, Direction: str, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetDirectionCalibration_Responses:
        logging.debug(
            "SetDirectionCalibration called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )

        command = str.encode(f'{Channel}xR{Direction}\r\n')
        try:
            if self.properties.simulation_mode:
                read = f'{Direction.capitalize()}'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError('Setting of calibration direction unsuccessful!')
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetDirectionCalibration command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('Calibration Direction on channel %s set to: %s' % (Channel, read))
            return SetDirectionCalibration_Responses(SetDirectionCalibrationSet=Direction.capitalize())
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetDirectionCalibration(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetDirectionCalibration_Responses:
        logging.debug(
            "GetDirectionCalibration called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )

        command = str.encode(f'{Channel}xR\r\n')
        try:
            if self.properties.simulation_mode:
                read = f'J'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != 'J' and read != 'K':
                        raise SerialResponseError('Response empty!')
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetDirectionCalibration command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('Rotation direction of channel %s is: %s' % (Channel, read))
            return GetDirectionCalibration_Responses(CurrentDirectionCalibration=read)
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def SetCalibrationTime(
        self, Channel: int, Time: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetCalibrationTime_Responses:
        logging.debug(
            "SetCalibrationTime called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        value = int(round(float(Time)*10, 1))            # eliminates '.'
        value = '{:0>8}'.format(value)                   # formats the input to 8 digits
        command = str.encode(f'{Channel}xW{value}\r\n')
        try:
            if self.properties.simulation_mode:
                read = 'Simulation: *'
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                    if read != '*':
                        raise SerialResponseError('Setting of calibration time unsuccessful!')
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing SetCalibrationTime command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug('Calibration time on channel %s set to %s: %s' % (Channel, value, read))
            logging.debug('Rotation direction of channel %s is: %s' % (Channel, read))
            return SetCalibrationTime_Responses(SetCalibrationTimeSet=read)
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetCalibrationTime(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetCalibrationTime_Responses:
        logging.debug(
            "GetCalibrationTime called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}xW\r\n')
        try:
            if self.properties.simulation_mode:
                read = int(35964000)
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = bytes.decode(self.properties.serial_connector.ser.readline().rstrip())
                    if read.isdecimal():
                        read = int(round(float(read), 2))  # answer is in s
                    else:
                        raise SerialResponseError('Getting of current calibration time unsuccessful!')
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetCalibrationTime command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Current calibration time on channel %s is: %s seconds" % (Channel, read))
            return GetCalibrationTime_Responses(CurrentCalibrationTime=read)
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e

    def GetRunTimeCalibration(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetRunTimeCalibration_Responses:
        logging.debug(
            "GetRunTimeCalibration called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        command = str.encode(f'{Channel}xW\r\n')
        try:
            if self.properties.simulation_mode:
                read = int(35964000)
                # raise SerialResponseError
                # raise SerialConnectionError
            else:
                try:
                    self.properties.serial_connector.ser.write(command)
                    read = bytes.decode(self.properties.serial_connector.ser.readline().rstrip())
                    if len(read) <= 8:
                        read = int(round(float(read), 2))  # answer is in s
                    else:
                        raise SerialResponseError('Getting of calibration run time unsuccessful!')
                except serial.SerialTimeoutException as e:
                    logging.exception("Error writing GetRunTimeCalibration command", e)
                    raise SerialConnectionError
                except (ValueError, UnicodeDecodeError) as e:
                    logging.exception(e)
                    raise SerialResponseError
                finally:
                    # Todo: Implement finally if necessary
                    pass
            logging.debug("Current run time since last calibration on channel %s is: %s seconds" % (Channel, read))
            return GetRunTimeCalibration_Responses(CurrentRunTimeCalibration=read)
        except Exception as e:
            # Todo: Figure out all possible exceptions and handle them
            logging.exception(e)
            raise e
