#!/bin/bash

# This is an example bash script for the start of the RegloICC_v2 server

echo "Executing bash script for RegloICC_v2 SiLA 2 Server start-up"

cd /home/pi/Desktop/sila2lib_implementations/sila2lib_implementations/RegloICC_v2/RegloICC_Service
/home/pi/.local/bin/poetry run python3 -m RegloICC -a "10.152.248.24" -p 50102 -u "524295_4"
