from os.path import dirname, join

from sila2.framework import Feature

ConfigurationServiceFeature = Feature(open(join(dirname(__file__), "ConfigurationService.sila.xml")).read())
