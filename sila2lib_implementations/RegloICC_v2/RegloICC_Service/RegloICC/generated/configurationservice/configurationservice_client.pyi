from __future__ import annotations

from typing import Iterable, Optional

from configurationservice_types import (
    GetBacksteps_Responses,
    GetTubingDiameter_Responses,
    ResetToDefault_Responses,
    SetBacksteps_Responses,
    SetTubingDiameter_Responses,
)

from sila2.client import ClientMetadataInstance

class ConfigurationServiceClient:
    """

            Set and retrieve the control parameter values for the configuration of the Reglo ICC pump.
    By Valeryia Sidarava, Institute of Biochemical Engineering, Technical University of Munich, 31.07.2019,
    Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 15.04.2020.
    Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 28.01.2022.

    """

    def SetTubingDiameter(
        self, Channel: int, TubingDiameter: float, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetTubingDiameter_Responses:
        """

        Set the inside diameter of the tubing in mm.

        """
        ...
    def ResetToDefault(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> ResetToDefault_Responses:
        """

        Resets all user configurable data to default values.

        """
        ...
    def GetTubingDiameter(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetTubingDiameter_Responses:
        """
        Get the inside diameter of the tubing in mm.
        """
        ...
    def SetBacksteps(
        self, Channel: int, BackstepsSetting: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetBacksteps_Responses:
        """

        Set the current backsteps setting using Discrete Type 2.

        """
        ...
    def GetBacksteps(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetBacksteps_Responses:
        """

        Get the current backsteps setting.

        """
        ...
