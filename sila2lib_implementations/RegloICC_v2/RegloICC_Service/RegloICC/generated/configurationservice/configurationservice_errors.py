from __future__ import annotations

from typing import Optional

from sila2.framework.errors.defined_execution_error import DefinedExecutionError

from .configurationservice_feature import ConfigurationServiceFeature


class TubingDiameterInvalidInput(DefinedExecutionError):
    def __init__(self, message: Optional[str] = None):
        if message is None:
            message = "The input value for tubing diameter the number must be real."
        super().__init__(
            ConfigurationServiceFeature.defined_execution_errors["TubingDiameterInvalidInput"], message=message
        )


class SetBackstepsInvalidInput(DefinedExecutionError):
    def __init__(self, message: Optional[str] = None):
        if message is None:
            message = "The input value for backsteps setting must be using Discrete Type 2 format."
        super().__init__(
            ConfigurationServiceFeature.defined_execution_errors["SetBackstepsInvalidInput"], message=message
        )


class SerialResponseError(DefinedExecutionError):
    def __init__(self, message: Optional[str] = None):
        if message is None:
            message = "The serial response could not be parsed. Device errors are not defined."
        super().__init__(ConfigurationServiceFeature.defined_execution_errors["SerialResponseError"], message=message)


class SerialConnectionError(DefinedExecutionError):
    def __init__(self, message: Optional[str] = None):
        if message is None:
            message = "Serial connection is not available. Make sure the serial cable is connected and the right serial port used!"
        super().__init__(ConfigurationServiceFeature.defined_execution_errors["SerialConnectionError"], message=message)
