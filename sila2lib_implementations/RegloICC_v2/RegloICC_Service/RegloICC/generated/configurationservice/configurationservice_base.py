from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Any, Dict

from sila2.framework import FullyQualifiedIdentifier
from sila2.server import FeatureImplementationBase

from .configurationservice_types import (
    GetBacksteps_Responses,
    GetTubingDiameter_Responses,
    ResetToDefault_Responses,
    SetBacksteps_Responses,
    SetTubingDiameter_Responses,
)


class ConfigurationServiceBase(FeatureImplementationBase, ABC):

    """

            Set and retrieve the control parameter values for the configuration of the Reglo ICC pump.
    By Valeryia Sidarava, Institute of Biochemical Engineering, Technical University of Munich, 31.07.2019,
    Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 15.04.2020.
    Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 28.01.2022.

    """

    @abstractmethod
    def SetTubingDiameter(
        self, Channel: int, TubingDiameter: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetTubingDiameter_Responses:
        """

        Set the inside diameter of the tubing in mm.



        :param Channel:
                The channel the tubing diameter is set for.


        :param TubingDiameter:
                The inside diameter of the tubing in mm. (must be one of following list:
                [0.13,0.19,0.25,0.38,0.44,0.51,0.57,0.64,0.76,0.89,0.95,1.02,1.09,1.14,1.22,1.3,1.42,1.52,1.65,1.75,1.85,2.06,2.29,2.54,2.79,3.17])


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - TubingDiameterSet: Tubing Diameter succeeded to Set.


        """
        pass

    @abstractmethod
    def ResetToDefault(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> ResetToDefault_Responses:
        """

        Resets all user configurable data to default values.



        :param Channel:
                The channel for  which the settings are set back to default.


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - ResetStatus: Reset of pump succeeded.


        """
        pass

    @abstractmethod
    def GetTubingDiameter(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetTubingDiameter_Responses:
        """
        Get the inside diameter of the tubing in mm.


        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - CurrentTubingDiameter: Current the inside diameter of the tubing in mm.


        """
        pass

    @abstractmethod
    def SetBacksteps(
        self, Channel: int, BackstepsSetting: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetBacksteps_Responses:
        """

        Set the current backsteps setting using Discrete Type 2.



        :param Channel:
                The channel (1-4).


        :param BackstepsSetting:
                Set the current backsteps setting using Discrete Type 2.


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - BackstepsSettingSet: Backsteps Setting succeeded to Set.


        """
        pass

    @abstractmethod
    def GetBacksteps(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> GetBacksteps_Responses:
        """

        Get the current backsteps setting.



        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - CurrentBackstepsSetting: Get the current backsteps  setting.


        """
        pass
