from __future__ import annotations

from typing import Iterable, Optional

from calibrationservice_types import (
    CancelCalibration_Responses,
    GetCalibrationTime_Responses,
    GetDirectionCalibration_Responses,
    GetRunTimeCalibration_Responses,
    GetTargetVolume_Responses,
    SetActualVolume_Responses,
    SetCalibrationTime_Responses,
    SetDirectionCalibration_Responses,
    SetTargetVolume_Responses,
    StartCalibration_Responses,
)

from sila2.client import ClientMetadataInstance

class CalibrationServiceClient:
    """

            Set and retrieve the control parameter values for calibration of the Reglo ICC pump.
    By Valeryia Sidarava, Institute of Biochemical Engineering, Technical University of Munich, 31.07.2019,
    Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 15.04.2020.
    Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 28.01.2022.

    """

    def StartCalibration(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> StartCalibration_Responses:
        """

        Starts calibration on a channel(s).

        """
        ...
    def CancelCalibration(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> CancelCalibration_Responses:
        """

        Cancels calibration.

        """
        ...
    def SetTargetVolume(
        self, Channel: int, TargetVolume: float, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetTargetVolume_Responses:
        """

        Set target volume to pump for calibrating in mL (using Volume Type 1).

        """
        ...
    def SetActualVolume(
        self, Channel: int, ActualVolume: float, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetActualVolume_Responses:
        """

        Set the actual volume measured during calibration in mL (using Volume Type 1).

        """
        ...
    def GetTargetVolume(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetTargetVolume_Responses:
        """
        Get target volume to pump for calibrating in mL.
        """
        ...
    def SetDirectionCalibration(
        self, Channel: int, Direction: str, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetDirectionCalibration_Responses:
        """

        Set direction flow for calibration J or K using DIRECTION format.

        """
        ...
    def GetDirectionCalibration(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetDirectionCalibration_Responses:
        """

        Get direction flow for calibration (J for CW or K for CCW).

        """
        ...
    def SetCalibrationTime(
        self, Channel: int, Time: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetCalibrationTime_Responses:
        """

        Set the current calibration time using Time Type 2 format (unit 0.1 s, 0-35964000, or 0 to 999 hr) .

        """
        ...
    def GetCalibrationTime(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetCalibrationTime_Responses:
        """

        Get the current calibration time in 0.1 s (Time Type 1).

        """
        ...
    def GetRunTimeCalibration(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetRunTimeCalibration_Responses:
        """

        Get the channel run time since last calibration in 0.1 s (Time Type 2).

        """
        ...
