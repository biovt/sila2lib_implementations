from os.path import dirname, join

from sila2.framework import Feature

CalibrationServiceFeature = Feature(open(join(dirname(__file__), "CalibrationService.sila.xml")).read())
