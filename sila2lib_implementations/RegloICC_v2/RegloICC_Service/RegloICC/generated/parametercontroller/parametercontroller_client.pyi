from __future__ import annotations

from typing import Iterable, Optional

from parametercontroller_types import (
    GetCurrentRunTime_Responses,
    GetCycleCount_Responses,
    GetDispenseTimeMlMin_Responses,
    GetDispenseTimeRPM_Responses,
    GetFlowRate_Responses,
    GetFlowRateAtModes_Responses,
    GetMaximumFlowRate_Responses,
    GetMaximumFlowRateWithCalibration_Responses,
    GetMode_Responses,
    GetPumpingPauseTime_Responses,
    GetSpeedSettingRPM_Responses,
    GetVolume_Responses,
    SetCurrentRunTime_Responses,
    SetCycleCount_Responses,
    SetFlowRate_Responses,
    SetFlowRateAtModes_Responses,
    SetFlowRateMode_Responses,
    SetPumpingPauseTime_Responses,
    SetRPMMode_Responses,
    SetSpeedSettingRPM_Responses,
    SetTimeMode_Responses,
    SetTimePauseMode_Responses,
    SetVolume_Responses,
    SetVolumePauseMode_Responses,
    SetVolumeRateMode_Responses,
    SetVolumeTimeMode_Responses,
)

from sila2.client import ClientMetadataInstance

class ParameterControllerClient:
    """

            Set and retrieve information regarding the parameter settings of the Reglo ICC pump.
    By Valeryia Sidarava, Institute of Biochemical Engineering, Technical University of Munich, 31.07.2019,
    Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 15.04.2020.
    Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 28.01.2022.

    """

    def SetFlowRate(
        self, Channel: int, SetFlowRate: float, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetFlowRate_Responses:
        """
        Set the desired flow rate of the pump, mL/min
        """
        ...
    def SetRPMMode(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetRPMMode_Responses:
        """

        Set the current channel/pump mode to RPM mode.

        """
        ...
    def SetFlowRateMode(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetFlowRateMode_Responses:
        """

        Set the current channel/pump mode to Flow Rate mode.

        """
        ...
    def SetVolumeRateMode(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetVolumeRateMode_Responses:
        """

        Set the current channel/pump mode to Volume (at Rate) mode.

        """
        ...
    def SetVolumeTimeMode(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetVolumeTimeMode_Responses:
        """

        Set the current channel/pump mode to Volume (over Time) mode.

        """
        ...
    def SetVolumePauseMode(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetVolumePauseMode_Responses:
        """

        Set the current channel/pump mode to Volume+Pause mode.

        """
        ...
    def SetTimeMode(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetTimeMode_Responses:
        """

        Set the current channel/pump mode to Time mode.

        """
        ...
    def SetTimePauseMode(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetTimePauseMode_Responses:
        """

        Set the current channel/pump mode to Time+Pause mode.

        """
        ...
    def SetVolume(
        self, Channel: int, Volume: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetVolume_Responses:
        """

        Set the current setting for volume in mL. Volume Type 2.

        """
        ...
    def GetFlowRate(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetFlowRate_Responses:
        """
        Get current flow rate (mL/min).
        """
        ...
    def GetMode(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetMode_Responses:
        """
        Get the current channel or pump mode
        """
        ...
    def GetVolume(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetVolume_Responses:
        """
        Get the current setting for volume in mL/min.
        """
        ...
    def GetMaximumFlowRate(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetMaximumFlowRate_Responses:
        """
        Get maximum flow rate achievable with current settings, mL/min.
        """
        ...
    def GetMaximumFlowRateWithCalibration(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetMaximumFlowRateWithCalibration_Responses:
        """
        Get maximum flow rate achievable with current settings using calibration, mL/min.
        """
        ...
    def GetSpeedSettingRPM(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetSpeedSettingRPM_Responses:
        """
        Get the current speed setting in RPM.
        """
        ...
    def SetSpeedSettingRPM(
        self, Channel: int, Speed: float, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetSpeedSettingRPM_Responses:
        """
        Set speed setting in RPM (RPM Mode flow rate setting (0.01 RPM) Discrete Type 3).
        """
        ...
    def SetCurrentRunTime(
        self, Channel: int, RunTime: float, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetCurrentRunTime_Responses:
        """
        Set current pump run time using Time Type 2 format (0 to 999h or 0 to 3596400s).
        """
        ...
    def GetCurrentRunTime(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetCurrentRunTime_Responses:
        """
        Get current pump run time using Time Type 2 format.
        """
        ...
    def SetPumpingPauseTime(
        self, Channel: int, PumpingPauseTime: float, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetPumpingPauseTime_Responses:
        """
        Set pumping pause time using Time Type 2 format (0 to 999h or 0 to 3596400s).
        """
        ...
    def GetPumpingPauseTime(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetPumpingPauseTime_Responses:
        """
        Get pumping pause time.
        """
        ...
    def GetCycleCount(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetCycleCount_Responses:
        """
        Get pump cycle count.
        """
        ...
    def SetCycleCount(
        self, Channel: int, CycleCount: float, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetCycleCount_Responses:
        """
        Set pump cycle count (Discrete Type 2).
        """
        ...
    def GetDispenseTimeMlMin(
        self,
        Channel: int,
        Volume: float,
        FlowRate: float,
        *,
        metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetDispenseTimeMlMin_Responses:
        """
        Get time to dispense at a given volume at a given mL/min flow rate.
        """
        ...
    def GetDispenseTimeRPM(
        self,
        Channel: int,
        Volume: float,
        FlowRate: float,
        *,
        metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetDispenseTimeRPM_Responses:
        """
        Get time to dispense at a given volume at a given RPM.
        """
        ...
    def GetFlowRateAtModes(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetFlowRateAtModes_Responses:
        """
        Get flow rate from RPM (S) or flow rate (f) when mode is not RPM or Flow Rate.: 0=RPM, 1=Flow Rate
        """
        ...
    def SetFlowRateAtModes(
        self, Channel: int, FlowRate: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetFlowRateAtModes_Responses:
        """
        Set RPM flow rate not in RPM or Flow Rate mode. (Discrete Type 3)
        """
        ...
