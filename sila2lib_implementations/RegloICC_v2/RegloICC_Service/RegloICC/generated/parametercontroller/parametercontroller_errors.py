from __future__ import annotations

from typing import Optional

from sila2.framework.errors.defined_execution_error import DefinedExecutionError

from .parametercontroller_feature import ParameterControllerFeature


class SerialResponseError(DefinedExecutionError):
    def __init__(self, message: Optional[str] = None):
        if message is None:
            message = "The serial response could not be parsed. Device errors are not defined."
        super().__init__(ParameterControllerFeature.defined_execution_errors["SerialResponseError"], message=message)


class SerialConnectionError(DefinedExecutionError):
    def __init__(self, message: Optional[str] = None):
        if message is None:
            message = "Serial connection is not available. Make sure the serial cable is connected and the right serial port used!"
        super().__init__(ParameterControllerFeature.defined_execution_errors["SerialConnectionError"], message=message)


class ChannelSettingsError(DefinedExecutionError):
    def __init__(self, message: Optional[str] = None):
        if message is None:
            message = "The channel setting(s) are not correct or unachievable"
        super().__init__(ParameterControllerFeature.defined_execution_errors["ChannelSettingsError"], message=message)

