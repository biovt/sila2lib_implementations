from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Any, Dict

from sila2.framework import FullyQualifiedIdentifier
from sila2.server import FeatureImplementationBase

from .parametercontroller_types import (
    GetCurrentRunTime_Responses,
    GetCycleCount_Responses,
    GetDispenseTimeMlMin_Responses,
    GetDispenseTimeRPM_Responses,
    GetFlowRate_Responses,
    GetFlowRateAtModes_Responses,
    GetMaximumFlowRate_Responses,
    GetMaximumFlowRateWithCalibration_Responses,
    GetMode_Responses,
    GetPumpingPauseTime_Responses,
    GetSpeedSettingRPM_Responses,
    GetVolume_Responses,
    SetCurrentRunTime_Responses,
    SetCycleCount_Responses,
    SetFlowRate_Responses,
    SetFlowRateAtModes_Responses,
    SetFlowRateMode_Responses,
    SetPumpingPauseTime_Responses,
    SetRPMMode_Responses,
    SetSpeedSettingRPM_Responses,
    SetTimeMode_Responses,
    SetTimePauseMode_Responses,
    SetVolume_Responses,
    SetVolumePauseMode_Responses,
    SetVolumeRateMode_Responses,
    SetVolumeTimeMode_Responses,
)


class ParameterControllerBase(FeatureImplementationBase, ABC):

    """

            Set and retrieve information regarding the parameter settings of the Reglo ICC pump.
    By Valeryia Sidarava, Institute of Biochemical Engineering, Technical University of Munich, 31.07.2019,
    Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 15.04.2020.
    Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 28.01.2022.

    """

    @abstractmethod
    def SetFlowRate(
        self, Channel: int, SetFlowRate: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetFlowRate_Responses:
        """
        Set the desired flow rate of the pump, mL/min


        :param Channel:
                The channel (1-4).


        :param SetFlowRate:
                Set the desired RPM flow rate in volume/time mode, mL/min


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - SetFlowRateSet: Set Flow Rate succeeded to Set.


        """
        pass

    @abstractmethod
    def SetRPMMode(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> SetRPMMode_Responses:
        """

            Set the current channel/pump mode to RPM mode.



        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - PumpModeSet: Pump mode succeeded to Set.


        """
        pass

    @abstractmethod
    def SetFlowRateMode(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetFlowRateMode_Responses:
        """

        Set the current channel/pump mode to Flow Rate mode.



        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - PumpModeSet: Pump mode succeeded to Set.


        """
        pass

    @abstractmethod
    def SetVolumeRateMode(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetVolumeRateMode_Responses:
        """

        Set the current channel/pump mode to Volume (at Rate) mode.



        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - PumpModeSet: Pump mode succeeded to Set.


        """
        pass

    @abstractmethod
    def SetVolumeTimeMode(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetVolumeTimeMode_Responses:
        """

        Set the current channel/pump mode to Volume (over Time) mode.



        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - PumpModeSet: Pump mode succeeded to Set.


        """
        pass

    @abstractmethod
    def SetVolumePauseMode(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetVolumePauseMode_Responses:
        """

         Set the current channel/pump mode to Volume+Pause mode.



        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - PumpModeSet: Pump mode succeeded to Set.


        """
        pass

    @abstractmethod
    def SetTimeMode(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> SetTimeMode_Responses:
        """

        Set the current channel/pump mode to Time mode.



        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - PumpModeSet: Pump mode succeeded to Set.


        """
        pass

    @abstractmethod
    def SetTimePauseMode(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetTimePauseMode_Responses:
        """

        Set the current channel/pump mode to Time+Pause mode.



        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - PumpModeSet: Pump mode succeeded to Set.


        """
        pass

    @abstractmethod
    def SetVolume(
        self, Channel: int, Volume: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetVolume_Responses:
        """

        Set the current setting for volume in mL. Volume Type 2.



        :param Channel:
                The channel (1-4).


        :param Volume:
                Set the current setting for volume in mL. Volume Type 2.


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - VolumeSet: Volume succeeded to Set


        """
        pass

    @abstractmethod
    def GetFlowRate(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> GetFlowRate_Responses:
        """
        Get current flow rate (mL/min).


        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - CurrentFlowRate: Current flow rate of the channel.


        """
        pass

    @abstractmethod
    def GetMode(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> GetMode_Responses:
        """
        Get the current channel or pump mode


        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - CurrentPumpMode: Current channel or pump mode is retrieved.


        """
        pass

    @abstractmethod
    def GetVolume(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> GetVolume_Responses:
        """
        Get the current setting for volume in mL/min.


        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - CurrentVolume: Current setting for volume in mL/min.


        """
        pass

    @abstractmethod
    def GetMaximumFlowRate(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetMaximumFlowRate_Responses:
        """
        Get maximum flow rate achievable with current settings, mL/min.


        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - MaximumFlowRate: Maximum Flow Rate achievable with current settings, mL/min.


        """
        pass

    @abstractmethod
    def GetMaximumFlowRateWithCalibration(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetMaximumFlowRateWithCalibration_Responses:
        """
        Get maximum flow rate achievable with current settings using calibration, mL/min.


        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - MaximumFlowRateWithCalibration: Maximum Flow Rate achievable with current settings using calibration, mL/min.


        """
        pass

    @abstractmethod
    def GetSpeedSettingRPM(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetSpeedSettingRPM_Responses:
        """
        Get the current speed setting in RPM.


        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - CurrentSpeedSetting: The current speed setting in RPM.


        """
        pass

    @abstractmethod
    def SetSpeedSettingRPM(
        self, Channel: int, Speed: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetSpeedSettingRPM_Responses:
        """
        Set speed setting in RPM (RPM Mode flow rate setting (0.01 RPM) Discrete Type 3).


        :param Channel:
                The channel (1-4).


        :param Speed:
                Speed setting in RPM (RPM Mode flow rate setting (0.01 RPM) Discrete Type 3)


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - CurrentSpeedSetting: RPM Mode flow rate setting.


        """
        pass

    @abstractmethod
    def SetCurrentRunTime(
        self, Channel: int, RunTime: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetCurrentRunTime_Responses:
        """
        Set current pump run time using Time Type 2 format (0 to 999h or 0 to 3596400s).


        :param Channel:
                The channel (1-4).


        :param RunTime:
                Set current pump run time using Time Type 2 format (0 to 999h or 0 to 3596400s).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - SetCurrentRunTimeSet: Current Run Time succeeded to set.


        """
        pass

    @abstractmethod
    def GetCurrentRunTime(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetCurrentRunTime_Responses:
        """
        Get current pump run time using Time Type 2 format.


        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - GetCurrentRunTime: Current Run Time of the pump.


        """
        pass

    @abstractmethod
    def SetPumpingPauseTime(
        self, Channel: int, PumpingPauseTime: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetPumpingPauseTime_Responses:
        """
        Set pumping pause time using Time Type 2 format (0 to 999h or 0 to 3596400s).


        :param Channel:
                The channel (1-4).


        :param PumpingPauseTime:
                Set pumping pause time using Time Type 2 format (0 to 999h or 0 to 3596400s).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - SetPumpingPauseTimeSet: Pumping pause time succeeded to set.


        """
        pass

    @abstractmethod
    def GetPumpingPauseTime(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetPumpingPauseTime_Responses:
        """
        Get pumping pause time.


        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - CurrentPumpingPauseTime: Current Pumping Pause Time.


        """
        pass

    @abstractmethod
    def GetCycleCount(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> GetCycleCount_Responses:
        """
        Get pump cycle count.


        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - CurrentCycleCount: Current Cycle Count.


        """
        pass

    @abstractmethod
    def SetCycleCount(
        self, Channel: int, CycleCount: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetCycleCount_Responses:
        """
        Set pump cycle count (Discrete Type 2).


        :param Channel:
                The channel (1-4).


        :param CycleCount:
                Set pump cycle count


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - SetCycleCountSet: Cycle Count succeeded to set.


        """
        pass

    @abstractmethod
    def GetDispenseTimeMlMin(
        self, Channel: int, Volume: float, FlowRate: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetDispenseTimeMlMin_Responses:
        """
        Get time to dispense at a given volume at a given mL/min flow rate.


        :param Channel:
                The channel (1-4).


        :param Volume:
                Gets the volume, which must be dispensed


        :param FlowRate:
                Gets the flow rate, with which the volume must be dispensed


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - CurrentDispenseTime: CurrentDispenseTime at a given volume at a given mL/min flow rate.


        """
        pass

    @abstractmethod
    def GetDispenseTimeRPM(
        self, Channel: int, Volume: float, FlowRate: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetDispenseTimeRPM_Responses:
        """
        Get time to dispense at a given volume at a given RPM.


        :param Channel:
                The channel (1-4).


        :param Volume:
                Gets the volume, which must be dispensed


        :param FlowRate:
                Gets the flow rate, with which the volume must be dispensed


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - CurrentDispenseTime: CurrentDispenseTime at a given volume at a given RPM.


        """
        pass

    @abstractmethod
    def GetFlowRateAtModes(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetFlowRateAtModes_Responses:
        """
        Get flow rate from RPM (S) or flow rate (f) when mode is not RPM or Flow Rate.: 0=RPM, 1=Flow Rate


        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - CurrentFlowRate: CurrentDispenseTime at a given volume at a given RPM. : 0=RPM, 1=Flow Rate


        """
        pass

    @abstractmethod
    def SetFlowRateAtModes(
        self, Channel: int, FlowRate: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetFlowRateAtModes_Responses:
        """
        Set RPM flow rate not in RPM or Flow Rate mode. (Discrete Type 3)


        :param Channel:
                The channel (1-4).


        :param FlowRate:
                Set RPM flow rate not in RPM or Flow Rate mode.


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - SetFlowRateSet: RPM flow rate not in RPM or Flow Rate mode succeeded to set


        """
        pass
