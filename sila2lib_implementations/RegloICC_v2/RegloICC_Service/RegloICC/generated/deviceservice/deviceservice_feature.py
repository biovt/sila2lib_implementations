from os.path import dirname, join

from sila2.framework import Feature

DeviceServiceFeature = Feature(open(join(dirname(__file__), "DeviceService.sila.xml")).read())
