from __future__ import annotations

from typing import Iterable, Optional

from deviceservice_types import (
    GetVersionSoftware_Responses,
    GetChannelAddressing_Responses,
    GetChannelNumber_Responses,
    GetChannelTotalVolume_Responses,
    GetEventMessages_Responses,
    GetFootSwitchStatus_Responses,
    GetHeadModel_Responses,
    GetLanguage_Responses,
    GetPauseTime_Responses,
    GetPumpStatus_Responses,
    GetRevolutions_Responses,
    GetRollersNumber_Responses,
    GetRollerStepsHigh_Responses,
    GetRollerStepsLow_Responses,
    GetRSV_Responses,
    GetSerialNumber_Responses,
    GetSerialProtocol_Responses,
    GetTimeSetting_Responses,
    GetTotalTime_Responses,
    GetTotalVolume_Responses,
    GetVersionType_Responses,
    ResetRSVTable_Responses,
    SaveSetRoller_Responses,
    SaveSettings_Responses,
    SetChannelAddressing_Responses,
    SetChannelNumber_Responses,
    SetDisableInterface_Responses,
    SetDisplayLetters_Responses,
    SetDisplayNumbers_Responses,
    SetEventMessages_Responses,
    SetHeadModel_Responses,
    SetLanguage_Responses,
    SetNonFactoryRSV_Responses,
    SetPauseTime_Responses,
    SetPauseTimeH_Responses,
    SetPauseTimeM_Responses,
    SetPumpAddress_Responses,
    SetPumpName_Responses,
    SetPumpSerialNumber_Responses,
    SetRollersNumber_Responses,
    SetRollerStepsHigh_Responses,
    SetRollerStepsLow_Responses,
    SetRSV_Responses,
    SetRSVReset_Responses,
    SetRunTimeH_Responses,
    SetRunTimeM_Responses,
    SetTimeSetting_Responses,
    SetUserInterface_Responses,
)

from sila2.client import ClientMetadataInstance

class DeviceServiceClient:
    """

    General device software and hardware information can be retrieved and changed within this function.
    By Valeryia Sidarava, Institute of Biochemical Engineering, Technical University of Munich, 31.07.2019 and
    Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 15.04.2020
    Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 28.01.2022.

    """

    def SetPumpAddress(
        self, Address: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetPumpAddress_Responses:
        """

        Set the address of the pump (1-8).

        """
        ...
    def SetLanguage(
        self, Address: int, Language: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetLanguage_Responses:
        """

        Set the language of the pump.

        """
        ...
    def GetLanguage(
        self, Address: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetLanguage_Responses:
        """
        Get The language of the pump. 0=English / 1=French / 2=Spanish / 3=German.
        """
        ...
    def GetPumpStatus(
        self, Address: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetPumpStatus_Responses:
        """
        Get pump status. +=running, -=stopped/standby.
        """
        ...
    def GetVersionType(
        self, Address: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetVersionType_Responses:
        """
        Get pump information. Response is string of model description (variable length), software version (3 digits) and pump head model type code (3 digits).
        """
        ...
    def GetVersionSoftware(
        self, Address: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetVersionSoftware_Responses:
        """
        Get pump software version. Response is string.
        """
        ...
    def GetSerialNumber(
        self, Address: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetSerialNumber_Responses:
        """
        Get pump serial number.
        """
        ...
    def SetChannelAddressing(
        self, Address: int, ChannelAddressing: bool, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetChannelAddressing_Responses:
        """

        Set whether (1) or not (0) channel addressing is enabled.

        """
        ...
    def GetChannelAddressing(
        self, Address: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetChannelAddressing_Responses:
        """
        Get information whether (1) or not (0) channel addressing is enabled.
        """
        ...
    def SetEventMessages(
        self, Address: int, EventMessages: bool, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetEventMessages_Responses:
        """

        Set whether (1) or not (0) event messages are enabled.

        """
        ...
    def GetEventMessages(
        self, Address: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetEventMessages_Responses:
        """
        Get information whether (1) or not (0) event messages are enabled.
        """
        ...
    def GetSerialProtocol(
        self, Address: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetSerialProtocol_Responses:
        """
        Get an integer representing the version of the serial protocol.
        """
        ...
    def SetPumpName(
        self, Address: int, PumpName: str, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetPumpName_Responses:
        """

        Set pump name for display under remote control string.

        """
        ...
    def GetChannelNumber(
        self, Address: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetChannelNumber_Responses:
        """
        Get number of pump channels.
        """
        ...
    def SetChannelNumber(
        self, Address: int, ChannelNumber: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetChannelNumber_Responses:
        """

        Set number of pump channels.

        """
        ...
    def GetRevolutions(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetRevolutions_Responses:
        """
        Get total number of revolutions.
        """
        ...
    def GetChannelTotalVolume(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetChannelTotalVolume_Responses:
        """
        Get channel total volume pumped since last reset, mL.
        """
        ...
    def GetTotalTime(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetTotalTime_Responses:
        """
        Get total time pumped for a channel since last reset, mL.
        """
        ...
    def GetHeadModel(
        self, Address: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetHeadModel_Responses:
        """
        Get pump head model type code indicating the ID number of the pump head. The first two digits encode the number of channels on the head, and the second 2 digits represent the number of rollers.
        """
        ...
    def SetHeadModel(
        self, Address: int, HeadModel: str, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetHeadModel_Responses:
        """

        Set pump head model type code up to 4 digits indicating the ID number of the pump head. The first two digits encode the number of channels on the head, and the second 2 digits represent the number of rollers

        """
        ...
    def SetUserInterface(
        self, Address: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetUserInterface_Responses:
        """

        Set control from the pump user interface

        """
        ...
    def SetDisableInterface(
        self, Address: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetDisableInterface_Responses:
        """

        Disable pump user interface

        """
        ...
    def SetDisplayNumbers(
        self, Address: int, DisplayNumbers: str, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetDisplayNumbers_Responses:
        """

        Write numbers to the pump to display while under external control - string (less than 17 characters)

        """
        ...
    def SetDisplayLetters(
        self, Address: int, DisplayLetters: str, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetDisplayLetters_Responses:
        """

        Write letters to the pump to display while under external control - string (less than 17 characters)

        """
        ...
    def GetTimeSetting(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetTimeSetting_Responses:
        """
        Get current setting for pump time in 1/10 seconds.
        """
        ...
    def SetTimeSetting(
        self, Channel: int, TimeSetting: float, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetTimeSetting_Responses:
        """

        Set current setting for pump time in 1/10 seconds.

        """
        ...
    def SetRunTimeM(
        self, Channel: int, RunTimeM: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetRunTimeM_Responses:
        """

        Set current run time setting for dispensing in minutes (0-999)

        """
        ...
    def SetRunTimeH(
        self, Channel: int, RunTimeH: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetRunTimeH_Responses:
        """

        Set current run time setting for dispensing in hours (0-999)

        """
        ...
    def GetRollerStepsLow(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetRollerStepsLow_Responses:
        """
        Get current setting for volume to pump in low order roller steps. The total number of roller steps
        which are dispensed during an operation is computed as [ u*65536 + U].
        """
        ...
    def SetRollerStepsLow(
        self, Channel: int, RollerStepsLow: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetRollerStepsLow_Responses:
        """

        Set current setting for volume to pump in roller steps (0 - 65536).

        """
        ...
    def GetRollerStepsHigh(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetRollerStepsHigh_Responses:
        """
        Get current setting for volume to pump in high order roller steps. The total number of roller steps
            which are dispensed during an operation is computed as [ u*65536 + U].
        """
        ...
    def SetRollerStepsHigh(
        self, Channel: int, RollerSteps: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetRollerStepsHigh_Responses:
        """

        Set current setting for volume to pump in roller steps (greater than 65536).

        """
        ...
    def GetRSV(self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None) -> GetRSV_Responses:
        """
        Get the pump's current setting for roller step volume (RSV) based on the current calibration,
        tubing diameter and roller count in nL. If no calibration has been performed, the default volume is returned
        """
        ...
    def SetRSV(
        self, Channel: int, RSV: float, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetRSV_Responses:
        """

        Set the calibrated roller step volume (RSV) in nL to use for this pump/channel.
        The value is used as calibration value and is overwritten by subsequent calibrations and reset by changing tubing diameter.

        """
        ...
    def SetRSVReset(
        self, Address: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetRSVReset_Responses:
        """

        Reset the pump to discard calibration data, use default roller step volume

        """
        ...
    def ResetRSVTable(
        self, Address: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> ResetRSVTable_Responses:
        """

        Reset roller step value table to default

        """
        ...
    def SetNonFactoryRSV(
        self,
        Channel: int,
        RollerCount: int,
        TubingID: str,
        NonFactoryRSV: float,
        *,
        metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetNonFactoryRSV_Responses:
        """

        Change factory roller step volume for a particular roller count and tubing size using roller count (6,8,12);
        index of the tubing diameter; RSV

        """
        ...
    def GetPauseTime(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetPauseTime_Responses:
        """
        Get current setting for pause time in 1/10 seconds
        """
        ...
    def SetPauseTime(
        self, Channel: int, PauseTime: float, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetPauseTime_Responses:
        """

        Set current setting for pause time in 1/10 seconds

        """
        ...
    def SetPauseTimeM(
        self, Channel: int, PauseTimeM: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetPauseTimeM_Responses:
        """

        Set current setting for pause time in minutes (0-999)

        """
        ...
    def SetPauseTimeH(
        self, Channel: int, PauseTimeH: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetPauseTimeH_Responses:
        """

        Set current setting for pause time in hours (0-999)

        """
        ...
    def GetTotalVolume(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetTotalVolume_Responses:
        """
        Get total volume dispensed since the last reset in uL, mL or L.
        """
        ...
    def SaveSettings(
        self, Address: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SaveSettings_Responses:
        """
        Saves the current pump settings values to memory.
        """
        ...
    def SaveSetRoller(
        self, Address: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SaveSetRoller_Responses:
        """
        Saves set roller step settings.
        """
        ...
    def GetFootSwitchStatus(
        self, Address: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetFootSwitchStatus_Responses:
        """
        Get the current status of the foot switch:   - open  /  + grounded.
        """
        ...
    def SetRollersNumber(
        self, Channel: int, RollersNumber: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetRollersNumber_Responses:
        """

        Set number of rollers for channel

        """
        ...
    def GetRollersNumber(
        self, Channel: int, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> GetRollersNumber_Responses:
        """
        Get number of rollers for channel
        """
        ...
    def SetPumpSerialNumber(
        self, Address: int, PumpSerialNumber: str, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> SetPumpSerialNumber_Responses:
        """

        Set pump serial number string.

        """
        ...
