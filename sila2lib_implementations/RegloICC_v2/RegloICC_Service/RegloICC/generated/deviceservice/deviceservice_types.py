from __future__ import annotations

from typing import NamedTuple


class SetPumpAddress_Responses(NamedTuple):

    PumpAddressSet: str
    """
    Pump Address succeeded to Set.
    """


class SetLanguage_Responses(NamedTuple):

    LanguageSet: str
    """
    Language of the pump succeeded to Set.
    """


class GetLanguage_Responses(NamedTuple):

    CurrentLanguage: int
    """
    Current language of the pump. 0=English / 1=French / 2=Spanish / 3=German.
    """


class GetPumpStatus_Responses(NamedTuple):

    CurrentPumpStatus: str
    """
    Current pump status. +=running, -=stopped/standby.
    """


class GetVersionType_Responses(NamedTuple):

    CurrentVersionType: str
    """
    Current pump information. Response is string of model description (variable length), software version (3 digits) and pump head model type code (3 digits). 
    """


class GetVersionSoftware_Responses(NamedTuple):

    CurrentVersionSoftware: str
    """
    Get pump software version. Response is string. 
    """


class GetSerialNumber_Responses(NamedTuple):

    SerialNumber: str
    """
    Pump serial number query. 
    """


class SetChannelAddressing_Responses(NamedTuple):

    ChannelAddressingSet: bool
    """
    Channel Addressing of the pump succeeded to Set.
    """


class GetChannelAddressing_Responses(NamedTuple):

    ChannelAddressing: bool
    """
    Information whether or not channel addressing is enabled String of form XXXXXXX; 0=disabled,1=enabled. 
    """


class SetEventMessages_Responses(NamedTuple):

    EventMessagesSet: bool
    """
    Event Messages of the pump succeeded to Set.
    """


class GetEventMessages_Responses(NamedTuple):

    EventMessages: bool
    """
    information whether or not event messages are enabled; 0=disabled,1=enabled. 
    """


class GetSerialProtocol_Responses(NamedTuple):

    SerialProtocol: int
    """
    Integer representing the version of the serial protocol. 
    """


class SetPumpName_Responses(NamedTuple):

    PumpNameSet: str
    """
    Pump Name succeeded to Set.
    """


class GetChannelNumber_Responses(NamedTuple):

    ChannelNumber: int
    """
    Number of pump channels 
    """


class SetChannelNumber_Responses(NamedTuple):

    ChannelNumberSet: str
    """
    Channel Number succeeded to Set.
    """


class GetRevolutions_Responses(NamedTuple):

    Revolutions: int
    """
    Get total number of revolutions 
    """


class GetChannelTotalVolume_Responses(NamedTuple):

    ChannelTotalVolume: int
    """
    Channel total volume pumped since last reset, mL 
    """


class GetTotalTime_Responses(NamedTuple):

    TotalTime: int
    """
    Get total time pumped for a channel since last reset, mL.
    """


class GetHeadModel_Responses(NamedTuple):

    HeadModel: int
    """
    Get pump head model type code indicating the ID number of the pump head. The first two digits encode the number of channels on the head, and the second 2 digits represent the number of rollers.
    """


class SetHeadModel_Responses(NamedTuple):

    HeadModelSet: str
    """
    Head Model succeeded to Set.
    """


class SetUserInterface_Responses(NamedTuple):

    UserInterfaceSet: str
    """
    User Interface succeeded to Set.
    """


class SetDisableInterface_Responses(NamedTuple):

    DisableInterfaceSet: str
    """
    Disable Interface succeeded to Set.
    """


class SetDisplayNumbers_Responses(NamedTuple):

    DisplayNumbersSet: str
    """
    Display Numbers succeeded to Set.
    """


class SetDisplayLetters_Responses(NamedTuple):

    DisplayLettersSet: str
    """
    Display Letters succeeded to Set.
    """


class GetTimeSetting_Responses(NamedTuple):

    TimeSetting: float
    """
    Get current setting for pump time in 1/10 seconds.
    """


class SetTimeSetting_Responses(NamedTuple):

    TimeSettingSet: str
    """
    Time Setting succeeded to Set.
    """


class SetRunTimeM_Responses(NamedTuple):

    RunTimeMSet: str
    """
    Run Time succeeded to Set.
    """


class SetRunTimeH_Responses(NamedTuple):

    RunTimeHSet: str
    """
    Run Time succeeded to Set.
    """


class GetRollerStepsLow_Responses(NamedTuple):

    RollerStepsLow: int
    """
    Get current setting for volume to pump in low order roller steps. The total number of roller steps
            which are dispensed during an operation is computed as [ u*65536 + U].
    """


class SetRollerStepsLow_Responses(NamedTuple):

    RollerStepsLowSet: str
    """
    Roller Steps low order succeeded to Set.
    """


class GetRollerStepsHigh_Responses(NamedTuple):

    RollerStepsHigh: int
    """
    Get current setting for volume to pump in high order roller steps. The total number of roller steps
            which are dispensed during an operation is computed as [ u*65536 + U].
    """


class SetRollerStepsHigh_Responses(NamedTuple):

    RollerStepsHighSet: str
    """
    Roller Steps High order succeeded to Set.
    """


class GetRSV_Responses(NamedTuple):

    RSV: float
    """
    Get the pump's current setting for roller step volume (RSV) based on the current calibration,
            tubing diameter and roller count in nL. If no calibration has been performed, the default volume is returned.
            
    """


class SetRSV_Responses(NamedTuple):

    RSVSet: str
    """
    Roller Step Volume succeeded to Set.
    """


class SetRSVReset_Responses(NamedTuple):

    RSVResetSet: str
    """
    Roller Step Volume Reset succeeded.
    """


class ResetRSVTable_Responses(NamedTuple):

    ResetRSVTableSet: str
    """
    Reset Roller Step Volume Table succeeded.
    """


class SetNonFactoryRSV_Responses(NamedTuple):

    SetNonFactoryRSVSet: str
    """
    Non-Factory Roller Step Volume succeeded.
    """


class GetPauseTime_Responses(NamedTuple):

    PauseTime: float
    """
    Get current setting for pause time in 1/10 seconds
    """


class SetPauseTime_Responses(NamedTuple):

    PauseTimeSet: str
    """
    Pause Time succeeded to Set.
    """


class SetPauseTimeM_Responses(NamedTuple):

    PauseTimeMSet: str
    """
    Pause Time succeeded to Set.
    """


class SetPauseTimeH_Responses(NamedTuple):

    PauseTimeHSet: str
    """
    Pause Time succeeded to Set.
    """


class GetTotalVolume_Responses(NamedTuple):

    CurrentTotalVolume: float
    """
    Get total volume dispensed since the last reset in uL, mL or L. 
    """


class SaveSettings_Responses(NamedTuple):

    Save: str
    """
    Saves the current pump settings values to memory.
    """


class SaveSetRoller_Responses(NamedTuple):

    Save: str
    """
    Saves set roller step settings.
    """


class GetFootSwitchStatus_Responses(NamedTuple):

    FootSwitchStatus: str
    """
    Get the current status of the foot switch:   - open  /  + grounded..
    """


class SetRollersNumber_Responses(NamedTuple):

    RollersNumber: str
    """
    Rollers Number succeeded to Set.
    """


class GetRollersNumber_Responses(NamedTuple):

    RollersNumber: int
    """
    Get number of rollers for channel. 
    """


class SetPumpSerialNumber_Responses(NamedTuple):

    PumpSerialNumberSet: str
    """
    Pump Serial Number succeeded to Set.
    """
