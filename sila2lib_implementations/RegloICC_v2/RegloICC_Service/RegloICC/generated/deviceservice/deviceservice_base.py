from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Any, Dict

from sila2.framework import FullyQualifiedIdentifier
from sila2.server import FeatureImplementationBase

from .deviceservice_types import (
    GetVersionSoftware_Responses,
    GetChannelAddressing_Responses,
    GetChannelNumber_Responses,
    GetChannelTotalVolume_Responses,
    GetEventMessages_Responses,
    GetFootSwitchStatus_Responses,
    GetHeadModel_Responses,
    GetLanguage_Responses,
    GetPauseTime_Responses,
    GetPumpStatus_Responses,
    GetRevolutions_Responses,
    GetRollersNumber_Responses,
    GetRollerStepsHigh_Responses,
    GetRollerStepsLow_Responses,
    GetRSV_Responses,
    GetSerialNumber_Responses,
    GetSerialProtocol_Responses,
    GetTimeSetting_Responses,
    GetTotalTime_Responses,
    GetTotalVolume_Responses,
    GetVersionType_Responses,
    ResetRSVTable_Responses,
    SaveSetRoller_Responses,
    SaveSettings_Responses,
    SetChannelAddressing_Responses,
    SetChannelNumber_Responses,
    SetDisableInterface_Responses,
    SetDisplayLetters_Responses,
    SetDisplayNumbers_Responses,
    SetEventMessages_Responses,
    SetHeadModel_Responses,
    SetLanguage_Responses,
    SetNonFactoryRSV_Responses,
    SetPauseTime_Responses,
    SetPauseTimeH_Responses,
    SetPauseTimeM_Responses,
    SetPumpAddress_Responses,
    SetPumpName_Responses,
    SetPumpSerialNumber_Responses,
    SetRollersNumber_Responses,
    SetRollerStepsHigh_Responses,
    SetRollerStepsLow_Responses,
    SetRSV_Responses,
    SetRSVReset_Responses,
    SetRunTimeH_Responses,
    SetRunTimeM_Responses,
    SetTimeSetting_Responses,
    SetUserInterface_Responses,
)


class DeviceServiceBase(FeatureImplementationBase, ABC):

    """

    General device software and hardware information can be retrieved and changed within this function.
    By Valeryia Sidarava, Institute of Biochemical Engineering, Technical University of Munich, 31.07.2019 and
    Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 15.04.2020
    Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 28.01.2022.

    """

    @abstractmethod
    def SetPumpAddress(
        self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetPumpAddress_Responses:
        """

          Set the address of the pump (1-8).



        :param Address:
                Set the address of the pump (1-8).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - PumpAddressSet: Pump Address succeeded to Set.


        """
        pass

    @abstractmethod
    def SetLanguage(
        self, Address: int, Language: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetLanguage_Responses:
        """

          Set the language of the pump.



        :param Address:
                Gets the pump, on which the command will be executed


        :param Language:
                The language of the pump. 0=English / 1=French / 2=Spanish / 3=German.


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - LanguageSet: Language of the pump succeeded to Set.


        """
        pass

    @abstractmethod
    def GetLanguage(self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> GetLanguage_Responses:
        """
        Get The language of the pump. 0=English / 1=French / 2=Spanish / 3=German.


        :param Address:
                The pump address (1-8).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - CurrentLanguage: Current language of the pump. 0=English / 1=French / 2=Spanish / 3=German.


        """
        pass

    @abstractmethod
    def GetPumpStatus(self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> GetPumpStatus_Responses:
        """
        Get pump status. +=running, -=stopped/standby.


        :param Address:
              The pump address (1-8).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - CurrentPumpStatus: Current pump status. +=running, -=stopped/standby.


        """
        pass

    @abstractmethod
    def GetVersionType(
        self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetVersionType_Responses:
        """
        Get pump information. Response is string of model description (variable length), software version (3 digits) and pump head model type code (3 digits).


        :param Address:
                The pump address (1-8).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - CurrentVersionType: Current pump information. Response is string of model description (variable length), software version (3 digits) and pump head model type code (3 digits).


        """
        pass

    @abstractmethod
    def GetVersionSoftware(
        self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetVersionSoftware_Responses:
        """
        Get pump software version. Response is string.


        :param Address:
                The pump address (1-8).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - CurrentVersionSoftware: Current pump software version. Response is string.


        """
        pass

    @abstractmethod
    def GetSerialNumber(
        self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetSerialNumber_Responses:
        """
        Get pump serial number.


        :param Address:
                The pump address (1-8).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - SerialNumber: Pump serial number query.


        """
        pass

    @abstractmethod
    def SetChannelAddressing(
        self, Address: int, ChannelAddressing: bool, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetChannelAddressing_Responses:
        """

          Set whether (1) or not (0) channel addressing is enabled.



        :param Address:
                The pump address (1-8).


        :param ChannelAddressing:
                Set whether (1) or not (0) channel addressing is enabled.


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - ChannelAddressingSet: Channel Addressing of the pump succeeded to Set.


        """
        pass

    @abstractmethod
    def GetChannelAddressing(
        self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetChannelAddressing_Responses:
        """
        Get information whether (1) or not (0) channel addressing is enabled.


        :param Address:
                The pump address (1-8).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - ChannelAddressing: Information whether or not channel addressing is enabled String of form XXXXXXX; 0=disabled,1=enabled.


        """
        pass

    @abstractmethod
    def SetEventMessages(
        self, Address: int, EventMessages: bool, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetEventMessages_Responses:
        """

          Set whether (1) or not (0) event messages are enabled.



        :param Address:
                The pump address (1-8).


        :param EventMessages:
                Set whether (1) or not (0) event messages are enabled.


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - EventMessagesSet: Event Messages of the pump succeeded to Set.


        """
        pass

    @abstractmethod
    def GetEventMessages(
        self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetEventMessages_Responses:
        """
        Get information whether (1) or not (0) event messages are enabled.


        :param Address:
              The pump address (1-8).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - EventMessages: information whether or not event messages are enabled; 0=disabled,1=enabled.


        """
        pass

    @abstractmethod
    def GetSerialProtocol(
        self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetSerialProtocol_Responses:
        """
        Get an integer representing the version of the serial protocol.


        :param Address:
                The pump address(1-8).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - SerialProtocol: Integer representing the version of the serial protocol.


        """
        pass

    @abstractmethod
    def SetPumpName(
        self, Address: int, PumpName: str, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetPumpName_Responses:
        """

              Set pump name for display under remote control string.



        :param Address:
                    The pump address (1-8).


        :param PumpName:
                    Set pump name for display under remote control string.


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - PumpNameSet: Pump Name succeeded to Set.


        """
        pass

    @abstractmethod
    def GetChannelNumber(
        self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetChannelNumber_Responses:
        """
        Get number of pump channels.


        :param Address:
                The pump address (1-8).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - ChannelNumber: Number of pump channels


        """
        pass

    @abstractmethod
    def SetChannelNumber(
        self, Address: int, ChannelNumber: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetChannelNumber_Responses:
        """

        Set number of pump channels.



        :param Address:
              The pump address (1-8).


        :param ChannelNumber:
              Set number of pump channels. Implemented to max 4. If it varies, must change in implementation.


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - ChannelNumberSet: Channel Number succeeded to Set.


        """
        pass

    @abstractmethod
    def GetRevolutions(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetRevolutions_Responses:
        """
        Get total number of revolutions.


        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - Revolutions: Get total number of revolutions


        """
        pass

    @abstractmethod
    def GetChannelTotalVolume(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetChannelTotalVolume_Responses:
        """
        Get channel total volume pumped since last reset, mL.


        :param Channel:
            The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - ChannelTotalVolume: Channel total volume pumped since last reset, mL


        """
        pass

    @abstractmethod
    def GetTotalTime(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> GetTotalTime_Responses:
        """
        Get total time pumped for a channel since last reset, mL.


        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - TotalTime: Get total time pumped for a channel since last reset, mL.


        """
        pass

    @abstractmethod
    def GetHeadModel(self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> GetHeadModel_Responses:
        """
        Get pump head model type code indicating the ID number of the pump head. The first two digits encode the number of channels on the head, and the second 2 digits represent the number of rollers.


        :param Address:
              The pump address (1-8).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - HeadModel: Get pump head model type code indicating the ID number of the pump head. The first two digits encode the number of channels on the head, and the second 2 digits represent the number of rollers.


        """
        pass

    @abstractmethod
    def SetHeadModel(
        self, Address: int, HeadModel: str, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetHeadModel_Responses:
        """

          Set pump head model type code up to 4 digits indicating the ID number of the pump head. The first two digits encode the number of channels on the head, and the second 2 digits represent the number of rollers



        :param Address:
                The pump address (1-8).


        :param HeadModel:
                Set pump head model type code indicating the ID number of the pump head. The first two digits encode the number of channels on the head, and the second 2 digits represent the number of rollers


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - HeadModelSet: Head Model succeeded to Set.


        """
        pass

    @abstractmethod
    def SetUserInterface(
        self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetUserInterface_Responses:
        """

        Set control from the pump user interface



        :param Address:
                The pump address (1-8).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - UserInterfaceSet: User Interface succeeded to Set.


        """
        pass

    @abstractmethod
    def SetDisableInterface(
        self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetDisableInterface_Responses:
        """

          Disable pump user interface



        :param Address:
                The pump address (1-8).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - DisableInterfaceSet: Disable Interface succeeded to Set.


        """
        pass

    @abstractmethod
    def SetDisplayNumbers(
        self, Address: int, DisplayNumbers: str, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetDisplayNumbers_Responses:
        """

        Write numbers to the pump to display while under external control - string (less than 17 characters)



        :param Address:
                The pump address (1-8).


        :param DisplayNumbers:
            Write numbers to the pump to display while under external control - string (less than 17 characters)


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - DisplayNumbersSet: Display Numbers succeeded to Set.


        """
        pass

    @abstractmethod
    def SetDisplayLetters(
        self, Address: int, DisplayLetters: str, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetDisplayLetters_Responses:
        """

          Write letters to the pump to display while under external control - string (less than 17 characters)



        :param Address:
                    The pump address (1-8).


        :param DisplayLetters:
                Write letters to the pump to display while under external control - string (less than 17 characters)


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - DisplayLettersSet: Display Letters succeeded to Set.


        """
        pass

    @abstractmethod
    def GetTimeSetting(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetTimeSetting_Responses:
        """
        Get current setting for pump time in 1/10 seconds.


        :param Channel: The channel (1-4).

        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - TimeSetting: Get current setting for pump time in 1/10 seconds.


        """
        pass

    @abstractmethod
    def SetTimeSetting(
        self, Channel: int, TimeSetting: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetTimeSetting_Responses:
        """

          Set current setting for pump time in 1/10 seconds.



        :param Channel: The channel (1-4).

        :param TimeSetting:
              Set current setting for pump time in 1/10 seconds.


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - TimeSettingSet: Time Setting succeeded to Set.


        """
        pass

    @abstractmethod
    def SetRunTimeM(
        self, Channel: int, RunTimeM: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetRunTimeM_Responses:
        """

          Set current run time setting for dispensing in minutes (0-999)



        :param Channel: The channel (1-4).

        :param RunTimeM:
              Set current run time setting for dispensing in minutes (0-999)


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - RunTimeMSet: Run Time succeeded to Set.


        """
        pass

    @abstractmethod
    def SetRunTimeH(
        self, Channel: int, RunTimeH: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetRunTimeH_Responses:
        """

        Set current run time setting for dispensing in hours (0-999)



        :param Channel: The channel (1-4).

        :param RunTimeH:
              Set current run time setting for dispensing in hours (0-999)


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - RunTimeHSet: Run Time succeeded to Set.


        """
        pass

    @abstractmethod
    def GetRollerStepsLow(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetRollerStepsLow_Responses:
        """
        Get current setting for volume to pump in low order roller steps. The total number of roller steps
        which are dispensed during an operation is computed as [ u*65536 + U].


        :param Channel: The channel (1-4).

        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - RollerStepsLow: Get current setting for volume to pump in low order roller steps. The total number of roller steps
            which are dispensed during an operation is computed as [ u*65536 + U].


        """
        pass

    @abstractmethod
    def SetRollerStepsLow(
        self, Channel: int, RollerStepsLow: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetRollerStepsLow_Responses:
        """

            Set current setting for volume to pump in roller steps (0 - 65536).



        :param Channel: The channel (1-4).

        :param RollerStepsLow:
                Set current setting for volume to pump in roller steps (0 - 65536).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - RollerStepsLowSet: Roller Steps low order succeeded to Set.


        """
        pass

    @abstractmethod
    def GetRollerStepsHigh(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetRollerStepsHigh_Responses:
        """
        Get current setting for volume to pump in high order roller steps. The total number of roller steps
            which are dispensed during an operation is computed as [ u*65536 + U].


        :param Channel: The channel (1-4).

        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - RollerStepsHigh: Get current setting for volume to pump in high order roller steps. The total number of roller steps
            which are dispensed during an operation is computed as [ u*65536 + U].


        """
        pass

    @abstractmethod
    def SetRollerStepsHigh(
        self, Channel: int, RollerSteps: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetRollerStepsHigh_Responses:
        """

        Set current setting for volume to pump in roller steps (greater than 65536).



        :param Channel: The channel (1-4).

        :param RollerSteps:
            Set current setting for volume to pump in roller steps (0 - 65536).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - RollerStepsHighSet: Roller Steps High order succeeded to Set.


        """
        pass

    @abstractmethod
    def GetRSV(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> GetRSV_Responses:
        """
        Get the pump's current setting for roller step volume (RSV) based on the current calibration,
        tubing diameter and roller count in nL. If no calibration has been performed, the default volume is returned


        :param Channel: The channel (1-4).

        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - RSV: Get the pump's current setting for roller step volume (RSV) based on the current calibration,
            tubing diameter and roller count in nL. If no calibration has been performed, the default volume is returned.



        """
        pass

    @abstractmethod
    def SetRSV(self, Channel: int, RSV: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> SetRSV_Responses:
        """

        Set the calibrated roller step volume (RSV) in nL to use for this pump/channel.
        The value is used as calibration value and is overwritten by subsequent calibrations and reset by changing tubing diameter.



        :param Channel: The channel (1-4).

        :param RSV:
            Set the calibrated roller step volume (RSV) in nL to use for this pump/channel.
            The value is used as calibration value and is overwritten by subsequent calibrations and reset by changing tubing diameter.


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - RSVSet: Roller Step Volume succeeded to Set.


        """
        pass

    @abstractmethod
    def SetRSVReset(self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> SetRSVReset_Responses:
        """

        Reset the pump to discard calibration data, use default roller step volume



        :param Address:
                The pump address (1-8).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - RSVResetSet: Roller Step Volume Reset succeeded.


        """
        pass

    @abstractmethod
    def ResetRSVTable(self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> ResetRSVTable_Responses:
        """

        Reset roller step value table to default



        :param Address:
                The pump address (1-8).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - ResetRSVTableSet: Reset Roller Step Volume Table succeeded.


        """
        pass

    @abstractmethod
    def SetNonFactoryRSV(
        self,
        Channel: int,
        RollerCount: int,
        TubingID: str,
        NonFactoryRSV: float,
        *,
        metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetNonFactoryRSV_Responses:
        """

        Change factory roller step volume for a particular roller count and tubing size using roller count (6,8,12);
        index of the tubing diameter; RSV



        :param Channel:
            The channel (1-4).


        :param RollerCount:
            Gets roller count of the channel


        :param TubingID:
            Index of tubing diameter, see manual p.16 of 36)


        :param NonFactoryRSV: Roller step volume in nL
        Change factory roller step volume for a particular roller count and tubing size using roller count (6,8,12);
        index of the tubing diameter; RSVReset roller step value table to default


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - SetNonFactoryRSVSet: Non-Factory Roller Step Volume succeeded.


        """
        pass

    @abstractmethod
    def GetPauseTime(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> GetPauseTime_Responses:
        """
        Get current setting for pause time in 1/10 seconds


        :param Channel:
            The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - PauseTime: Get current setting for pause time in 1/10 seconds


        """
        pass

    @abstractmethod
    def SetPauseTime(
        self, Channel: int, PauseTime: float, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetPauseTime_Responses:
        """

        Set current setting for pause time in 1/10 seconds



        :param Channel:
            The channel (1-4).


        :param PauseTime:
            Set current setting for pause time in 1/10 seconds


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - PauseTimeSet: Pause Time succeeded to Set.


        """
        pass

    @abstractmethod
    def SetPauseTimeM(
        self, Channel: int, PauseTimeM: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetPauseTimeM_Responses:
        """

        Set current setting for pause time in minutes (0-999)



        :param Channel:
            The channel (1-4).


        :param PauseTimeM:
            Set current setting for pause time in minutes (0-999)


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - PauseTimeMSet: Pause Time succeeded to Set.


        """
        pass

    @abstractmethod
    def SetPauseTimeH(
        self, Channel: int, PauseTimeH: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetPauseTimeH_Responses:
        """

        Set current setting for pause time in hours (0-999)



        :param Channel:
            The channel (1-4).


        :param PauseTimeH:
            Set current setting for pause time in hours (0-999)


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - PauseTimeHSet: Pause Time succeeded to Set.


        """
        pass

    @abstractmethod
    def GetTotalVolume(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetTotalVolume_Responses:
        """
        Get total volume dispensed since the last reset in uL, mL or L.


        :param Channel:
            The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - CurrentTotalVolume: Get total volume dispensed since the last reset in uL, mL or L.


        """
        pass

    @abstractmethod
    def SaveSettings(self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> SaveSettings_Responses:
        """
        Saves the current pump settings values to memory.


        :param Address:
                The pump address (1-8).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - Save: Saves the current pump settings values to memory.


        """
        pass

    @abstractmethod
    def SaveSetRoller(self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> SaveSetRoller_Responses:
        """
        Saves set roller step settings.


        :param Address:
                The pump address (1-8).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - Save: Saves set roller step settings.


        """
        pass

    @abstractmethod
    def GetFootSwitchStatus(
        self, Address: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetFootSwitchStatus_Responses:
        """
        Get the current status of the foot switch:   - open  /  + grounded.


        :param Address:
                The pump address (1-8).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - FootSwitchStatus: Get the current status of the foot switch:   - open  /  + grounded..


        """
        pass

    @abstractmethod
    def SetRollersNumber(
        self, Channel: int, RollersNumber: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetRollersNumber_Responses:
        """

        Set number of rollers for channel



        :param Channel:
            The channel (1-4).


        :param RollersNumber:
            Set number of rollers for channel


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - RollersNumber: Rollers Number succeeded to Set.


        """
        pass

    @abstractmethod
    def GetRollersNumber(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetRollersNumber_Responses:
        """
        Get number of rollers for channel


        :param Channel:
            The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - RollersNumber: Get number of rollers for channel.


        """
        pass

    @abstractmethod
    def SetPumpSerialNumber(
        self, Address: int, PumpSerialNumber: str, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetPumpSerialNumber_Responses:
        """

        Set pump serial number string.



        :param Address:
                The pump address (1-8).


        :param PumpSerialNumber:
            Set pump serial number string.


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - PumpSerialNumberSet: Pump Serial Number succeeded to Set.


        """
        pass
