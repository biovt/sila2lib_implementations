from os.path import dirname, join

from sila2.framework import Feature

DriveControllerFeature = Feature(open(join(dirname(__file__), "DriveController.sila.xml")).read())
