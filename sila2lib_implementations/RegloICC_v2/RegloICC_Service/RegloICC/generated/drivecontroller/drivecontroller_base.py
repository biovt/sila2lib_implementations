from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Any, Dict

from sila2.framework import FullyQualifiedIdentifier
from sila2.server import FeatureImplementationBase

from .drivecontroller_types import (
    GetCauseResponse_Responses,
    GetPumpDirection_Responses,
    PausePump_Responses,
    SetDirectionClockwise_Responses,
    SetDirectionCounterClockwise_Responses,
    StartPump_Responses,
    StopPump_Responses,
)


class DriveControllerBase(FeatureImplementationBase, ABC):

    """

            Set and retrieve information regarding the pump drive.
    By Valeryia Sidarava, Institute of Biochemical Engineering, Technical University of Munich, 31.07.2019,
    Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 15.04.2020.
    Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 28.01.2022.

    """

    @abstractmethod
    def StartPump(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> StartPump_Responses:
        """

          Starts the pump out of stand-by mode.



        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - StartStatus: Start of pump succeeded.


        """
        pass

    @abstractmethod
    def StopPump(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> StopPump_Responses:
        """

            Stops the pump.



        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - StopStatus: Shut-down of pump succeeded.


        """
        pass

    @abstractmethod
    def PausePump(self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]) -> PausePump_Responses:
        """

            Pause pumping (STOP in RPM or flow rate mode).



        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - PauseStatus: Pausing of pump succeeded.


        """
        pass

    @abstractmethod
    def GetPumpDirection(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetPumpDirection_Responses:
        """
        Get pump direction (J for CW or K for CCW).


        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - PumpDirection: Pump direction query. J = clockwise/ K = counter-clockwise.


        """
        pass

    @abstractmethod
    def SetDirectionClockwise(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetDirectionClockwise_Responses:
        """
        Set the rotation direction of the pump to clockwise


        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - SetDirectionSet: Set Direction succeeded to Set.


        """
        pass

    @abstractmethod
    def SetDirectionCounterClockwise(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> SetDirectionCounterClockwise_Responses:
        """
        Set the rotation direction of the pump to counter-clockwise


        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - SetDirectionSet: Set Direction succeeded to Set.


        """
        pass

    @abstractmethod
    def GetCauseResponse(
        self, Channel: int, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetCauseResponse_Responses:
        """
        Get cause of "-" cannot run response = Parameter 1 :
                      C=Cycle count of 0 / R=Max flow rate exceeded or flow rate is set to 0 / V=Max volume exceeded;
                      Limiting value that was exceeded = Parameter 2:
                    C=Value is undefined / R=Max flow (mL/min) / V=Max volume (mL)   .


        :param Channel:
                The channel (1-4).


        :param metadata: The SiLA Client Metadata attached to the call

        :return:

            - Cause: Cause displayed.


        """
        pass
