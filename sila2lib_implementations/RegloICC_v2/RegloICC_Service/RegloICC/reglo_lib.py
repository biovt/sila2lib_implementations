# Written by Valeryia Sidarava and Lukas Bromig at TU Munich
# Institute of Biochemical Engineering
# Lukas.bromig@tum.de, valeryia.sidarava@tum.de
#
# Technical Support Ismatec Reglo ICC :
# Tel: 09377920344
# E-Mail: http://www.ismatec.de/download/documents/IDX1798-PD-IS_lr.pdf

import time
from typing import List


class RegloLib:
    """Volume at Rate mode and Time at Rate mode"""

    def __init__(self, client):
        self.client = client

    def pump_set_volume_at_flow_rate(self,
                                     pump_address: int = 1,
                                     flow_rate: List = None,
                                     channels: List = [1, 2],
                                     volume: List = [80, 80],
                                     direction: list = ['K', 'K']
                                     ):
        """
        Method to dispense a set volume (mL, max 2 decimal places, in a list []) with a set flow rate (mL/min, max 2
        decimal places, in a list []), rotation direction (J=clockwise, K=counter-clockwise, in a list []) for specific
        channel or channels.
        Pump address for Reglo ICC is set on 1, yet can be changed with SetPumpAddress function

        If several channels are used, there must be same number of values in each other parameter,
        except for pump_address and return_val.
        """
        start_time0 = time.time()

        # Channel addressing (ability to "speak" to each separate channel) and event messaging (pump reports on what is
        # going on) are being set to 1 (on). If it is already enabled, then the command is skipped

        if self.client.DeviceService.GetChannelAddressing(Address=pump_address).ChannelAddressing is False:
            self.client.DeviceService.SetChannelAddressing(Address=pump_address, ChannelAddressing=True)

        # If flow rate was not defined, max calibrated values are used
        if flow_rate is None:
            flow_rate = []
            for i, channel in enumerate(channels):
                flow_rate.append(
                    self.client.ParameterController.GetMaximumFlowRateWithCalibration(
                        Channel=channel).MaximumFlowRateWithCalibration)  # [:6])
            print('Calibrated maximum flow rate used: ', flow_rate)

        # Checks whether the flow rate is set on RPM or mL/min and sets it to mL/min mode if needed:
        for i, channel in enumerate(channels):
            print(self.client.ParameterController.GetFlowRateAtModes(Channel=channel).CurrentFlowRate)
            if self.client.ParameterController.GetFlowRateAtModes(Channel=channel).CurrentFlowRate is False:
                self.client.ParameterController.SetFlowRateMode(Channel=channel)
                #time.sleep(0.2)
                print(self.client.ParameterController.GetFlowRateAtModes(Channel=channel).CurrentFlowRate)


        # Iteration: for each channel the following commands will be executed
        for i, channel in enumerate(channels):
            # The channel is set to Volume at Rate mode, which allows to dispense a set volume with a set flow rate
            if self.client.ParameterController.GetMode(Channel=channel).CurrentPumpMode != 'O':
                self.client.ParameterController.SetVolumeRateMode(Channel=channel)
            # The volume to dispense is set according to the setting in the head of the function (in mL)
            self.client.ParameterController.SetVolume(Channel=channel, Volume=int(volume[i]))

            # The flow rate is set according to the setting in the head of the function (in mL/min). Must be LESS or
            # EQUAL to the max. flow rate of the channel
            self.client.ParameterController.SetFlowRate(Channel=channel, SetFlowRate=flow_rate[i])

            # Gets the current rotation direction of the channel and compares to the desired setting. If the settings
            # do not match, the sets it to counter-clockwise ...
            if self.client.DriveController.GetPumpDirection(Channel=channel).PumpDirection != direction[i] and \
                    direction[i] == 'K':
                self.client.DriveController.SetDirectionCounterClockwise(Channel=channel)
            # ... or clockwise rotation direction
        
            elif self.client.DriveController.GetPumpDirection(Channel=channel).PumpDirection != direction[i] and \
                    direction[i] == 'J':
                self.client.DriveController.SetDirectionClockwise(Channel=channel)
        
        start_time = time.time()    # notes the time
        for i, channel in enumerate(channels):
            # Starts pumping. The channel will stop as soon as the volume is dispensed
            self.client.DriveController.StartPump(Channel=channel)
            return_val = f'Volume at Rate mode on channel {channel} for volume ' \
                         f'{volume[i]} mL at rate {flow_rate[i]} mL/min set \n'
        return volume

    @staticmethod
    def pump_time_at_flow_rate(self, return_val='', pump=1, flow_rate=None, channels=[1, 2], runtime=['10.5', '5.3'],
                               direction=['K', 'K']):
        """
        Method to dispense for a set time duration (seconds, max 1 decimal place, in a list []) with a set flow rate
        (mL/min, max 2 decimal places, in a list []), rotation direction (J=clockwise, K=counter-clockwise, in a
        list []) for specific channel or channels.
        Pump address for Reglo ICC is set on 1, yet can be changed with SetPumpAddress function

        If several channels are used, there must be same number of values in each other parameter, except for pump and
        return_val.
        """

        # Channel addressing (ability to "speak" to each separate channel) and event messaging (pump reports on what is
        # going on) are being set to 1 (on).
        # If it is already enabled, then the command are skipped
        if self.client.DeviceService.GetChannelAddressing(Address=pump).ChannelAddressing is False:
            self.client.DeviceService.SetChannelAddressing(Address=pump, ChannelAddressing=True)

        # checks, whether flow_rate is set on RPM or mL/min and sets it to mL/min mode if needed:
        for i, channel in enumerate(channels):
            if self.client.ParameterController.GetFlowRateAtModes(Channel=channel).CurrentFlowRate is False:
                self.client.ParameterController.SetFlowRateMode(Channel=channel)

        # If flow rate was not defined, max calibrated values are used
        if flow_rate is None:
            flow_rate = []
            for i, channel in enumerate(channels):
                max_flow_rate = self.client.ParameterController.GetMaximumFlowRateWithCalibration(
                    Channel=channel).MaximumFlowRateWithCalibration
                flow_rate.append(max_flow_rate)
            print('Calibrated maximum flow rate used: ', flow_rate)

        # Iteration: for each channel following commands will be executed
        for i, channel in enumerate(channels):
            # The channel is set to Time mode, with allows to dispense for a set time duration with a set flow rate
            if self.client.ParameterController.GetMode(channel).CurrentPumpMode != 'N':
                self.client.ParameterController.SetTimeMode(str(channel))
            # The time duration to dispense is set according to the setting in the head of the function (in seconds with
            # max. 1 decimal place)
            self.client.ParameterController.SetCurrentRunTime(channel, runtime[i])
            # The flow rate is set according to the setting in the head of the function (in mL/min). Must be LESS or
            # EQUAL to the max. flow rate of the channel
            self.client.ParameterController.SetFlowRate(Channel=channel, SetFlowRate=flow_rate[i])

            # Gets the current rotation direction of the channel and compares to the desired setting. If the settings do
            # not match, the sets it to counter-clockwise ...
            if self.client.DriveController.GetPumpDirection(Channel=channel).PumpDirection != direction[i] and \
                    direction[i] == 'K':
                self.client.DriveController.SetDirectionCounterClockwise(Channel=channel)
            # ... or clockwise rotation direction
            elif self.client.DriveController.GetPumpDirection(Channel=channel).PumpDirection != direction[i] and \
                    direction[i] == 'J':
                self.client.DriveController.SetDirectionClockwise(Channel=channel)

        for i, channel in enumerate(channels):
            # Starts pumping. The channel will stop as soon as the volume is dispensed
            self.client.DriveController.StartPump(Channel=channel)
            return_val = return_val+"Time mode on channel %s for time %s seconds at rate %s mL/min set \n\n" % \
                         (channel, runtime[i], flow_rate[i])
        return_val = return_val
        # returns the text with set parameters
        return return_val

    def pump_stop(self, return_val='', pump=1, channels=[1, 2]):
        """
        Method to stop the channel(s).
        """

        # Channel addressing (ability to "speak" to each separate channel) and event messaging (pump reports on what is
        # going on) are being set to 1 (on).
        # If it is already enabled, then the command are skipped
        if self.client.DeviceService.GetChannelAddressing(Address=pump).ChannelAddressing is False:
            self.client.DeviceService.SetChannelAddressing(Address=pump, ChannelAddressing=True)

        for channel in channels:
            self.client.DriveController.StopPump(Channel=channel)
            return_val = return_val+"Channel %s stopped \n\n" % channel
        # returns the text with set parameters
        return return_val

    def pump_status(self, pump=1):
        """
        Method to check whether the pump is currently running (+) or not (-).

        If one or more channels are in use, the status would be 'running'. However, it is impossible to distinguish,
        which and how many channels are used.
        """

        return_val = self.client.DeviceService.GetPumpStatus(Address=pump).CurrentPumpStatus
        return return_val
