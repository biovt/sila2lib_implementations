"""
________________________________________________________________________

:PROJECT: SiLA2_python

*Drive Control Servicer*

:details: DriveControlServicer:
    Set and retrieve information regarding the pump drive of the Reglo DC (digital control) pump..
    Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 15.03.2021.

:file:    DriveControlServicer_simulation.py
:authors: Lukas Bromig

:date: (creation)          2021-03-16T12:40:00.407176
:date: (last modification) 2021-03-16T12:40:00.407176

.. note:: Code generated by sila2codegenerator 0.3.3-dev

________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "1.0"

# import general packages
import logging
import time         # used for observables
import uuid         # used for observables
import grpc         # used for type hinting only
import inspect      # used for status determination

# import SiLA2 library
import sila2lib.framework.SiLAFramework_pb2 as silaFW_pb2

# import gRPC modules for this feature
from .gRPC import DriveControlServicer_pb2 as DriveControlServicer_pb2
# from .gRPC import DriveControlServicer_pb2_grpc as DriveControlServicer_pb2_grpc

# import default arguments
from .DriveControlServicer_default_arguments import default_dict


# noinspection PyPep8Naming,PyUnusedLocal
class DriveControlServicerSimulation:
    """
    Implementation of the *Drive Control Servicer* in *Simulation* mode
        This is a RegloDC Service
    """

    def __init__(self, properties):
        """Class initialiser"""

        self.properties = properties
        logging.debug('Started server in mode: {mode}'.format(mode='Simulation'))

    def _get_command_state(self, command_uuid: str) -> silaFW_pb2.ExecutionInfo:
        """
        Method to fill an ExecutionInfo message from the SiLA server for observable commands

        :param command_uuid: The uuid of the command for which to return the current state

        :return: An execution info object with the current command state
        """

        #: Enumeration of silaFW_pb2.ExecutionInfo.CommandStatus
        command_status = silaFW_pb2.ExecutionInfo.CommandStatus.waiting
        #: Real silaFW_pb2.Real(0...1)
        command_progress = None
        #: Duration silaFW_pb2.Duration(seconds=<seconds>, nanos=<nanos>)
        command_estimated_remaining = None
        #: Duration silaFW_pb2.Duration(seconds=<seconds>, nanos=<nanos>)
        command_lifetime_of_execution = None

        # TODO: check the state of the command with the given uuid and return the correct information

        # just return a default in this example
        return silaFW_pb2.ExecutionInfo(
            commandStatus=command_status,
            progressInfo=(
                command_progress if command_progress is not None else None
            ),
            estimatedRemainingTime=(
                command_estimated_remaining if command_estimated_remaining is not None else None
            ),
            updatedLifetimeOfExecution=(
                command_lifetime_of_execution if command_lifetime_of_execution is not None else None
            )
        )

    @staticmethod
    def _get_function_name():
        return inspect.stack()[1][3]

    def StartPump(self, request, context: grpc.ServicerContext) \
            -> DriveControlServicer_pb2.StartPump_Responses:
        """
        Executes the unobservable command "Start Pump"
            Starts the pump out of stand-by mode.
    
        :param request: gRPC request containing the parameters passed:
            request.EmptyParameter (Empty Parameter): An empty parameter data type used if no parameter is required.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.StartStatus (Start Status): Start of pump succeeded.
        """
    
        # initialise the return value
        return_value = None
        read = None
        self.properties.status = f'{self._get_function_name()}'
        logging.debug(f'New status is: {self.properties.status}')

        command = f'{self.properties.pump_address_sim}H\r\n'
        try:
            read = '*'
            par_dict = {
                'StartStatus': silaFW_pb2.String(value=read),
            }
            return_value = DriveControlServicer_pb2.StartPump_Responses(**par_dict)
            logging.debug('Executed command StartPump in mode: {mode} with response: {response}'
                          .format(mode='Simulation', response=read))
        except (ValueError, IndexError):
            logging.exception('Parsing of the following command response failed: {command}, {response}'
                              .format(command=command, response=read))
        except ConnectionError:
            logging.exception(f'Communication failed executing the command: {command}')
            return_value = None
    
        # fallback to default
        if return_value is None:
            return_value = DriveControlServicer_pb2.StartPump_Responses(
                **default_dict['StartPump_Responses']
            )
    
        # update the server status
        self.properties.status = f'{self._get_function_name()}'
    
        return return_value

    def StopPump(self, request, context: grpc.ServicerContext) \
            -> DriveControlServicer_pb2.StopPump_Responses:
        """
        Executes the unobservable command "Stop Pump"
            Stops the all channels of the pump.
    
        :param request: gRPC request containing the parameters passed:
            request.EmptyParameter (Empty Parameter): An empty parameter data type used if no parameter is required.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.StopStatus (Stop Status): Shut-down of pump succeeded.
        """
    
        # initialise the return value
        return_value = None
        read = None
        self.properties.status = f'{self._get_function_name()}'
        logging.debug(f'New status is: {self.properties.status}')

        command = f'{self.properties.pump_address_sim}I\r\n'
        try:
            read = '*'
            par_dict = {
                'StopStatus': silaFW_pb2.String(value=read),
            }
            return_value = DriveControlServicer_pb2.StopPump_Responses(**par_dict)
            logging.debug('Executed command StopPump in mode: {mode} with response: {response}'
                          .format(mode='Simulation', response=read))
        except (ValueError, IndexError):
            logging.exception('Parsing of the following command response failed: {command}, {response}'
                              .format(command=command, response=read))
        except ConnectionError:
            logging.exception(f'Communication failed executing the command: {command}')
            return_value = None
    
        # fallback to default
        if return_value is None:
            return_value = DriveControlServicer_pb2.StopPump_Responses(
                **default_dict['StopPump_Responses']
            )
    
        # update the server status
        self.properties.status = f'{self._get_function_name()}'
    
        return return_value

    def GetPumpDirection(self, request, context: grpc.ServicerContext) \
            -> DriveControlServicer_pb2.GetPumpDirection_Responses:
        """
        Executes the unobservable command "Get Pump Direction"
            Get pump direction.
    
        :param request: gRPC request containing the parameters passed:
            request.Channel (Channel):
            Gets the channel, on which the command will be executed
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.PumpDirection (Pump Direction): Pump direction query. J = clockwise/ K = counter-clockwise.
        """
    
        # initialise the return value
        return_value = None
        read = 'Not available'
        self.properties.status = f'{self._get_function_name()}'
        logging.debug(f'New status is: {self.properties.status}')

        par_dict = {
            'PumpDirection': silaFW_pb2.String(value=read),
        }
        return_value = DriveControlServicer_pb2.GetPumpDirection_Responses(**par_dict)
        logging.debug('Executed command GetPumpDirection in mode: {mode} with response: {response}'
                      .format(mode='Simulation', response=read))
    
        # fallback to default
        if return_value is None:
            return_value = DriveControlServicer_pb2.GetPumpDirection_Responses(
                **default_dict['GetPumpDirection_Responses']
            )
    
        # update the server status
        self.properties.status = f'{self._get_function_name()}'
    
        return return_value

    def SetDirectionClockwise(self, request, context: grpc.ServicerContext) \
            -> DriveControlServicer_pb2.SetDirectionClockwise_Responses:
        """
        Executes the unobservable command "Set Direction to clockwise"
            Set the rotation direction of the pump to clockwise
    
        :param request: gRPC request containing the parameters passed:
            request.EmptyParameter (Empty Parameter): An empty parameter data type used if no parameter is required.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.SetDirectionClockwiseSet (Set Direction Clockwise Set): Set direction to clockwise succeeded to set.
        """
    
        # initialise the return value
        return_value = None
        read = None
        self.properties.status = f'{self._get_function_name()}'
        logging.debug(f'New status is: {self.properties.status}')

        command = f'{self.properties.pump_address_sim}J\r\n'
        try:
            read = '*'
            par_dict = {
                'SetDirectionClockwiseSet': silaFW_pb2.String(value=read),
            }
            return_value = DriveControlServicer_pb2.SetDirectionClockwise_Responses(**par_dict)
            logging.debug('Executed command SetDirectionClockwise in mode: {mode} with response: {response}'
                          .format(mode='Simulation', response=read))
        except (ValueError, IndexError):
            logging.exception('Parsing of the following command response failed: {command}, {response}'
                              .format(command=command, response=read))
        except ConnectionError:
            logging.exception(f'Communication failed executing the command: {command}')
            return_value = None
    
        # fallback to default
        if return_value is None:
            return_value = DriveControlServicer_pb2.SetDirectionClockwise_Responses(
                **default_dict['SetDirectionClockwise_Responses']
            )
    
        # update the server status
        self.properties.status = f'{self._get_function_name()}'
    
        return return_value

    def SetDirectionCounterClockwise(self, request, context: grpc.ServicerContext) \
            -> DriveControlServicer_pb2.SetDirectionCounterClockwise_Responses:
        """
        Executes the unobservable command "Set Direction to Counter-Clockwise"
            Set the rotation direction of the pump to counter-clockwise
    
        :param request: gRPC request containing the parameters passed:
            request.EmptyParameter (Empty Parameter): An empty parameter data type used if no parameter is required.
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.SetDirectionCounterClockwiseSet (Set Direction to Counter-Clockwise Set): Set direction to
            counter-clockwise succeeded to set.
        """
    
        # initialise the return value
        return_value = None
        read = None
        self.properties.status = f'{self._get_function_name()}'
        logging.debug(f'New status is: {self.properties.status}')

        command = f'{self.properties.pump_address_sim}K\r\n'
        try:
            read = '*'
            par_dict = {
                'SetDirectionCounterClockwiseSet': silaFW_pb2.String(value=read),
            }
            return_value = DriveControlServicer_pb2.SetDirectionCounterClockwise_Responses(**par_dict)
            logging.debug('Executed command SetDirectionCounterClockwise in mode: {mode} with response: {response}'
                          .format(mode='Simulation', response=read))
        except (ValueError, IndexError):
            logging.exception('Parsing of the following command response failed: {command}, {response}'
                              .format(command=command, response=read))
        except ConnectionError:
            logging.exception(f'Communication failed executing the command: {command}')
            return_value = None
    
        # fallback to default
        if return_value is None:
            return_value = DriveControlServicer_pb2.SetDirectionCounterClockwise_Responses(
                **default_dict['SetDirectionCounterClockwise_Responses']
            )
    
        # update the server status
        self.properties.status = f'{self._get_function_name()}'
    
        return return_value
