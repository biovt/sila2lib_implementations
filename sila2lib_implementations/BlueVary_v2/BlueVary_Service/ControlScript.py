import os
import time

from BlueVary_Service.feature_implementations import BlueVary_Library as lib


SIMULATION_MODE = False


if SIMULATION_MODE:
    print('WARNING: Running in simulation mode!')
start_time = time.localtime(time.time())
t_0 = time.time()
dir_path = os.path.dirname(os.path.realpath(__file__))
lib.timestamp()

units = lib.input_units()
calibration = lib.input_calibration()
interval = lib.input_interval()
file_title = lib.create_csv(units)

# Start servers
server_list = lib.start_servers(units, simulation=SIMULATION_MODE)
time.sleep(7)
inactive_list = lib.get_server_status(server_list)

print(f'Servers on server_list: {server_list}')
print(f'Servers on inactive_list: {inactive_list}')
print(f'Servers on units: {units}')

# def del element:
# TODO: Implement del function
for i in inactive_list:
    del(units[units.index(i)])
print(units)

# def restart(inactive_list, units):
# TODO: ask indefinitely whether servers should be restarted
# keep returning the inactive_list
# TODO: Invoke del function and delete if user chooses to continue

# TODO: Delete inactive servers from server list or/AND implement try:/except: statements in GetResults!

client_list = lib.load_clients(units)
time.sleep(5)

try:
    # TODO: Implement a request to the respective servers,
    # whether they are all warmed up and ready to use. Only proceed if ready!
    # Validate implementation
    if calibration == True:

        lib.calibrate(client_list)
    else:
        pass

    while True:
        data = lib.get_data(client_list, t_0)
        lib.append_data_csv(file_title=file_title, data=data)
        print('Measuring every %s seconds. Current file size:%s KB' % (interval, lib.get_file_size(file_title)/1024))
        print("Time:%.4s" % (time.time()-t_0))
        time.sleep(interval - (time.time() % interval))
except:
    print("Exception occurred")
    lib.kill_servers(server_list)
finally:
    lib.kill_servers(server_list)









