from __future__ import annotations


from sila2.client import SilaClient


    
from .sensorservicer import SensorServicerFeature
from .deviceservicer import DeviceServicerFeature
from .calibrationservicer import CalibrationServicerFeature
        
from .sensorservicer import DeviceNotReadyError
from .deviceservicer import DeviceNotReadyError
from .calibrationservicer import DeviceNotReadyError
        
from .sensorservicer import SerialConnectionError
from .deviceservicer import SerialConnectionError
from .calibrationservicer import SerialConnectionError
        
    



from typing import TYPE_CHECKING
if TYPE_CHECKING:
    
    from .sensorservicer import SensorServicerClient
    from .deviceservicer import DeviceServicerClient
    from .calibrationservicer import CalibrationServicerClient
    

class Client(SilaClient):

    DeviceServicer: DeviceServicerClient
    SensorServicer: SensorServicerClient
    CalibrationServicer: CalibrationServicerClient

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._register_defined_execution_error_class(
            DeviceServicerFeature.defined_execution_errors["DeviceNotReadyError"], DeviceNotReadyError
        )
        self._register_defined_execution_error_class(
            SensorServicerFeature.defined_execution_errors["DeviceNotReadyError"], DeviceNotReadyError
        )
        self._register_defined_execution_error_class(
            CalibrationServicerFeature.defined_execution_errors["DeviceNotReadyError"], DeviceNotReadyError
        )

        self._register_defined_execution_error_class(
            DeviceServicerFeature.defined_execution_errors["SerialConnectionError"], SerialConnectionError
        )
        self._register_defined_execution_error_class(
            SensorServicerFeature.defined_execution_errors["SerialConnectionError"], SerialConnectionError
        )
        self._register_defined_execution_error_class(
            CalibrationServicerFeature.defined_execution_errors["SerialConnectionError"], SerialConnectionError
        )
