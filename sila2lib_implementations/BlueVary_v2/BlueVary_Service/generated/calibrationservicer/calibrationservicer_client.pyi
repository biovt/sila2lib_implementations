from __future__ import annotations

from typing import Any, Dict, Optional, Union
from sila2.client import ClientMetadata

from .calibrationservicer_types import StartCalibration_Responses


class CalibrationServicerClient:
    """
		Used to initiate device 1-pt calibration of the off-gas sensors.
        By Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 21.12.2021
    """

    def StartCalibration(
            self,
            *,
            metadata: Optional[Dict[Union[ClientMetadata, str], Any]] = None
    ) -> StartCalibration_Responses:
        """
        Start the 1-pt calibration of the sensors
        """
        ...
