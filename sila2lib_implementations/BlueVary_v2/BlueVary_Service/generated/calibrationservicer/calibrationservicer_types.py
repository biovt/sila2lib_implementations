from __future__ import annotations

from typing import NamedTuple


class StartCalibration_Responses(NamedTuple):
    CalibrationStatus: str
    """
    Calibration command received. Calibration status response.
    """
