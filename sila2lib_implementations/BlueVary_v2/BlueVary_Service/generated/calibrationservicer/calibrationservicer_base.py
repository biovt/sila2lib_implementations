from __future__ import annotations

from abc import ABC
from sila2.server import FeatureImplementationBase

from abc import abstractmethod

from typing import Any, Dict
from sila2.framework import FullyQualifiedIdentifier

from .calibrationservicer_types import StartCalibration_Responses


class CalibrationServicerBase(FeatureImplementationBase, ABC):
    """
	Used to initiate device 1-pt calibration of the off-gas sensors.
    By Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 21.12.2021
    """

    @abstractmethod
    def StartCalibration(
        self,
        *,
        metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> StartCalibration_Responses:
        """
        Start the 1-pt calibration of the sensors
        :param metadata: The SiLA Client Metadata attached to the call
        :return:
            - CalibrationStatus: Calibration command received. Calibration status response.
        """
        pass
