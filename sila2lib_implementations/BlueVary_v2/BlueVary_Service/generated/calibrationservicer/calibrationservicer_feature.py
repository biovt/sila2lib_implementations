from os.path import dirname, join

from sila2.framework import Feature

CalibrationServicerFeature = Feature(open(join(dirname(__file__), "CalibrationServicer.sila.xml")).read())
