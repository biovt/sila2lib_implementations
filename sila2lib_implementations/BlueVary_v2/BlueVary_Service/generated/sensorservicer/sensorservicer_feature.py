from os.path import dirname, join

from sila2.framework import Feature

SensorServicerFeature = Feature(open(join(dirname(__file__), "SensorServicer.sila.xml")).read())
