from __future__ import annotations

from typing import NamedTuple


class GetResults_Responses(NamedTuple):
    CO2: float
    """
    Get the current CO2 concentration
    """
    
    O2: float
    """
    Get the current O2 concentration
    """
    
    Pressure: float
    """
    Get the current air pressure reading
    """
    

class GetHumidity_Responses(NamedTuple):
    Humidity: float
    """
    Get the current humidity in [%]
    """
    
    Temperature: float
    """
    Get the current temperature in [C]
    """
    
    AbsoluteHumidity: float
    """
    Get the current absolute humidity in [vol.-%]
    """
