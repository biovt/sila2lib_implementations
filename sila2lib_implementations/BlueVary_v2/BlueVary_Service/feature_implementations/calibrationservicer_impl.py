from __future__ import annotations
import logging
from typing import Any, Dict
from time import sleep

from sila2.framework import FullyQualifiedIdentifier

from .serial_connector import SharedProperties
from ..generated.calibrationservicer import StartCalibration_Responses
from ..generated.calibrationservicer import CalibrationServicerBase
from ..generated.calibrationservicer import DeviceNotReadyError, SerialConnectionError


class CalibrationServicerImpl(CalibrationServicerBase):
    def __init__(self, properties: SharedProperties):
        self.properties = properties

    def StartCalibration(
            self,
            *,
            metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> StartCalibration_Responses:
        logging.debug(
            "StartCalibration called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        heat_up_response = 'Sensor 1: Sensor 2, wait while Sensor heats up :E,9E'
        try:
            with self.properties.acquire_timeout_lock(timeout=1):
                if self.properties.simulation_mode:
                    sleep(0.1)  # Delay simulating processing speed of device
                    # Expected positive response: 'OK'
                    # Expected negative response: heat_up_response
                    return StartCalibration_Responses(
                        CalibrationStatus='OK (Sim)'
                    )
                else:
                    try:
                        self.properties.serial_connector.ser.write(str.encode('&k\r\n'))
                        read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                        if read == heat_up_response:
                            raise DeviceNotReadyError
                        elif len(read) == 52:
                            raise DeviceNotReadyError
                        elif len(read) == 2:
                            try:
                                CalibrationStatus = str(read)
                                return StartCalibration_Responses(
                                    CalibrationStatus=CalibrationStatus
                                )
                            except Exception:
                                logging.error('Faulty serial read out : {read}'.format(read=read))
                                raise SerialConnectionError
                        else:
                            logging.error('Unexpected serial read out : {read}'.format(read=read))
                            raise SerialConnectionError
                    except DeviceNotReadyError:
                            raise DeviceNotReadyError
                    except SerialConnectionError:
                        raise SerialConnectionError
                    finally:
                        sleep(0.5)
        except DeviceNotReadyError as e:
            logging.error(e)
        except Exception as e:
            logging.error('Lock may be unavailable!?')
            logging.error(e)
            