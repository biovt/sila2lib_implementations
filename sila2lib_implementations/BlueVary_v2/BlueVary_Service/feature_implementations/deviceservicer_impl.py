from __future__ import annotations
import logging

from typing import Any, Dict, Union
from sila2.framework import CommandExecutionInfo, CommandExecutionStatus, FullyQualifiedIdentifier

from concurrent.futures import Executor
from threading import Event
from queue import Queue
from time import sleep
from random import choice

from .serial_connector import SharedProperties
from ..generated.deviceservicer import GetSensorID_Responses
from ..generated.deviceservicer import GetSensorInfo_Responses
from ..generated.deviceservicer import DeviceServicerBase
from ..generated.deviceservicer import DeviceNotReadyError, SerialConnectionError


class DeviceServicerImpl(DeviceServicerBase):
    def __init__(self, properties: SharedProperties, executor: Executor):
        super().__init__()
        self.__stop_event = Event()
        self.properties: SharedProperties = properties

        def update_status(stop_event: Event):
            while not stop_event.is_set():
                status_response = 'Sensor 1: Sensor 2, wait while Sensor heats up :E,9E'
                with self.properties.acquire_timeout_lock(timeout=3):
                    if self.properties.simulation_mode:
                        sleep(0.1)  # Delay simulating processing speed of device
                        possible_responses = ['ready', 'not ready']
                        CurrentStatus = choice(possible_responses)
                    else:
                        try:
                            self.properties.serial_connector.ser.write(str.encode('&e\r\n'))
                            read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                            if read == status_response:
                                CurrentStatus = 'not ready'
                            else:
                                CurrentStatus = 'ready'
                        except Exception as e:
                            logging.error(e)
                            raise SerialConnectionError
                        finally:
                            sleep(0.2)
                self.update_CurrentStatus(CurrentStatus=CurrentStatus)
                sleep(5)

        executor.submit(update_status, self.__stop_event)

    def update_CurrentStatus(self, CurrentStatus: str):
        self._CurrentStatus_producer_queue.put(CurrentStatus)

    def GetSensorID(
            self,
            *,
            metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetSensorID_Responses:
        logging.debug(
            "GetSensorID called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        try:
            with self.properties.acquire_timeout_lock(timeout=1):
                if self.properties.simulation_mode:
                    sleep(0.1)  # Delay simulating processing speed of device
                    return GetSensorID_Responses(
                        UnitID=1234,
                        CO2ID=1234,
                        O2ID=1234,
                        HUMID=1234,
                        PRESID=1234
                    )
                else:
                    try:
                        self.properties.serial_connector.ser.write(str.encode('&i\r\n'))
                        read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                        try:
                            UnitID = int(read[:3].split(' ')[0])
                            if " CO2_" in read:
                                CO2ID = int(read[read.find(" CO2_") + 5:read.find("CO2_") + 9])
                            else:
                                raise ValueError('Parsing of serial read out unsuccessful (CO2): {read}'.format(read=read))

                            if " O2_" in read:
                                O2ID = int(read[read.find(" O2_") + 4:read.find(" O2_") + 9])
                            else:
                                raise ValueError('Parsing of serial read out unsuccessful (O2): {read}'.format(read=read))

                            if " HUM_" in read:
                                HUMID = int(read[read.find(" HUM_") + 5:read.find(" HUM_") + 10])
                            else:
                                raise ValueError('Parsing of serial read out unsuccessful (HUM): {read}'.format(read=read))

                            return GetSensorID_Responses(
                                    UnitID=UnitID,
                                    CO2ID=CO2ID,
                                    O2ID=O2ID,
                                    HUMID=HUMID,
                                    PRESID=0
                                )

                        except ValueError as e:
                            logging.error(e)
                    except Exception as e:
                        logging.error(e)
                        raise SerialConnectionError
                    finally:
                        sleep(0.2)
        except Exception as e:
            logging.error(e)
            raise SerialConnectionError

    def GetSensorInfo(
            self,
            *,
            metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> GetSensorInfo_Responses:
        logging.debug(
            "GetSensorInfo called in {current_mode} mode".format(
                current_mode=('simulation' if self.properties.simulation_mode else 'real')
            )
        )
        try:
            with self.properties.acquire_timeout_lock(timeout=1):
                if self.properties.simulation_mode:
                    sleep(0.1)  # Delay simulating processing speed of device
                    return GetSensorInfo_Responses(SensorInfo='A string containing sensor info')
                else:
                    try:
                        self.properties.serial_connector.ser.write(str.encode('&s\r\n'))
                        return GetSensorInfo_Responses(
                            SensorInfo=str(bytes.decode(self.properties.serial_connector.ser.read_until('\r\n').rstrip()))
                        )
                    except Exception as e:
                        logging.error(e)
                        raise SerialConnectionError
                    finally:
                        sleep(0.2)
        except Exception as e:
            logging.error(e)
            raise SerialConnectionError

    def stop(self):
        self.__stop_event.set()
