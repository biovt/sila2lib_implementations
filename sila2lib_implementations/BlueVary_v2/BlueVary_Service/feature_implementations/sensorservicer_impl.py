from __future__ import annotations
import logging
from typing import Any, Dict
from time import sleep

from sila2.framework import FullyQualifiedIdentifier

from .serial_connector import SharedProperties
from ..generated.sensorservicer import GetResults_Responses
from ..generated.sensorservicer import GetHumidity_Responses
from ..generated.sensorservicer import SensorServicerBase
from ..generated.sensorservicer import DeviceNotReadyError, SerialConnectionError


class SensorServicerImpl(SensorServicerBase):
    def __init__(self, properties: SharedProperties):
        self.properties = properties

    def GetResults(
                self,
                *,
                metadata: Dict[FullyQualifiedIdentifier, Any]
        ) -> GetResults_Responses:
            logging.debug(
                "GetResults called in {current_mode} mode".format(
                    current_mode=('simulation' if self.properties.simulation_mode else 'real')
                )
            )
            # heat_up_response = 'Sensor 1: Sensor 2, wait while Sensor heats up :E,9E'
            try:
                with self.properties.acquire_timeout_lock(timeout=1):
                    if self.properties.simulation_mode:
                        sleep(0.1)  # Delay simulating processing speed of device
                        return GetResults_Responses(
                            CO2=0.00,
                            O2=0.00,
                            Pressure=0.00
                        )
                    else:
                        try:
                            self.properties.serial_connector.ser.write(str.encode('&e\r\n'))
                            read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                            # Potential read '-9.297804354E-05 -2.096446037E+01 -9.579418302E-01 :E,3D'
                            if 'wait while Sensor heats up' in read:
                                raise DeviceNotReadyError
                            elif len(read) >= 53 and len(read) <= 56:
                                if '-' in read:
                                    logging.warning('Negative values detected! Unless you\'re working in a negative '
                                                    'pressure environment, this cannot be the case. You may have to '
                                                    'recalibrate!')
                                try:
                                    results = read.split(' ')
                                    co2 = float(results[0])
                                    o2 = float(results[1])
                                    pressure = float(results[2])
                                    return GetResults_Responses(
                                        CO2=co2,
                                        O2=o2,
                                        Pressure=pressure
                                    )
                                except Exception:
                                    logging.error('Faulty serial read out : {read}'.format(read=read))
                                    raise SerialConnectionError
                            else:
                                logging.error('Unexpected serial read out : {read}'.format(read=read))
                                raise SerialConnectionError
                        except DeviceNotReadyError:
                                raise DeviceNotReadyError
                        except SerialConnectionError:
                            raise SerialConnectionError
                sleep(0.2)
            except Exception as e:
                logging.error(e)
                logging.error('Lock may be unavailable!?')

    def GetHumidity(
                self,
                *,
                metadata: Dict[FullyQualifiedIdentifier, Any]
        ) -> GetHumidity_Responses:
            logging.debug(
                "GetHumidity called in {current_mode} mode".format(
                    current_mode=('simulation' if self.properties.simulation_mode else 'real')
                )
            )
            heat_up_response = 'Sensor 1: Sensor 2, wait while Sensor heats up :E,9E'
            try:
                with self.properties.acquire_timeout_lock(timeout=1):
                    if self.properties.simulation_mode:
                        sleep(0.1)  # Delay simulating processing speed of device
                        # Potential response
                        # 3.875056282E-02 2.101725388E+01 9.687846303E-01 :E,0A
                        return GetHumidity_Responses(
                            Humidity=0.00,
                            Temperature=0.00,
                            AbsoluteHumidity=0.00
                        )
                    else:
                        try:
                            self.properties.serial_connector.ser.write(str.encode('&e\r\n'))
                            read = str(bytes.decode(self.properties.serial_connector.ser.readline().rstrip()))
                            if 'wait while Sensor heats up' in read:
                                print('DeviceNotReadyError')
                            elif len(read) >= 53 and len(read) <= 56:
                                # Potential read: 3.875056282E-02 2.101725388E+01 9.687846303E-01 :E,0A
                                logging.warning('Negative values detected! Unless you\'re working in a freezing cold lab '
                                                'environment, this cannot be the case. You may have to recalibrate!')
                                try:
                                    results = read.split(' ')
                                    Humidity = float(results[0])
                                    Temperature = float(results[1])
                                    AbsoluteHumidity = float(results[2])
                                    return GetHumidity_Responses(
                                        Humidity=Humidity,
                                        Temperature=Temperature,
                                        AbsoluteHumidity=AbsoluteHumidity
                                    )
                                except Exception:
                                    logging.error('Faulty serial read out : {read}'.format(read=read))
                                    raise SerialConnectionError
                            else:
                                logging.error('Unexpected serial read out : {read}'.format(read=read))
                                raise SerialConnectionError
                        except DeviceNotReadyError:
                                raise DeviceNotReadyError
                        except SerialConnectionError:
                            raise SerialConnectionError
                sleep(0.2)
            except Exception as e:
                logging.error(e)
                logging.error('Lock may be unavailable!?')

