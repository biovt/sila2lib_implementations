import os
import csv
import time
import datetime
import subprocess

if os.name == "nt":  # only available on windows
    from subprocess import CREATE_NEW_CONSOLE
else:
    pass
# from . import BlueVaryService_client as Client
from BlueVary_Service import Client


def timestamp():
    t = time.localtime()
    time_stamp = f'{t.tm_year}_{t.tm_mon}_{t.tm_mday}-{t.tm_hour}_{t.tm_min}_{t.tm_sec}'
    print(time_stamp)
    return time_stamp


no_units = 4  # number of total implemented units

dict_address_mapping = {
    '1': ("127.0.0.1", 50001, '85 CO2_30753 O2_30700 HUM_30279 :I,BD'),
    '2': ("127.0.0.1", 50002, '86 CO2_30752 O2_30703 HUM_30590 :I,BC'),
    '3': ("127.0.0.1", 50003, '87 CO2_30754 O2_30702 HUM_30591 :I,BF'),
    '4': ("127.0.0.1", 50004, '88 CO2_30773 O2_30701 HUM_30587 :I,C5')
}


def input_units():
    units = list()
    while True:
        try:
            user_input = \
                input(f'Add reactor positions. Enter the reactor position/s used [1-{no_units}], exit with [c]: ')
            if "," in user_input:
                entries = user_input.split(",")
                for entry in entries:
                    if int(entry) in range(1, (no_units+1), 1):
                        units.append(int(entry))
                    else:
                        print("Enter integer numbers!")
                units.sort()    
                print("The following reactors position/s will be observed: ", units)
                
                add_another = input("Do you want to add another reactor position? [y,n]")
                if add_another == "y":
                    pass
                elif add_another == "n":
                    print("The following reactor position/s will be observed: ", units)
                    break
                else:
                    pass
            elif len(user_input) <= 1 and user_input in ["1", "2", "3", "4"]:
                units.append(int(user_input))
                units.sort() 
                print("The following reactor position/s will be observed: ", units)
                add_another = input("Do you want to add another reactor position? [y,n]")
                if add_another == "y":
                    pass
                elif add_another == "n":
                    print("The following reactor position/s will be observed: ", units)
                    break
                else:
                    pass
            elif (user_input == "c") == True:
                break
            else:
                print(
                    f'Enter reactor positions as integers [1-{no_units}]! Use a comma to separate reactors positions, '
                    f'if multiple reactors are used!')
                pass
        except Exception:
            print("Exception")
            print(f'Enter reactors as integers [1-{no_units}]! Use a comma to separate reactors, '
                  f'if multiple reactors are used!')
    print("Eliminating duplicates...")
    units = list(dict.fromkeys(units))
    print(units)
    return units


def input_interval():
    while True:
        try:
            user_input = input("Define a measurement interval [in seconds]. Max interval is 3600:")
            try:
                user_input = int(user_input)
                if user_input in range(1, 3601, 1):
                    interval = user_input
                    print("Measurements are taken every %s seconds" % interval)
                    break
                else:
                    pass
            except Exception:
                if user_input == '':
                    tmp = input("Would you like to use the default 60s Dominik? [y/enter,n]")
                    if tmp in ["y", ""]:
                        interval = 60
                        break
                    else:
                        pass
                else:
                    print("Enter an integer number in the range of [1,3600]. @Dominik: default is 60!")
        except:
            print("Wrong format. Enter an integer!")
    return interval


def input_calibration():
    while True:
        calibration = input("Do you want to calibrate the system? [y/n]:")
        if calibration == "y":
            calibration = True
            print("System is being calibrated")
            break
        elif calibration == "n":
            calibration = False
            print("No calibration")
            break
        else:
            print("Incorrect input. Retry!")
    return calibration


def calibrate(units):
    """
    Initiates the calibration sequence of the BlueVary device.
    """
    for i, unit in enumerate(units):
        unit.CalibrationServicer.StartCalibration()
    time.sleep(10)
    print("Systeme kalibriert")


def start_servers(units, simulation: bool = False):
    servers = list()
    for i, unit in enumerate(units):
        ip = dict_address_mapping[str(unit)][0]
        port = dict_address_mapping[str(unit)][1]
        if simulation:
            identifier = ''
        else:
            identifier = dict_address_mapping[str(unit)][2]
        print(f'Starting server for unit {unit} ({i}) with ID {identifier} on {ip}:{port}')
        try:
            print(f'Operating system is: {os.name}')
            if os.name == "nt":
                # $python -m BlueVary_Service -a "10.152.248.209" -p 50052 -u "85 CO2_30753 O2_30700 HUM_30279 :I,BD"
                if simulation:
                    vars()[f'SubprocessServer{unit}'] = subprocess.Popen(
                        ['python', '-m', 'BlueVary_Service', '-a', f'{ip}', '-p', f'{port}',
                         "-u", f'\"{identifier}\"', "-s"],
                        creationflags=CREATE_NEW_CONSOLE)
                else:
                    vars()[f'SubprocessServer{unit}'] = subprocess.Popen(
                        ['python', '-m', 'BlueVary_Service', '-a', f'{ip}', '-p', f'{port}',
                         "-u", f'\"{identifier}\"'],
                        creationflags=CREATE_NEW_CONSOLE)  
            elif os.name == "posix":
                # $python3 -m BlueVary_Service -a "10.152.248.209" -p 50052 -u "85 CO2_30753 O2_30700 HUM_30279 :I,BD" 
                if simulation:
                    vars()[f'SubprocessServer{unit}'] = subprocess.Popen(
                        ['python3', '-m', 'BlueVary_Service', '-a', f'{ip}', '-p', f'{port}',
                         "-u", f'{identifier}', "-s"])
                else:
                    vars()[f'SubprocessServer{unit}'] = subprocess.Popen(
                    ['python3', '-m', 'BlueVary_Service', '-a', f'{ip}', '-p', f'{port}',
                     "-u", f'{identifier}'])
            else:
                print(f'OS not supported: {os.name}')
            print(f'Server {unit} started in SubprocessServer{unit}')
        except:
            print(f'Error starting server {unit}')
        finally:
            pass
            # print("finally")
        servers.append(vars()[f'SubprocessServer{unit}'])
        time.sleep(5)
    print('Started servers:', servers)
    return servers


def get_server_status(servers):
    status_list = list()
    inactive_list = list()
    for i, server in enumerate(servers):
        status = server.poll()  # None means active, 1 means dead
        status_list.append(status)
        print(f'Server {i+1}: {"OK" if status == None else "Not running!"}')

    if status_list.count(None) == len(status_list):
        pass
    else:
        inactive_list = [i for i in range(0, len(status_list)) if status_list[i] == 1]
        for i, unit in enumerate(inactive_list):
            inactive_list[i] = int(unit)+1
        print("Error: The following servers of reactor positions are offline/ Can't be started. "
              "Check the serial connection!")
        print(inactive_list)
        for i, unit in enumerate(inactive_list):
            print(inactive_list[i])
            print(f'{servers[i-1]} DEAD')
        if len(inactive_list) == 0:
            print("All servers offline")
            pass
        while True:
            tmp = input("Continue without, restart dead server and continue, abort? [y,r,n]")
            if tmp == "y":
                break
            elif tmp == "r":
                print(f'Restarting Server/s {inactive_list}...')
                start_servers(inactive_list)
                time.sleep(7)
                break
            elif tmp == "n":
                print("Abort")
                kill_servers(servers)
                exit()
            else:
                print("Incorrect input. Retry!")
    return inactive_list


def kill_servers(servers):
    print("Shutting down servers")
    for i, server in enumerate(servers):
        subprocess.Popen.kill(server)


def load_clients(units):
    clients = list()
    for i, unit in enumerate(units):

        ip = dict_address_mapping[str(unit)][0]
        port = dict_address_mapping[str(unit)][1]
        print(f'Loading client for unit {i+1} on {ip}:{port}')
        vars()[f'Client{unit}'] = Client(hostname=ip, port=port)
        clients.append(vars()[f'Client{unit}'])
    return clients


def get_data(client_list, t_0):
    """
    A function that sends the respective data pull request via the SiLA2 clients to the
    SiLA2 servers of the respective modules.
    """
    # TODO:Rearrange the output file to reduce the number of loops.
    # TODO:Not fail-safe yet. Make sure that this function still works, once a module is excluded.
    #  The data must still be recorded if one unit fails.
    data = list()
    start_time = str(datetime.datetime.now())
    time_stamp = time.time()
    curr_time = (time_stamp - t_0)/(60*60)

    data.append(start_time)
    data.append(time_stamp)
    data.append(curr_time)
    print('Current Time \t| Current Epoch Time [s] \t| Time Since Start [h]')
    print(data)
    i = None
    for i, client in enumerate(client_list):
        data.append(client.SensorServicer.GetResults().CO2)
    print('CO2: ', data[-i-1:])
    for i, client in enumerate(client_list):
        data.append(client.SensorServicer.GetResults().O2)
    print('O2: ', data[-i-1:])
    for i, client in enumerate(client_list):
        data.append(client.SensorServicer.GetResults().Pressure)
    print('P: ', data[-i-1:])
    for i, client in enumerate(client_list):
        data.append(client.SensorServicer.GetHumidity().Humidity)
    print('Hum.: ', data[-i-1:])
    for i, client in enumerate(client_list):
        data.append(client.SensorServicer.GetHumidity().Temperature)
    print('Temp.: ', data[-i-1:])
    for i, client in enumerate(client_list):
        data.append(client.SensorServicer.GetHumidity().AbsoluteHumidity)
    print('Abs. Hum.: ', data[-i-1:])
    return data


# TODO: get away from csv and us a DB. real-time plotting of the data should be possible.
# Constantly reading csv files is heavy on the processor and not very elegant
def create_csv(units):
    """
    This function generates the .csv file in which the BlueVary data will be stored.
    The function "append_data_csv" fills this file with information.
    """
    file_title = f'data/{timestamp()}.csv'

    first_row = ['Time', 'Epoch time [s]', 'Time [h]']
    reactors = list()
    for i, unit in enumerate(units):
        print(i+1)
        reactors.append("R{}".format(unit))
    # Reactors = [ 'R1', 'R2', 'R3', 'R4']
    data_types = ['CO2 [%]', 'O2 [%]', 'Pressure [bar]', 'Humidity [%]', 'abs. Humidity [vol.-%]', 'Temperature [°C]']

    # Create a list with the stuff to write in the first row
    for d_types in data_types:
        for r in reactors:
            first_row.append(r + ' ' + d_types)

    # Make sure the directory exists
    if not os.path.isdir('data'):
        os.mkdir('data')
    # Actual writing of stuff
    with open(file_title, mode='w', newline='') as output_file:
        output_writer = csv.writer(output_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        output_writer.writerow(first_row)
    print(f'File created in: {os.path.dirname(os.path.realpath(__file__))}\\data\\{file_title}')
    return file_title


def append_data_csv(file_title, data):
    """
    Appends the most current data from "get_data" to the .csv log-file
    """
    with open(file_title, mode='a', newline='') as output_file:
        output_writer = csv.writer(output_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        output_writer.writerow(data)


def get_file_size(file_title):
    """
    Calculates the file size of the log-file in bytes.
    """
    return os.stat(file_title).st_size
