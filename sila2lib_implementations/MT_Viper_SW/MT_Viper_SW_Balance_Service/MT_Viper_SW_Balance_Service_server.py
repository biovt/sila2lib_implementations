#!/usr/bin/env python3
"""
________________________________________________________________________

:PROJECT: SiLA2_python

*MT_Viper_SW_Balance_Service*

:details: MT_Viper_SW_Balance_Service:
    This is a Mettler Toledo Viper SW balance service
           
:file:    MT_Viper_SW_Balance_Service_server.py
:authors: Lukas Bromig

:date: (creation)          2021-04-09T13:29:06.819405
:date: (last modification) 2021-04-09T13:29:06.819405

.. note:: Code generated by sila2codegenerator 0.3.6

________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "1.0"

import os
import logging
import argparse
import sys

# Import the main SiLA library
from sila2lib.sila_server import SiLA2Server

# Import gRPC libraries of features
from MT_Viper_SW_Balance_Service.BalanceService.gRPC import BalanceService_pb2
from MT_Viper_SW_Balance_Service.BalanceService.gRPC import BalanceService_pb2_grpc
# import default arguments for this feature
from MT_Viper_SW_Balance_Service.BalanceService.BalanceService_default_arguments import default_dict as BalanceService_default_dict
from MT_Viper_SW_Balance_Service.DeviceInformationProvider.gRPC import DeviceInformationProvider_pb2
from MT_Viper_SW_Balance_Service.DeviceInformationProvider.gRPC import DeviceInformationProvider_pb2_grpc
# import default arguments for this feature
from MT_Viper_SW_Balance_Service.DeviceInformationProvider.DeviceInformationProvider_default_arguments import default_dict as DeviceInformationProvider_default_dict
from MT_Viper_SW_Balance_Service.DisplayController.gRPC import DisplayController_pb2
from MT_Viper_SW_Balance_Service.DisplayController.gRPC import DisplayController_pb2_grpc
# import default arguments for this feature
from MT_Viper_SW_Balance_Service.DisplayController.DisplayController_default_arguments import default_dict as DisplayController_default_dict

# Import the servicer modules for each feature
from MT_Viper_SW_Balance_Service.BalanceService.BalanceService_servicer import BalanceService
from MT_Viper_SW_Balance_Service.DeviceInformationProvider.DeviceInformationProvider_servicer import DeviceInformationProvider
from MT_Viper_SW_Balance_Service.DisplayController.DisplayController_servicer import DisplayController

# import custom implementation libraries
import serial
import serial.tools.list_ports
import multiprocessing
from contextlib import contextmanager # used for locking
from MT_Viper_SW_Balance_Service.SerialDetector import serial_ports


# optional hardware interface communication
# from sila2comlib.com.com_serial import ComSerial
class Properties:
    ser = None
    port_name = ''
    baud_rate: int = 9600
    parity: str = serial.PARITY_NONE
    stop_bits: str = serial.STOPBITS_ONE
    byte_size: str = serial.EIGHTBITS
    timeout: float = 0.2

    pump_address = 1

    port_name_sim = ''
    baud_rate_sim: int = 9600
    parity_sim: str = serial.PARITY_NONE
    stop_bits_sim: str = serial.STOPBITS_ONE
    byte_size_sim: str = serial.EIGHTBITS
    timeout_sim: float = 0.2

    pump_address_sim = 1

    lock = multiprocessing.Lock()

    status = 'Idle'

    def __init__(self, port_name):
        self.port_name: str = port_name  # .upper()

    def initial_connection(self):
        """
        Establishes a connection with the hardware device on startup, if server is running in real-mode.
        :return:
        """
        try:
            ports = []
            available_ports = serial_ports()
            ports_not_required = []
            for i in range(0, len(available_ports), 1):
                if available_ports[i] in [self.port_name]:
                    ports.append(available_ports[i])
                else:
                    ports_not_required.append(available_ports[i])
            unavailable_ports = list(set(available_ports) - set(ports))
            if unavailable_ports is not []:
                logging.warning(
                    "The following other COM/USB port/s are available: {ports}".format(ports=unavailable_ports))
            if ports == []:
                logging.critical(
                    "No available COM/USB port/s to establish connection to: {ports}".format(ports=ports))
                self.connect()
                raise ConnectionError
            else:
                self.connect()
        except Exception as e:
            logging.exception(e)
            sys.exit(1)

    def connect(self):
        """
        Establish connection with the hardware device using the serial communication port with pySerial.
        """
        try:
            self.ser = serial.Serial(port=self.port_name,
                                     baudrate=self.baud_rate,
                                     parity=self.parity,
                                     stopbits=self.stop_bits,
                                     bytesize=self.byte_size,
                                     timeout=self.timeout)
            logging.info(
                "Connection established on port: {ports}".format(ports=self.port_name))
            return 'Connection successful'
        except (serial.SerialException, serial.SerialTimeoutException, AttributeError, ConnectionError, PermissionError)\
                as exception:
            logging.exception(
                "Connection could not be established on port: {ports}".format(ports=self.port_name))
            return f'Connection unsuccessful: {type(exception).__name__} '

    @contextmanager
    def acquire_timeout_lock(self, timeout: float = 0.2):
        result = self.lock.acquire(timeout=timeout)
        yield result
        if result:
            self.lock.release()


class MT_Viper_SW_Balance_ServiceServer(SiLA2Server):
    """
    This is a Mettler Toledo Viper SW balance service
    """
    hardware_interface = None

    def __init__(self, cmd_args, simulation_mode: bool = True, com_port: str = None):
        """Class initialiser"""
        super().__init__(
            name='MT_Viper_SW',
            description="This is a MT Viper SW Balance Service",
            server_type="Balance",
            server_uuid=None,
            version=__version__,
            vendor_url="",
            ip=cmd_args.ip_address, port=cmd_args.port,  # change to hostname/port
            key_file=cmd_args.encryption_key, cert_file=cmd_args.encryption_cert
        )

        self.simulation_mode = simulation_mode

        # Define some server properties
        self.hardware_interface = Properties(com_port)
        self.simulation_mode = simulation_mode

        if not simulation_mode:
            self.hardware_interface.initial_connection()
        else:
            self.ser = None

        logging.info(
            "Starting SiLA2 server with server name: MT_Viper_SW"
        )

        # registering features
        #  Register BalanceService
        self.BalanceService_servicer = BalanceService(simulation_mode=self.simulation_mode,
                                                      hardware_interface=self.hardware_interface)
        BalanceService_pb2_grpc.add_BalanceServiceServicer_to_server(
            self.BalanceService_servicer,
            self.grpc_server
        )
        self.add_feature(feature_id='BalanceService',  # org.silastandard/examples/DeviceInformationProvider/v1
                         servicer=self.BalanceService_servicer,
                         data_path=os.path.join(
                            os.path.dirname(os.path.abspath(__file__)), 'meta'
                         ))
        #  Register DeviceInformationProvider
        self.DeviceInformationProvider_servicer = DeviceInformationProvider(simulation_mode=self.simulation_mode,
                                                                            hardware_interface=self.hardware_interface)
        DeviceInformationProvider_pb2_grpc.add_DeviceInformationProviderServicer_to_server(
            self.DeviceInformationProvider_servicer,
            self.grpc_server
        )
        self.add_feature(feature_id='DeviceInformationProvider',  # org.silastandard/examples/DeviceInformationProvider/v1
                         servicer=self.DeviceInformationProvider_servicer,
                         data_path=os.path.join(
                             os.path.dirname(os.path.abspath(__file__)), 'meta'
                         ))
        #  Register DisplayController
        self.DisplayController_servicer = DisplayController(simulation_mode=self.simulation_mode,
                                                                   hardware_interface=self.hardware_interface)
        DisplayController_pb2_grpc.add_DisplayControllerServicer_to_server(
            self.DisplayController_servicer,
            self.grpc_server
        )
        self.add_feature(feature_id='DisplayController',  # org.silastandard/examples/DeviceInformationProvider/v1
                         servicer=self.DisplayController_servicer,
                         data_path=os.path.join(
                             os.path.dirname(os.path.abspath(__file__)), 'meta'
                         ))

        # starting and running the gRPC/SiLA2 server
        self.simulation_mode = simulation_mode
        self.run()

    # overwrite these methods, if required:
    # def pre_switch_to_simulation_mode(self) -> None:
    #    if self.hardware_interface is not None:
    # def post_switch_to_simulation_mode(self) -> None:
    # def pre_switch_to_real_mode(self) -> None: 
    # def post_switch_to_real_mode(self) -> None:
    #    if self.hardware_interface is not None:
    # def shutdown_all(self) -> None:
    
def parse_command_line():
    """
    Just looking for commandline arguments
    """
    parser = argparse.ArgumentParser(description="A SiLA2 service: MT_Viper_SW_Balance_Service")

    # simple arguments for the server identification
    parser.add_argument('-s', '--server-name', action='store',
                        default="MT_Viper_SW_Balance_Service", help='start SiLA server with SiLA server name [server-name]')
    parser.add_argument('-t', '--server-type', action='store',
                        default="Balance", help='start SiLA server with SiLA server type [server-type]')
    parser.add_argument('-d', '--description', action='store',
                        default="This is a Mettler Toledo Viper SW balance service", help='SiLA server description')

    # connection parameters
    parser.add_argument('--server-hostname', action='store', default='localhost',
                        help='SiLA server hostname')
    parser.add_argument("-a", "--ip-address", default="127.0.0.1", help="The IP address (default: '127.0.0.1')")
    parser.add_argument("-p", "--port", type=int, default=50052, help="The port (default: 50052)")

    # encryption
    parser.add_argument('-X', '--encryption', action='store', default=None,
                        help='The name of the private key and certificate file (without extension).')
    parser.add_argument('--encryption-key', action='store', default=None,
                        help='The name of the encryption key (*with* extension). Can be used if key and certificate '
                             'vary or non-standard file extensions are used.')
    parser.add_argument('--encryption-cert', action='store', default=None,
                        help='The name of the encryption certificate (*with* extension). Can be used if key and '
                             'certificate vary or non-standard file extensions are used.')

    parser.add_argument('-v', '--version', action='version', version='%(prog)s ' + __version__)

    parsed_args = parser.parse_args()

    # validate/update some settings
    #   encryption
    if parsed_args.encryption is not None:
        # only overwrite the separate keys if not given manually
        if parsed_args.encryption_key is None:
            parsed_args.encryption_key = parsed_args.encryption + '.key'
        if parsed_args.encryption_cert is None:
            parsed_args.encryption_cert = parsed_args.encryption + '.cert'

    return parsed_args
    
        
if __name__ == '__main__':
    # or use logging.ERROR for less output
    logging.basicConfig(format='%(levelname)-8s| %(module)s.%(funcName)s: %(message)s', level=logging.DEBUG)
    
    args = parse_command_line()

    # generate SiLA2Server
    sila_server = MT_Viper_SW_Balance_ServiceServer(cmd_args=args, simulation_mode=True, com_port='Com1')
