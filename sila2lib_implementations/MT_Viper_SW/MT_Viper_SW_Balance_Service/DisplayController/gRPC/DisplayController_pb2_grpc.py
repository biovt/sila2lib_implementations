# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

from . import DisplayController_pb2 as DisplayController__pb2


class DisplayControllerStub(object):
    """Feature: Display Controller

    THis feature contains functions to control the balances display.
    By Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 20.05.2019

    """

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.WriteToDisplay = channel.unary_unary(
                '/sila2.org.silastandard.examples.displaycontroller.v1.DisplayController/WriteToDisplay',
                request_serializer=DisplayController__pb2.WriteToDisplay_Parameters.SerializeToString,
                response_deserializer=DisplayController__pb2.WriteToDisplay_Responses.FromString,
                )
        self.ShowWeight = channel.unary_unary(
                '/sila2.org.silastandard.examples.displaycontroller.v1.DisplayController/ShowWeight',
                request_serializer=DisplayController__pb2.ShowWeight_Parameters.SerializeToString,
                response_deserializer=DisplayController__pb2.ShowWeight_Responses.FromString,
                )


class DisplayControllerServicer(object):
    """Feature: Display Controller

    THis feature contains functions to control the balances display.
    By Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 20.05.2019

    """

    def WriteToDisplay(self, request, context):
        """Write to Display
        Write text to the balance display.
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def ShowWeight(self, request, context):
        """Show Weight
        Resets the display after using the WriteToDisplay command. The device display will show the current weight value and
        unit.
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_DisplayControllerServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'WriteToDisplay': grpc.unary_unary_rpc_method_handler(
                    servicer.WriteToDisplay,
                    request_deserializer=DisplayController__pb2.WriteToDisplay_Parameters.FromString,
                    response_serializer=DisplayController__pb2.WriteToDisplay_Responses.SerializeToString,
            ),
            'ShowWeight': grpc.unary_unary_rpc_method_handler(
                    servicer.ShowWeight,
                    request_deserializer=DisplayController__pb2.ShowWeight_Parameters.FromString,
                    response_serializer=DisplayController__pb2.ShowWeight_Responses.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'sila2.org.silastandard.examples.displaycontroller.v1.DisplayController', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class DisplayController(object):
    """Feature: Display Controller

    THis feature contains functions to control the balances display.
    By Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 20.05.2019

    """

    @staticmethod
    def WriteToDisplay(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/sila2.org.silastandard.examples.displaycontroller.v1.DisplayController/WriteToDisplay',
            DisplayController__pb2.WriteToDisplay_Parameters.SerializeToString,
            DisplayController__pb2.WriteToDisplay_Responses.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def ShowWeight(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/sila2.org.silastandard.examples.displaycontroller.v1.DisplayController/ShowWeight',
            DisplayController__pb2.ShowWeight_Parameters.SerializeToString,
            DisplayController__pb2.ShowWeight_Responses.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)
