import logging
from argparse import ArgumentParser

from .MT_Viper_SW_Balance_Service_server import MT_Viper_SW_Balance_ServiceServer as Server


logger = logging.getLogger(__name__)


def parse_args():
    parser = ArgumentParser(prog="RegloICC", description="Start this SiLA 2 server")
    parser.add_argument("-a", "--ip-address", default="127.0.0.1", help="The IP address (default: '127.0.0.1')")
    parser.add_argument("-p", "--port", type=int, default=50052, help="The port (default: 50052)")
    parser.add_argument("--disable-discovery", action="store_true", help="Disable SiLA Server Discovery")
    parser.add_argument("-s", "--simulation-mode", action="store_true", help="Specify if you want to start in simulation mode.")
    parser.add_argument("-u", "--unit-id", type=str, default="",
                        help="Specify the unit that shall be connected to this server.")

    # encryption
    parser.add_argument('-X', '--encryption', action='store', default=None,
                        help='The name of the private key and certificate file (without extension).')
    parser.add_argument('--encryption-key', action='store', default=None,
                        help='The name of the encryption key (*with* extension). Can be used if key and certificate '
                             'vary or non-standard file extensions are used.')
    parser.add_argument('--encryption-cert', action='store', default=None,
                        help='The name of the encryption certificate (*with* extension). Can be used if key and '
                             'certificate vary or non-standard file extensions are used.')

    log_level_group = parser.add_mutually_exclusive_group()
    log_level_group.add_argument("-q", "--quiet", action="store_true", help="Only log errors")
    log_level_group.add_argument("-v", "--verbose", action="store_true", help="Enable verbose logging")
    log_level_group.add_argument("-d", "--debug", action="store_true", help="Enable debug logging")

    return parser.parse_args()


def start_server(args):

    server = Server(cmd_args=args, simulation_mode=True, com_port='Com1')

    if args.unit_id == '':
        logging.warning(f'Default unit-id ("") is used. You should specify a RegloICC unit!')
    try:
        print(f"Server startup complete, running on {args.ip_address}:{args.port}. Press Enter to stop it")
        try:
            input()
        except KeyboardInterrupt:
            pass
    finally:
        server.stop_grpc_server()
        print("Stopped server")


def setup_basic_logging(args):
    level = logging.WARNING
    if args.verbose:
        level = logging.INFO
    if args.debug:
        level = logging.DEBUG
    if args.quiet:
        level = logging.ERROR

    logging.basicConfig(level=level, format="%(asctime)s:%(levelname)s:%(name)s:%(message)s")


if __name__ == "__main__":
    args = parse_args()
    setup_basic_logging(args)
    start_server(args)
