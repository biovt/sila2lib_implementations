from setuptools import find_packages, setup

setup(
    name="MT_Viper_SW_Balance_Service",
    packages=find_packages(),
    install_requires=["sila2lib", "pyserial", "numpy"],
    include_package_data=True,
)
