"""
________________________________________________________________________

:PROJECT: SiLA2_python

*Balance Service_defined_errors*

:details: BalanceService Defined SiLA Error factories:
    This feature enables several modes of weight measuring and allows taring and zeroing of the balance.
    By Lukas Bromig, Institute of Biochemical Engineering, Technical University of Munich, 07.04.2021

:file:    BalanceService_defined_errors.py
:authors: Lukas Bromig

:date: (creation)          2021-04-07T13:30:01.315279
:date: (last modification) 2021-04-07T13:30:01.315279

.. note:: Code generated by sila2codegenerator 0.3.6

________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "1.0"

# import general packages
from sila2lib.error_handling.server_err import SiLAExecutionError

# SiLA Defined Error factories

def generate_def_error_OverloadError(extra_message: str = "") \
        -> SiLAExecutionError:
    """
    Generates a defined SiLAExcecutionError with id "OverloadError"

    :param extra_message: extra message, that can be added to the default message
    :returns: SiLAExecutionError
    """

    msg = f"""Weigh module or balance is in overload range (weighing range exceeded). \n{extra_message}"""
    return SiLAExecutionError(error_identifier="OverloadError",
                         msg=msg)


def generate_def_error_UnderloadError(extra_message: str = "") \
        -> SiLAExecutionError:
    """
    Generates a defined SiLAExcecutionError with id "UnderloadError"

    :param extra_message: extra message, that can be added to the default message
    :returns: SiLAExecutionError
    """

    msg = f"""Weighing module or balance is in underload range (e.g. weighing pane is not in place). \n{extra_message}"""
    return SiLAExecutionError(error_identifier="UnderloadError",
                         msg=msg)


def generate_def_error_InternalError(extra_message: str = "") \
        -> SiLAExecutionError:
    """
    Generates a defined SiLAExcecutionError with id "InternalError"

    :param extra_message: extra message, that can be added to the default message
    :returns: SiLAExecutionError
    """

    msg = f"""Internal error. Balance not ready yet. \n{extra_message}"""
    return SiLAExecutionError(error_identifier="InternalError",
                         msg=msg)


def generate_def_error_LogicalError(extra_message: str = "") \
        -> SiLAExecutionError:
    """
    Generates a defined SiLAExcecutionError with id "LogicalError"

    :param extra_message: extra message, that can be added to the default message
    :returns: SiLAExecutionError
    """

    msg = f"""Logical error. Balance not ready yet. \n{extra_message}"""
    return SiLAExecutionError(error_identifier="LogicalError",
                         msg=msg)

