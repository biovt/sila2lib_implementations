# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: DeviceService.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


import sila2lib.framework.SiLAFramework_pb2 as SiLAFramework__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='DeviceService.proto',
  package='sila2.biovt.mw.tum.de.examples.deviceservice.v1',
  syntax='proto3',
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\x13\x44\x65viceService.proto\x12/sila2.biovt.mw.tum.de.examples.deviceservice.v1\x1a\x13SiLAFramework.proto\"\x1b\n\x19GetValveOutput_Parameters\"T\n\x18GetValveOutput_Responses\x12\x38\n\x12\x43urrentValveOutput\x18\x01 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real\"\x1b\n\x19GetCapacity100_Parameters\"T\n\x18GetCapacity100_Responses\x12\x38\n\x12\x43urrentCapacity100\x18\x01 \x01(\x0b\x32\x1c.sila2.org.silastandard.Real\"\x1c\n\x1aGetCapacityUnit_Parameters\"X\n\x19GetCapacityUnit_Responses\x12;\n\x13\x43urrentCapacityUnit\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.String\"\x1c\n\x1aGetSerialNumber_Parameters\"X\n\x19GetSerialNumber_Responses\x12;\n\x13\x43urrentSerialNumber\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.String\"\"\n GetPrimaryNodeAddress_Parameters\"d\n\x1fGetPrimaryNodeAddress_Responses\x12\x41\n\x19\x43urrentPrimaryNodeAddress\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.String\"\x1f\n\x1dGetFirmwareVersion_Parameters\"^\n\x1cGetFirmwareVersion_Responses\x12>\n\x16\x43urrentFirmwareVersion\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.String\"%\n#GetCommunicationProtocol_Parameters\"j\n\"GetCommunicationProtocol_Responses\x12\x44\n\x1c\x43urrentCommunicationProtocol\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.String\"+\n)SetCommunicationProtocolBinary_Parameters\"v\n(SetCommunicationProtocolBinary_Responses\x12J\n\"CurrentCommunicationProtocolBinary\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.String\"*\n(SetCommunicationProtocolAscii_Parameters\"t\n\'SetCommunicationProtocolAscii_Responses\x12I\n!CurrentCommunicationProtocolAscii\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.String\"\x1a\n\x18GetSerialPort_Parameters\"T\n\x17GetSerialPort_Responses\x12\x39\n\x11\x43urrentSerialPort\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.String\"Q\n\x18SetSerialPort_Parameters\x12\x35\n\rSetSerialPort\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.String\"P\n\x17SetSerialPort_Responses\x12\x35\n\rNewSerialPort\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.String2\x8f\x10\n\rDeviceService\x12\xa9\x01\n\x0eGetValveOutput\x12J.sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetValveOutput_Parameters\x1aI.sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetValveOutput_Responses\"\x00\x12\xa9\x01\n\x0eGetCapacity100\x12J.sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetCapacity100_Parameters\x1aI.sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetCapacity100_Responses\"\x00\x12\xac\x01\n\x0fGetCapacityUnit\x12K.sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetCapacityUnit_Parameters\x1aJ.sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetCapacityUnit_Responses\"\x00\x12\xac\x01\n\x0fGetSerialNumber\x12K.sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetSerialNumber_Parameters\x1aJ.sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetSerialNumber_Responses\"\x00\x12\xbe\x01\n\x15GetPrimaryNodeAddress\x12Q.sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetPrimaryNodeAddress_Parameters\x1aP.sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetPrimaryNodeAddress_Responses\"\x00\x12\xb5\x01\n\x12GetFirmwareVersion\x12N.sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetFirmwareVersion_Parameters\x1aM.sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetFirmwareVersion_Responses\"\x00\x12\xc7\x01\n\x18GetCommunicationProtocol\x12T.sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetCommunicationProtocol_Parameters\x1aS.sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetCommunicationProtocol_Responses\"\x00\x12\xd9\x01\n\x1eSetCommunicationProtocolBinary\x12Z.sila2.biovt.mw.tum.de.examples.deviceservice.v1.SetCommunicationProtocolBinary_Parameters\x1aY.sila2.biovt.mw.tum.de.examples.deviceservice.v1.SetCommunicationProtocolBinary_Responses\"\x00\x12\xd6\x01\n\x1dSetCommunicationProtocolAscii\x12Y.sila2.biovt.mw.tum.de.examples.deviceservice.v1.SetCommunicationProtocolAscii_Parameters\x1aX.sila2.biovt.mw.tum.de.examples.deviceservice.v1.SetCommunicationProtocolAscii_Responses\"\x00\x12\xa6\x01\n\rGetSerialPort\x12I.sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetSerialPort_Parameters\x1aH.sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetSerialPort_Responses\"\x00\x12\xa6\x01\n\rSetSerialPort\x12I.sila2.biovt.mw.tum.de.examples.deviceservice.v1.SetSerialPort_Parameters\x1aH.sila2.biovt.mw.tum.de.examples.deviceservice.v1.SetSerialPort_Responses\"\x00\x62\x06proto3'
  ,
  dependencies=[SiLAFramework__pb2.DESCRIPTOR,])




_GETVALVEOUTPUT_PARAMETERS = _descriptor.Descriptor(
  name='GetValveOutput_Parameters',
  full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetValveOutput_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=93,
  serialized_end=120,
)


_GETVALVEOUTPUT_RESPONSES = _descriptor.Descriptor(
  name='GetValveOutput_Responses',
  full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetValveOutput_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='CurrentValveOutput', full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetValveOutput_Responses.CurrentValveOutput', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=122,
  serialized_end=206,
)


_GETCAPACITY100_PARAMETERS = _descriptor.Descriptor(
  name='GetCapacity100_Parameters',
  full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetCapacity100_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=208,
  serialized_end=235,
)


_GETCAPACITY100_RESPONSES = _descriptor.Descriptor(
  name='GetCapacity100_Responses',
  full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetCapacity100_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='CurrentCapacity100', full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetCapacity100_Responses.CurrentCapacity100', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=237,
  serialized_end=321,
)


_GETCAPACITYUNIT_PARAMETERS = _descriptor.Descriptor(
  name='GetCapacityUnit_Parameters',
  full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetCapacityUnit_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=323,
  serialized_end=351,
)


_GETCAPACITYUNIT_RESPONSES = _descriptor.Descriptor(
  name='GetCapacityUnit_Responses',
  full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetCapacityUnit_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='CurrentCapacityUnit', full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetCapacityUnit_Responses.CurrentCapacityUnit', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=353,
  serialized_end=441,
)


_GETSERIALNUMBER_PARAMETERS = _descriptor.Descriptor(
  name='GetSerialNumber_Parameters',
  full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetSerialNumber_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=443,
  serialized_end=471,
)


_GETSERIALNUMBER_RESPONSES = _descriptor.Descriptor(
  name='GetSerialNumber_Responses',
  full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetSerialNumber_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='CurrentSerialNumber', full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetSerialNumber_Responses.CurrentSerialNumber', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=473,
  serialized_end=561,
)


_GETPRIMARYNODEADDRESS_PARAMETERS = _descriptor.Descriptor(
  name='GetPrimaryNodeAddress_Parameters',
  full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetPrimaryNodeAddress_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=563,
  serialized_end=597,
)


_GETPRIMARYNODEADDRESS_RESPONSES = _descriptor.Descriptor(
  name='GetPrimaryNodeAddress_Responses',
  full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetPrimaryNodeAddress_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='CurrentPrimaryNodeAddress', full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetPrimaryNodeAddress_Responses.CurrentPrimaryNodeAddress', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=599,
  serialized_end=699,
)


_GETFIRMWAREVERSION_PARAMETERS = _descriptor.Descriptor(
  name='GetFirmwareVersion_Parameters',
  full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetFirmwareVersion_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=701,
  serialized_end=732,
)


_GETFIRMWAREVERSION_RESPONSES = _descriptor.Descriptor(
  name='GetFirmwareVersion_Responses',
  full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetFirmwareVersion_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='CurrentFirmwareVersion', full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetFirmwareVersion_Responses.CurrentFirmwareVersion', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=734,
  serialized_end=828,
)


_GETCOMMUNICATIONPROTOCOL_PARAMETERS = _descriptor.Descriptor(
  name='GetCommunicationProtocol_Parameters',
  full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetCommunicationProtocol_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=830,
  serialized_end=867,
)


_GETCOMMUNICATIONPROTOCOL_RESPONSES = _descriptor.Descriptor(
  name='GetCommunicationProtocol_Responses',
  full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetCommunicationProtocol_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='CurrentCommunicationProtocol', full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetCommunicationProtocol_Responses.CurrentCommunicationProtocol', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=869,
  serialized_end=975,
)


_SETCOMMUNICATIONPROTOCOLBINARY_PARAMETERS = _descriptor.Descriptor(
  name='SetCommunicationProtocolBinary_Parameters',
  full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.SetCommunicationProtocolBinary_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=977,
  serialized_end=1020,
)


_SETCOMMUNICATIONPROTOCOLBINARY_RESPONSES = _descriptor.Descriptor(
  name='SetCommunicationProtocolBinary_Responses',
  full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.SetCommunicationProtocolBinary_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='CurrentCommunicationProtocolBinary', full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.SetCommunicationProtocolBinary_Responses.CurrentCommunicationProtocolBinary', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1022,
  serialized_end=1140,
)


_SETCOMMUNICATIONPROTOCOLASCII_PARAMETERS = _descriptor.Descriptor(
  name='SetCommunicationProtocolAscii_Parameters',
  full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.SetCommunicationProtocolAscii_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1142,
  serialized_end=1184,
)


_SETCOMMUNICATIONPROTOCOLASCII_RESPONSES = _descriptor.Descriptor(
  name='SetCommunicationProtocolAscii_Responses',
  full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.SetCommunicationProtocolAscii_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='CurrentCommunicationProtocolAscii', full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.SetCommunicationProtocolAscii_Responses.CurrentCommunicationProtocolAscii', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1186,
  serialized_end=1302,
)


_GETSERIALPORT_PARAMETERS = _descriptor.Descriptor(
  name='GetSerialPort_Parameters',
  full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetSerialPort_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1304,
  serialized_end=1330,
)


_GETSERIALPORT_RESPONSES = _descriptor.Descriptor(
  name='GetSerialPort_Responses',
  full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetSerialPort_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='CurrentSerialPort', full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetSerialPort_Responses.CurrentSerialPort', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1332,
  serialized_end=1416,
)


_SETSERIALPORT_PARAMETERS = _descriptor.Descriptor(
  name='SetSerialPort_Parameters',
  full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.SetSerialPort_Parameters',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='SetSerialPort', full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.SetSerialPort_Parameters.SetSerialPort', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1418,
  serialized_end=1499,
)


_SETSERIALPORT_RESPONSES = _descriptor.Descriptor(
  name='SetSerialPort_Responses',
  full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.SetSerialPort_Responses',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='NewSerialPort', full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.SetSerialPort_Responses.NewSerialPort', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1501,
  serialized_end=1581,
)

_GETVALVEOUTPUT_RESPONSES.fields_by_name['CurrentValveOutput'].message_type = SiLAFramework__pb2._REAL
_GETCAPACITY100_RESPONSES.fields_by_name['CurrentCapacity100'].message_type = SiLAFramework__pb2._REAL
_GETCAPACITYUNIT_RESPONSES.fields_by_name['CurrentCapacityUnit'].message_type = SiLAFramework__pb2._STRING
_GETSERIALNUMBER_RESPONSES.fields_by_name['CurrentSerialNumber'].message_type = SiLAFramework__pb2._STRING
_GETPRIMARYNODEADDRESS_RESPONSES.fields_by_name['CurrentPrimaryNodeAddress'].message_type = SiLAFramework__pb2._STRING
_GETFIRMWAREVERSION_RESPONSES.fields_by_name['CurrentFirmwareVersion'].message_type = SiLAFramework__pb2._STRING
_GETCOMMUNICATIONPROTOCOL_RESPONSES.fields_by_name['CurrentCommunicationProtocol'].message_type = SiLAFramework__pb2._STRING
_SETCOMMUNICATIONPROTOCOLBINARY_RESPONSES.fields_by_name['CurrentCommunicationProtocolBinary'].message_type = SiLAFramework__pb2._STRING
_SETCOMMUNICATIONPROTOCOLASCII_RESPONSES.fields_by_name['CurrentCommunicationProtocolAscii'].message_type = SiLAFramework__pb2._STRING
_GETSERIALPORT_RESPONSES.fields_by_name['CurrentSerialPort'].message_type = SiLAFramework__pb2._STRING
_SETSERIALPORT_PARAMETERS.fields_by_name['SetSerialPort'].message_type = SiLAFramework__pb2._STRING
_SETSERIALPORT_RESPONSES.fields_by_name['NewSerialPort'].message_type = SiLAFramework__pb2._STRING
DESCRIPTOR.message_types_by_name['GetValveOutput_Parameters'] = _GETVALVEOUTPUT_PARAMETERS
DESCRIPTOR.message_types_by_name['GetValveOutput_Responses'] = _GETVALVEOUTPUT_RESPONSES
DESCRIPTOR.message_types_by_name['GetCapacity100_Parameters'] = _GETCAPACITY100_PARAMETERS
DESCRIPTOR.message_types_by_name['GetCapacity100_Responses'] = _GETCAPACITY100_RESPONSES
DESCRIPTOR.message_types_by_name['GetCapacityUnit_Parameters'] = _GETCAPACITYUNIT_PARAMETERS
DESCRIPTOR.message_types_by_name['GetCapacityUnit_Responses'] = _GETCAPACITYUNIT_RESPONSES
DESCRIPTOR.message_types_by_name['GetSerialNumber_Parameters'] = _GETSERIALNUMBER_PARAMETERS
DESCRIPTOR.message_types_by_name['GetSerialNumber_Responses'] = _GETSERIALNUMBER_RESPONSES
DESCRIPTOR.message_types_by_name['GetPrimaryNodeAddress_Parameters'] = _GETPRIMARYNODEADDRESS_PARAMETERS
DESCRIPTOR.message_types_by_name['GetPrimaryNodeAddress_Responses'] = _GETPRIMARYNODEADDRESS_RESPONSES
DESCRIPTOR.message_types_by_name['GetFirmwareVersion_Parameters'] = _GETFIRMWAREVERSION_PARAMETERS
DESCRIPTOR.message_types_by_name['GetFirmwareVersion_Responses'] = _GETFIRMWAREVERSION_RESPONSES
DESCRIPTOR.message_types_by_name['GetCommunicationProtocol_Parameters'] = _GETCOMMUNICATIONPROTOCOL_PARAMETERS
DESCRIPTOR.message_types_by_name['GetCommunicationProtocol_Responses'] = _GETCOMMUNICATIONPROTOCOL_RESPONSES
DESCRIPTOR.message_types_by_name['SetCommunicationProtocolBinary_Parameters'] = _SETCOMMUNICATIONPROTOCOLBINARY_PARAMETERS
DESCRIPTOR.message_types_by_name['SetCommunicationProtocolBinary_Responses'] = _SETCOMMUNICATIONPROTOCOLBINARY_RESPONSES
DESCRIPTOR.message_types_by_name['SetCommunicationProtocolAscii_Parameters'] = _SETCOMMUNICATIONPROTOCOLASCII_PARAMETERS
DESCRIPTOR.message_types_by_name['SetCommunicationProtocolAscii_Responses'] = _SETCOMMUNICATIONPROTOCOLASCII_RESPONSES
DESCRIPTOR.message_types_by_name['GetSerialPort_Parameters'] = _GETSERIALPORT_PARAMETERS
DESCRIPTOR.message_types_by_name['GetSerialPort_Responses'] = _GETSERIALPORT_RESPONSES
DESCRIPTOR.message_types_by_name['SetSerialPort_Parameters'] = _SETSERIALPORT_PARAMETERS
DESCRIPTOR.message_types_by_name['SetSerialPort_Responses'] = _SETSERIALPORT_RESPONSES
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

GetValveOutput_Parameters = _reflection.GeneratedProtocolMessageType('GetValveOutput_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GETVALVEOUTPUT_PARAMETERS,
  '__module__' : 'DeviceService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetValveOutput_Parameters)
  })
_sym_db.RegisterMessage(GetValveOutput_Parameters)

GetValveOutput_Responses = _reflection.GeneratedProtocolMessageType('GetValveOutput_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GETVALVEOUTPUT_RESPONSES,
  '__module__' : 'DeviceService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetValveOutput_Responses)
  })
_sym_db.RegisterMessage(GetValveOutput_Responses)

GetCapacity100_Parameters = _reflection.GeneratedProtocolMessageType('GetCapacity100_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GETCAPACITY100_PARAMETERS,
  '__module__' : 'DeviceService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetCapacity100_Parameters)
  })
_sym_db.RegisterMessage(GetCapacity100_Parameters)

GetCapacity100_Responses = _reflection.GeneratedProtocolMessageType('GetCapacity100_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GETCAPACITY100_RESPONSES,
  '__module__' : 'DeviceService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetCapacity100_Responses)
  })
_sym_db.RegisterMessage(GetCapacity100_Responses)

GetCapacityUnit_Parameters = _reflection.GeneratedProtocolMessageType('GetCapacityUnit_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GETCAPACITYUNIT_PARAMETERS,
  '__module__' : 'DeviceService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetCapacityUnit_Parameters)
  })
_sym_db.RegisterMessage(GetCapacityUnit_Parameters)

GetCapacityUnit_Responses = _reflection.GeneratedProtocolMessageType('GetCapacityUnit_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GETCAPACITYUNIT_RESPONSES,
  '__module__' : 'DeviceService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetCapacityUnit_Responses)
  })
_sym_db.RegisterMessage(GetCapacityUnit_Responses)

GetSerialNumber_Parameters = _reflection.GeneratedProtocolMessageType('GetSerialNumber_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GETSERIALNUMBER_PARAMETERS,
  '__module__' : 'DeviceService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetSerialNumber_Parameters)
  })
_sym_db.RegisterMessage(GetSerialNumber_Parameters)

GetSerialNumber_Responses = _reflection.GeneratedProtocolMessageType('GetSerialNumber_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GETSERIALNUMBER_RESPONSES,
  '__module__' : 'DeviceService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetSerialNumber_Responses)
  })
_sym_db.RegisterMessage(GetSerialNumber_Responses)

GetPrimaryNodeAddress_Parameters = _reflection.GeneratedProtocolMessageType('GetPrimaryNodeAddress_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GETPRIMARYNODEADDRESS_PARAMETERS,
  '__module__' : 'DeviceService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetPrimaryNodeAddress_Parameters)
  })
_sym_db.RegisterMessage(GetPrimaryNodeAddress_Parameters)

GetPrimaryNodeAddress_Responses = _reflection.GeneratedProtocolMessageType('GetPrimaryNodeAddress_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GETPRIMARYNODEADDRESS_RESPONSES,
  '__module__' : 'DeviceService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetPrimaryNodeAddress_Responses)
  })
_sym_db.RegisterMessage(GetPrimaryNodeAddress_Responses)

GetFirmwareVersion_Parameters = _reflection.GeneratedProtocolMessageType('GetFirmwareVersion_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GETFIRMWAREVERSION_PARAMETERS,
  '__module__' : 'DeviceService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetFirmwareVersion_Parameters)
  })
_sym_db.RegisterMessage(GetFirmwareVersion_Parameters)

GetFirmwareVersion_Responses = _reflection.GeneratedProtocolMessageType('GetFirmwareVersion_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GETFIRMWAREVERSION_RESPONSES,
  '__module__' : 'DeviceService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetFirmwareVersion_Responses)
  })
_sym_db.RegisterMessage(GetFirmwareVersion_Responses)

GetCommunicationProtocol_Parameters = _reflection.GeneratedProtocolMessageType('GetCommunicationProtocol_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GETCOMMUNICATIONPROTOCOL_PARAMETERS,
  '__module__' : 'DeviceService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetCommunicationProtocol_Parameters)
  })
_sym_db.RegisterMessage(GetCommunicationProtocol_Parameters)

GetCommunicationProtocol_Responses = _reflection.GeneratedProtocolMessageType('GetCommunicationProtocol_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GETCOMMUNICATIONPROTOCOL_RESPONSES,
  '__module__' : 'DeviceService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetCommunicationProtocol_Responses)
  })
_sym_db.RegisterMessage(GetCommunicationProtocol_Responses)

SetCommunicationProtocolBinary_Parameters = _reflection.GeneratedProtocolMessageType('SetCommunicationProtocolBinary_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _SETCOMMUNICATIONPROTOCOLBINARY_PARAMETERS,
  '__module__' : 'DeviceService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.deviceservice.v1.SetCommunicationProtocolBinary_Parameters)
  })
_sym_db.RegisterMessage(SetCommunicationProtocolBinary_Parameters)

SetCommunicationProtocolBinary_Responses = _reflection.GeneratedProtocolMessageType('SetCommunicationProtocolBinary_Responses', (_message.Message,), {
  'DESCRIPTOR' : _SETCOMMUNICATIONPROTOCOLBINARY_RESPONSES,
  '__module__' : 'DeviceService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.deviceservice.v1.SetCommunicationProtocolBinary_Responses)
  })
_sym_db.RegisterMessage(SetCommunicationProtocolBinary_Responses)

SetCommunicationProtocolAscii_Parameters = _reflection.GeneratedProtocolMessageType('SetCommunicationProtocolAscii_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _SETCOMMUNICATIONPROTOCOLASCII_PARAMETERS,
  '__module__' : 'DeviceService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.deviceservice.v1.SetCommunicationProtocolAscii_Parameters)
  })
_sym_db.RegisterMessage(SetCommunicationProtocolAscii_Parameters)

SetCommunicationProtocolAscii_Responses = _reflection.GeneratedProtocolMessageType('SetCommunicationProtocolAscii_Responses', (_message.Message,), {
  'DESCRIPTOR' : _SETCOMMUNICATIONPROTOCOLASCII_RESPONSES,
  '__module__' : 'DeviceService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.deviceservice.v1.SetCommunicationProtocolAscii_Responses)
  })
_sym_db.RegisterMessage(SetCommunicationProtocolAscii_Responses)

GetSerialPort_Parameters = _reflection.GeneratedProtocolMessageType('GetSerialPort_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GETSERIALPORT_PARAMETERS,
  '__module__' : 'DeviceService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetSerialPort_Parameters)
  })
_sym_db.RegisterMessage(GetSerialPort_Parameters)

GetSerialPort_Responses = _reflection.GeneratedProtocolMessageType('GetSerialPort_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GETSERIALPORT_RESPONSES,
  '__module__' : 'DeviceService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.deviceservice.v1.GetSerialPort_Responses)
  })
_sym_db.RegisterMessage(GetSerialPort_Responses)

SetSerialPort_Parameters = _reflection.GeneratedProtocolMessageType('SetSerialPort_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _SETSERIALPORT_PARAMETERS,
  '__module__' : 'DeviceService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.deviceservice.v1.SetSerialPort_Parameters)
  })
_sym_db.RegisterMessage(SetSerialPort_Parameters)

SetSerialPort_Responses = _reflection.GeneratedProtocolMessageType('SetSerialPort_Responses', (_message.Message,), {
  'DESCRIPTOR' : _SETSERIALPORT_RESPONSES,
  '__module__' : 'DeviceService_pb2'
  # @@protoc_insertion_point(class_scope:sila2.biovt.mw.tum.de.examples.deviceservice.v1.SetSerialPort_Responses)
  })
_sym_db.RegisterMessage(SetSerialPort_Responses)



_DEVICESERVICE = _descriptor.ServiceDescriptor(
  name='DeviceService',
  full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.DeviceService',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_start=1584,
  serialized_end=3647,
  methods=[
  _descriptor.MethodDescriptor(
    name='GetValveOutput',
    full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.DeviceService.GetValveOutput',
    index=0,
    containing_service=None,
    input_type=_GETVALVEOUTPUT_PARAMETERS,
    output_type=_GETVALVEOUTPUT_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='GetCapacity100',
    full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.DeviceService.GetCapacity100',
    index=1,
    containing_service=None,
    input_type=_GETCAPACITY100_PARAMETERS,
    output_type=_GETCAPACITY100_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='GetCapacityUnit',
    full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.DeviceService.GetCapacityUnit',
    index=2,
    containing_service=None,
    input_type=_GETCAPACITYUNIT_PARAMETERS,
    output_type=_GETCAPACITYUNIT_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='GetSerialNumber',
    full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.DeviceService.GetSerialNumber',
    index=3,
    containing_service=None,
    input_type=_GETSERIALNUMBER_PARAMETERS,
    output_type=_GETSERIALNUMBER_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='GetPrimaryNodeAddress',
    full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.DeviceService.GetPrimaryNodeAddress',
    index=4,
    containing_service=None,
    input_type=_GETPRIMARYNODEADDRESS_PARAMETERS,
    output_type=_GETPRIMARYNODEADDRESS_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='GetFirmwareVersion',
    full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.DeviceService.GetFirmwareVersion',
    index=5,
    containing_service=None,
    input_type=_GETFIRMWAREVERSION_PARAMETERS,
    output_type=_GETFIRMWAREVERSION_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='GetCommunicationProtocol',
    full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.DeviceService.GetCommunicationProtocol',
    index=6,
    containing_service=None,
    input_type=_GETCOMMUNICATIONPROTOCOL_PARAMETERS,
    output_type=_GETCOMMUNICATIONPROTOCOL_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='SetCommunicationProtocolBinary',
    full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.DeviceService.SetCommunicationProtocolBinary',
    index=7,
    containing_service=None,
    input_type=_SETCOMMUNICATIONPROTOCOLBINARY_PARAMETERS,
    output_type=_SETCOMMUNICATIONPROTOCOLBINARY_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='SetCommunicationProtocolAscii',
    full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.DeviceService.SetCommunicationProtocolAscii',
    index=8,
    containing_service=None,
    input_type=_SETCOMMUNICATIONPROTOCOLASCII_PARAMETERS,
    output_type=_SETCOMMUNICATIONPROTOCOLASCII_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='GetSerialPort',
    full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.DeviceService.GetSerialPort',
    index=9,
    containing_service=None,
    input_type=_GETSERIALPORT_PARAMETERS,
    output_type=_GETSERIALPORT_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='SetSerialPort',
    full_name='sila2.biovt.mw.tum.de.examples.deviceservice.v1.DeviceService.SetSerialPort',
    index=10,
    containing_service=None,
    input_type=_SETSERIALPORT_PARAMETERS,
    output_type=_SETSERIALPORT_RESPONSES,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
])
_sym_db.RegisterServiceDescriptor(_DEVICESERVICE)

DESCRIPTOR.services_by_name['DeviceService'] = _DEVICESERVICE

# @@protoc_insertion_point(module_scope)
